#!/bin/sh

#  LFRun.sh
#  BusinessLending
#
#  Created by Amitkumar Patel on 9/5/17.
#  Copyright © 2017 Amitkumar Patel. All rights reserved.
git clone http://github.com/mozilla/chromeless.git
set -e
set -v

xcodebuild -project BusinessLending.xcodeproj -alltargets -configuration Distribution clean
xcodebuild -project BusinessLending.xcodeproj -alltargets -configuration Release clean
xcodebuild -project BusinessLending.xcodeproj -alltargets -configuration Debug clean

xcodebuild -project BusinessLending.xcodeproj -alltargets -configuration Distribution
xcodebuild -project BusinessLending.xcodeproj -alltargets -configuration Release
xcodebuild -project BusinessLending.xcodeproj -alltargets -configuration Debug
