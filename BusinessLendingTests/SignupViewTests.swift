//
//  SignupViewTests.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 9/5/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import XCTest
@testable import BusinessLending
class SignupViewTests: XCTestCase {
    var signupVC:SignUpViewController? = nil
    
 

    override func setUp() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        signupVC = storyboard.instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController
        signupVC?.loadView()
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testWithValid_Next_Data(){
        signupVC?.txtFldFirstName.text = "FirstName"
        signupVC?.txtFldLastName.text = "LastName"
        signupVC?.txtFldEmailId.text = "test@gmail.com"
        signupVC?.txtFldPhoneNumber.text = "1234567890"
        signupVC?.txtFldCompanyName.text = "Sigma"
        signupVC?.txtFldDBAName.text = "DBA"
        signupVC?.txtFldAddress.text = "Naranpura"
        signupVC?.txtFldCity.text = "Ahmedabad"
        signupVC?.txtFldState.text = "Gujarat"
        signupVC?.txtFldZip.text = "38233"
        signupVC?.txtFldWebsite.text = ""
        signupVC?.txtFldTaxID.text = "11111"
        XCTAssertTrue((signupVC?.validateForm())!)
    }
    func testWithBlank_FirstName(){
        signupVC?.txtFldFirstName.text = ""
        XCTAssertFalse((signupVC?.validateForm())!)
    }
    func testWithBlank_LastName(){
        signupVC?.txtFldFirstName.text = "FirstName"
        signupVC?.txtFldLastName.text = ""
        XCTAssertFalse((signupVC?.validateForm())!)
    }
    func testWithBlank_EmailId(){
        signupVC?.txtFldFirstName.text = "FirstName"
        signupVC?.txtFldLastName.text = "LastName"
        signupVC?.txtFldEmailId.text = ""
        XCTAssertFalse((signupVC?.validateForm())!)
    }
    func testWithBlank_PhoneNumber(){
        signupVC?.txtFldFirstName.text = "FirstName"
        signupVC?.txtFldLastName.text = "LastName"
        signupVC?.txtFldEmailId.text = "test@gmail.com"
        signupVC?.txtFldPhoneNumber.text = ""
        XCTAssertFalse((signupVC?.validateForm())!)
    }
    func testWithBlank_CompanyName(){
        signupVC?.txtFldFirstName.text = "FirstName"
        signupVC?.txtFldLastName.text = "LastName"
        signupVC?.txtFldEmailId.text = "test@gmail.com"
        signupVC?.txtFldPhoneNumber.text = "1234567890"
        signupVC?.txtFldCompanyName.text = ""
        XCTAssertFalse((signupVC?.validateForm())!)
    }
    func testWithBlank_CDBAName(){
        signupVC?.txtFldFirstName.text = "FirstName"
        signupVC?.txtFldLastName.text = "LastName"
        signupVC?.txtFldEmailId.text = "test@gmail.com"
        signupVC?.txtFldPhoneNumber.text = "1234567890"
        signupVC?.txtFldCompanyName.text = "Sigma"
        signupVC?.txtFldDBAName.text = ""
        XCTAssertFalse((signupVC?.validateForm())!)
    }
    func testWithBlank_Address(){
        signupVC?.txtFldFirstName.text = "FirstName"
        signupVC?.txtFldLastName.text = "LastName"
        signupVC?.txtFldEmailId.text = "test@gmail.com"
        signupVC?.txtFldPhoneNumber.text = "1234567890"
        signupVC?.txtFldCompanyName.text = "Sigma"
        signupVC?.txtFldDBAName.text = "DBA"
        signupVC?.txtFldAddress.text = ""
        XCTAssertFalse((signupVC?.validateForm())!)
    }
    func testWithBlank_City(){
        signupVC?.txtFldFirstName.text = "FirstName"
        signupVC?.txtFldLastName.text = "LastName"
        signupVC?.txtFldEmailId.text = "test@gmail.com"
        signupVC?.txtFldPhoneNumber.text = "1234567890"
        signupVC?.txtFldCompanyName.text = "Sigma"
        signupVC?.txtFldDBAName.text = "DBA"
        signupVC?.txtFldAddress.text = "Naranpura"
        signupVC?.txtFldCity.text = ""
        XCTAssertFalse((signupVC?.validateForm())!)
    }
    func testWithBlank_State(){
        signupVC?.txtFldFirstName.text = "FirstName"
        signupVC?.txtFldLastName.text = "LastName"
        signupVC?.txtFldEmailId.text = "test@gmail.com"
        signupVC?.txtFldPhoneNumber.text = "1234567890"
        signupVC?.txtFldCompanyName.text = "Sigma"
        signupVC?.txtFldDBAName.text = "DBA"
        signupVC?.txtFldAddress.text = "Naranpura"
        signupVC?.txtFldCity.text = "Ahmedabad"
        signupVC?.txtFldState.text = ""
        XCTAssertFalse((signupVC?.validateForm())!)
    }
    func testWithBlank_Zip(){
        signupVC?.txtFldFirstName.text = "FirstName"
        signupVC?.txtFldLastName.text = "LastName"
        signupVC?.txtFldEmailId.text = "test@gmail.com"
        signupVC?.txtFldPhoneNumber.text = "1234567890"
        signupVC?.txtFldCompanyName.text = "Sigma"
        signupVC?.txtFldDBAName.text = "DBA"
        signupVC?.txtFldAddress.text = "Naranpura"
        signupVC?.txtFldCity.text = "Ahmedabad"
        signupVC?.txtFldState.text = "Gujarat"
        signupVC?.txtFldZip.text = ""
        XCTAssertFalse((signupVC?.validateForm())!)
    }
    func testWithBlank_Website(){
        signupVC?.txtFldFirstName.text = "FirstName"
        signupVC?.txtFldLastName.text = "LastName"
        signupVC?.txtFldEmailId.text = "test@gmail.com"
        signupVC?.txtFldPhoneNumber.text = "1234567890"
        signupVC?.txtFldCompanyName.text = "Sigma"
        signupVC?.txtFldDBAName.text = "DBA"
        signupVC?.txtFldAddress.text = "Naranpura"
        signupVC?.txtFldCity.text = "Ahmedabad"
        signupVC?.txtFldState.text = "Gujarat"
        signupVC?.txtFldZip.text = "38233"
        signupVC?.txtFldWebsite.text = ""
        signupVC?.txtFldTaxID.text = "11111"
        XCTAssertTrue((signupVC?.validateForm())!)
    }
    func testWithBlank_TaxID(){
        signupVC?.txtFldFirstName.text = "FirstName"
        signupVC?.txtFldLastName.text = "LastName"
        signupVC?.txtFldEmailId.text = "test@gmail.com"
        signupVC?.txtFldPhoneNumber.text = "1234567890"
        signupVC?.txtFldCompanyName.text = "Sigma"
        signupVC?.txtFldDBAName.text = "DBA"
        signupVC?.txtFldAddress.text = "Naranpura"
        signupVC?.txtFldCity.text = "Ahmedabad"
        signupVC?.txtFldState.text = "Gujarat"
        signupVC?.txtFldZip.text = "38233"
        signupVC?.txtFldWebsite.text = ""
        signupVC?.txtFldTaxID.text = ""
        XCTAssertFalse((signupVC?.validateForm())!)
    }

}
