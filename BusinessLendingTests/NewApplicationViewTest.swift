//
//  NewApplicationViewTest.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 8/31/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import XCTest
@testable import BusinessLending
class NewApplicationViewTest: XCTestCase {
    var newApplicationVC:NewApplicationViewController? = nil
    override func setUp() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        newApplicationVC = storyboard.instantiateViewController(withIdentifier: "NewApplicationViewController") as? NewApplicationViewController
        newApplicationVC?.loadView()
        
        
         newApplicationVC?.btnSaveDraft = UIButton()
         newApplicationVC?.btnSaveDraft.setTitle("Save Draft", for: .normal)
         newApplicationVC?.btnSaveDraft.frame = CGRect(x: 8.0, y: 80.0, width:  (newApplicationVC?.view.bounds.size.width)!-16.0, height: 40.0)
         newApplicationVC?.btnSaveDraft.addTarget(self, action: #selector( newApplicationVC?.btnSaveDraftClicked(_:)), for: .touchUpInside)
         newApplicationVC?.btnSaveDraft.setDefaultThemeStyle()
        
        
         newApplicationVC?.btnNext = UIButton()
         newApplicationVC?.btnNext.setTitle("Next", for: .normal)
         newApplicationVC?.btnNext.frame = CGRect(x: 8.0, y: 130.0, width:  (newApplicationVC?.view.bounds.size.width)!-16.0, height: 40.0)
         newApplicationVC?.btnNext.addTarget(self, action: #selector( newApplicationVC?.btnNextClicked(_:)), for: .touchUpInside)
         newApplicationVC?.btnNext.setDefaultThemeStyle()
        
        newApplicationVC?.checkbox = VKCheckbox.init(frame: CGRect(x: 8, y: 30, width: 30, height: 30))
        
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
  
    func testWithValid_SaveDraft_Data(){
   
        newApplicationVC?.txtFldFirstName.text = ""
        newApplicationVC?.txtFldLastName.text = ""
        newApplicationVC?.txtFldSSN.text = ""
        newApplicationVC?.txtFldPrimaryPhone.text = ""
        newApplicationVC?.txtFldSecondaryPhone.text = ""
        newApplicationVC?.txtFldEmailAddress.text = "test@gmail.com"
        newApplicationVC?.txtFldHomeAddress.text = ""
        newApplicationVC?.txtFldCity.text = ""
        newApplicationVC?.txtFldState.text = ""
        newApplicationVC?.txtFldZipCode.text = ""
        newApplicationVC?.txtFldDateOfBirth.text = ""
        
        newApplicationVC?.txtFldBusinessLegalName.text = ""
        newApplicationVC?.txtFldDBA.text = ""
        newApplicationVC?.txtFldEIN.text = ""
        newApplicationVC?.txtFldBusinessAddress.text = ""
        newApplicationVC?.txtFldBusinessCity.text = ""
        newApplicationVC?.txtFldBusinessState.text = ""
        newApplicationVC?.txtFldBusinessZipCode.text = ""
        newApplicationVC?.txtFldOfficePhone.text = ""
        
        newApplicationVC?.txtFldRequestedAmount.text = "12000"
        
        XCTAssertTrue((newApplicationVC?.validateForm(btn: (newApplicationVC?.btnSaveDraft)!))!)
    }
    
    func testWithValid_Next_Data(){
        newApplicationVC?.txtFldFirstName.text = "Amit"
        newApplicationVC?.txtFldLastName.text = "Patel"
        newApplicationVC?.txtFldSSN.text = "123-45-6789"
        newApplicationVC?.txtFldPrimaryPhone.text = "1234567890"
        newApplicationVC?.txtFldSecondaryPhone.text = ""
        newApplicationVC?.txtFldEmailAddress.text = "test@gmail.com"
        newApplicationVC?.txtFldHomeAddress.text = "Naranpura"
        newApplicationVC?.txtFldCity.text = "Ahmedabad"
        newApplicationVC?.txtFldState.text = "Gujarat"
        newApplicationVC?.txtFldZipCode.text = "38233"
        newApplicationVC?.txtFldDateOfBirth.text = "11/08/1987"
        
        newApplicationVC?.txtFldBusinessLegalName.text = "Sigma"
        newApplicationVC?.txtFldDBA.text = "11111"
        newApplicationVC?.txtFldEIN.text = "11111"
        newApplicationVC?.txtFldBusinessAddress.text = "Naranpura"
        newApplicationVC?.txtFldBusinessCity.text = "Ahmedabad"
        newApplicationVC?.txtFldBusinessState.text = "Gujarat"
        newApplicationVC?.txtFldBusinessZipCode.text = "38233"
        newApplicationVC?.txtFldOfficePhone.text = "1234567890"
        
        newApplicationVC?.txtFldRequestedAmount.text = "12000"
        newApplicationVC?.checkbox.setOn(true)
        XCTAssertTrue((newApplicationVC?.validateForm(btn: (newApplicationVC?.btnNext)!))!)
    }
    
    
    func testWithBlank_FirstName(){
        newApplicationVC?.txtFldFirstName.text = ""
        XCTAssertFalse((newApplicationVC?.validateForm(btn: (newApplicationVC?.btnNext)!))!)
    }
    func testWithBlank_LastName(){
        newApplicationVC?.txtFldFirstName.text = "Amit"
       
        newApplicationVC?.txtFldLastName.text = ""
        XCTAssertFalse((newApplicationVC?.validateForm(btn: (newApplicationVC?.btnNext)!))!)
    }
    func testWithBlank_SSN(){
        newApplicationVC?.txtFldFirstName.text = "Amit"
        newApplicationVC?.txtFldLastName.text = "Patel"
        
        newApplicationVC?.txtFldSSN.text = ""
        XCTAssertFalse((newApplicationVC?.validateForm(btn: (newApplicationVC?.btnNext)!))!)
    }
    func testWithBlank_PrimaryPhone(){
        newApplicationVC?.txtFldFirstName.text = "Amit"
        newApplicationVC?.txtFldLastName.text = "Patel"
        newApplicationVC?.txtFldSSN.text = "123-45-6789"
       
        newApplicationVC?.txtFldPrimaryPhone.text = ""
        XCTAssertFalse((newApplicationVC?.validateForm(btn: (newApplicationVC?.btnNext)!))!)
    }
    func testWithBlank_EmailAddress(){
        newApplicationVC?.txtFldFirstName.text = "Amit"
        newApplicationVC?.txtFldLastName.text = "Patel"
        newApplicationVC?.txtFldSSN.text = "123-45-6789"
        newApplicationVC?.txtFldPrimaryPhone.text = "1234567890"
        newApplicationVC?.txtFldSecondaryPhone.text = ""
  
        newApplicationVC?.txtFldEmailAddress.text = ""
        XCTAssertFalse((newApplicationVC?.validateForm(btn: (newApplicationVC?.btnNext)!))!)
    }
    func testWithBlank_HomeAddress(){
        newApplicationVC?.txtFldFirstName.text = "Amit"
        newApplicationVC?.txtFldLastName.text = "Patel"
        newApplicationVC?.txtFldSSN.text = "123-45-6789"
        newApplicationVC?.txtFldPrimaryPhone.text = "1234567890"
        newApplicationVC?.txtFldSecondaryPhone.text = ""
        newApplicationVC?.txtFldEmailAddress.text = "test@gmail.com"
        
        newApplicationVC?.txtFldHomeAddress.text = ""
        XCTAssertFalse((newApplicationVC?.validateForm(btn: (newApplicationVC?.btnNext)!))!)
    }
    func testWithBlank_City(){
        newApplicationVC?.txtFldFirstName.text = "Amit"
        newApplicationVC?.txtFldLastName.text = "Patel"
        newApplicationVC?.txtFldSSN.text = "123-45-6789"
        newApplicationVC?.txtFldPrimaryPhone.text = "1234567890"
        newApplicationVC?.txtFldSecondaryPhone.text = ""
        newApplicationVC?.txtFldEmailAddress.text = "test@gmail.com"
        newApplicationVC?.txtFldHomeAddress.text = "Naranpura"
        
        newApplicationVC?.txtFldCity.text = ""
        XCTAssertFalse((newApplicationVC?.validateForm(btn: (newApplicationVC?.btnNext)!))!)
    }
    func testWithBlank_State(){
        newApplicationVC?.txtFldFirstName.text = "Amit"
        newApplicationVC?.txtFldLastName.text = "Patel"
        newApplicationVC?.txtFldSSN.text = "123-45-6789"
        newApplicationVC?.txtFldPrimaryPhone.text = "1234567890"
        newApplicationVC?.txtFldSecondaryPhone.text = ""
        newApplicationVC?.txtFldEmailAddress.text = "test@gmail.com"
        newApplicationVC?.txtFldHomeAddress.text = "Naranpura"
        newApplicationVC?.txtFldCity.text = "Ahmedabad"
        newApplicationVC?.txtFldState.text = ""
        XCTAssertFalse((newApplicationVC?.validateForm(btn: (newApplicationVC?.btnNext)!))!)
    }
    func testWithBlank_ZipCode(){
        newApplicationVC?.txtFldFirstName.text = "Amit"
        newApplicationVC?.txtFldLastName.text = "Patel"
        newApplicationVC?.txtFldSSN.text = "123-45-6789"
        newApplicationVC?.txtFldPrimaryPhone.text = "1234567890"
        newApplicationVC?.txtFldSecondaryPhone.text = ""
        newApplicationVC?.txtFldEmailAddress.text = "test@gmail.com"
        newApplicationVC?.txtFldHomeAddress.text = "Naranpura"
        newApplicationVC?.txtFldCity.text = "Ahmedabad"
        newApplicationVC?.txtFldState.text = "Gujarat"
        newApplicationVC?.txtFldZipCode.text = ""
        XCTAssertFalse((newApplicationVC?.validateForm(btn: (newApplicationVC?.btnNext)!))!)
    }
    func testWithBlank_DateOfBirth(){
        newApplicationVC?.txtFldFirstName.text = "Amit"
        newApplicationVC?.txtFldLastName.text = "Patel"
        newApplicationVC?.txtFldSSN.text = "123-45-6789"
        newApplicationVC?.txtFldPrimaryPhone.text = "1234567890"
        newApplicationVC?.txtFldSecondaryPhone.text = ""
        newApplicationVC?.txtFldEmailAddress.text = "test@gmail.com"
        newApplicationVC?.txtFldHomeAddress.text = "Naranpura"
        newApplicationVC?.txtFldCity.text = "Ahmedabad"
        newApplicationVC?.txtFldState.text = "Gujarat"
        newApplicationVC?.txtFldZipCode.text = "38233"
        newApplicationVC?.txtFldDateOfBirth.text = ""
        XCTAssertFalse((newApplicationVC?.validateForm(btn: (newApplicationVC?.btnNext)!))!)
    }
    func testWithBlank_BusinessLegalName(){
        newApplicationVC?.txtFldFirstName.text = "Amit"
        newApplicationVC?.txtFldLastName.text = "Patel"
        newApplicationVC?.txtFldSSN.text = "123-45-6789"
        newApplicationVC?.txtFldPrimaryPhone.text = "1234567890"
        newApplicationVC?.txtFldSecondaryPhone.text = ""
        newApplicationVC?.txtFldEmailAddress.text = "test@gmail.com"
        newApplicationVC?.txtFldHomeAddress.text = "Naranpura"
        newApplicationVC?.txtFldCity.text = "Ahmedabad"
        newApplicationVC?.txtFldState.text = "Gujarat"
        newApplicationVC?.txtFldZipCode.text = "38233"
        newApplicationVC?.txtFldDateOfBirth.text = "11/08/1987"
        newApplicationVC?.txtFldBusinessLegalName.text = ""
        XCTAssertFalse((newApplicationVC?.validateForm(btn: (newApplicationVC?.btnNext)!))!)
    }
    func testWithBlank_DBA(){
        newApplicationVC?.txtFldFirstName.text = "Amit"
        newApplicationVC?.txtFldLastName.text = "Patel"
        newApplicationVC?.txtFldSSN.text = "123-45-6789"
        newApplicationVC?.txtFldPrimaryPhone.text = "1234567890"
        newApplicationVC?.txtFldSecondaryPhone.text = ""
        newApplicationVC?.txtFldEmailAddress.text = "test@gmail.com"
        newApplicationVC?.txtFldHomeAddress.text = "Naranpura"
        newApplicationVC?.txtFldCity.text = "Ahmedabad"
        newApplicationVC?.txtFldState.text = "Gujarat"
        newApplicationVC?.txtFldZipCode.text = "38233"
        newApplicationVC?.txtFldDateOfBirth.text = "11/08/1987"
        newApplicationVC?.txtFldBusinessLegalName.text = "Sigma"
        newApplicationVC?.txtFldDBA.text = ""
        XCTAssertFalse((newApplicationVC?.validateForm(btn: (newApplicationVC?.btnNext)!))!)
    }
    func testWithBlank_EIN(){
        newApplicationVC?.txtFldFirstName.text = "Amit"
        newApplicationVC?.txtFldLastName.text = "Patel"
        newApplicationVC?.txtFldSSN.text = "123-45-6789"
        newApplicationVC?.txtFldPrimaryPhone.text = "1234567890"
        newApplicationVC?.txtFldSecondaryPhone.text = ""
        newApplicationVC?.txtFldEmailAddress.text = "test@gmail.com"
        newApplicationVC?.txtFldHomeAddress.text = "Naranpura"
        newApplicationVC?.txtFldCity.text = "Ahmedabad"
        newApplicationVC?.txtFldState.text = "Gujarat"
        newApplicationVC?.txtFldZipCode.text = "38233"
        newApplicationVC?.txtFldDateOfBirth.text = "11/08/1987"
        newApplicationVC?.txtFldBusinessLegalName.text = "Sigma"
        newApplicationVC?.txtFldDBA.text = "11111"
        newApplicationVC?.txtFldEIN.text = ""
        XCTAssertFalse((newApplicationVC?.validateForm(btn: (newApplicationVC?.btnNext)!))!)
    }
    
    func testWithBlank_BusinessAddress(){
        newApplicationVC?.txtFldFirstName.text = "Amit"
        newApplicationVC?.txtFldLastName.text = "Patel"
        newApplicationVC?.txtFldSSN.text = "123-45-6789"
        newApplicationVC?.txtFldPrimaryPhone.text = "1234567890"
        newApplicationVC?.txtFldSecondaryPhone.text = ""
        newApplicationVC?.txtFldEmailAddress.text = "test@gmail.com"
        newApplicationVC?.txtFldHomeAddress.text = "Naranpura"
        newApplicationVC?.txtFldCity.text = "Ahmedabad"
        newApplicationVC?.txtFldState.text = "Gujarat"
        newApplicationVC?.txtFldZipCode.text = "38233"
        newApplicationVC?.txtFldDateOfBirth.text = "11/08/1987"
        newApplicationVC?.txtFldBusinessLegalName.text = "Sigma"
        newApplicationVC?.txtFldDBA.text = "11111"
        newApplicationVC?.txtFldEIN.text = "11111"
        newApplicationVC?.txtFldBusinessAddress.text = ""
        XCTAssertFalse((newApplicationVC?.validateForm(btn: (newApplicationVC?.btnNext)!))!)
    }
    
    func testWithBlank_BusinessCity(){
        newApplicationVC?.txtFldFirstName.text = "Amit"
        newApplicationVC?.txtFldLastName.text = "Patel"
        newApplicationVC?.txtFldSSN.text = "123-45-6789"
        newApplicationVC?.txtFldPrimaryPhone.text = "1234567890"
        newApplicationVC?.txtFldSecondaryPhone.text = ""
        newApplicationVC?.txtFldEmailAddress.text = "test@gmail.com"
        newApplicationVC?.txtFldHomeAddress.text = "Naranpura"
        newApplicationVC?.txtFldCity.text = "Ahmedabad"
        newApplicationVC?.txtFldState.text = "Gujarat"
        newApplicationVC?.txtFldZipCode.text = "38233"
        newApplicationVC?.txtFldDateOfBirth.text = "11/08/1987"
        newApplicationVC?.txtFldBusinessLegalName.text = "Sigma"
        newApplicationVC?.txtFldDBA.text = "11111"
        newApplicationVC?.txtFldEIN.text = "11111"
        newApplicationVC?.txtFldBusinessAddress.text = "Naranpura"
        newApplicationVC?.txtFldBusinessCity.text = ""
        XCTAssertFalse((newApplicationVC?.validateForm(btn: (newApplicationVC?.btnNext)!))!)
    }
    
    func testWithBlank_BusinessState(){
        newApplicationVC?.txtFldFirstName.text = "Amit"
        newApplicationVC?.txtFldLastName.text = "Patel"
        newApplicationVC?.txtFldSSN.text = "123-45-6789"
        newApplicationVC?.txtFldPrimaryPhone.text = "1234567890"
        newApplicationVC?.txtFldSecondaryPhone.text = ""
        newApplicationVC?.txtFldEmailAddress.text = "test@gmail.com"
        newApplicationVC?.txtFldHomeAddress.text = "Naranpura"
        newApplicationVC?.txtFldCity.text = "Ahmedabad"
        newApplicationVC?.txtFldState.text = "Gujarat"
        newApplicationVC?.txtFldZipCode.text = "38233"
        newApplicationVC?.txtFldDateOfBirth.text = "11/08/1987"
        newApplicationVC?.txtFldBusinessLegalName.text = "Sigma"
        newApplicationVC?.txtFldDBA.text = "11111"
        newApplicationVC?.txtFldEIN.text = "11111"
        newApplicationVC?.txtFldBusinessAddress.text = "Naranpura"
        newApplicationVC?.txtFldBusinessCity.text = "Ahmedabad"
        newApplicationVC?.txtFldBusinessState.text = ""
        XCTAssertFalse((newApplicationVC?.validateForm(btn: (newApplicationVC?.btnNext)!))!)
    }
    
    func testWithBlank_BusinessZipCode(){
        newApplicationVC?.txtFldFirstName.text = "Amit"
        newApplicationVC?.txtFldLastName.text = "Patel"
        newApplicationVC?.txtFldSSN.text = "123-45-6789"
        newApplicationVC?.txtFldPrimaryPhone.text = "1234567890"
        newApplicationVC?.txtFldSecondaryPhone.text = ""
        newApplicationVC?.txtFldEmailAddress.text = "test@gmail.com"
        newApplicationVC?.txtFldHomeAddress.text = "Naranpura"
        newApplicationVC?.txtFldCity.text = "Ahmedabad"
        newApplicationVC?.txtFldState.text = "Gujarat"
        newApplicationVC?.txtFldZipCode.text = "38233"
        newApplicationVC?.txtFldDateOfBirth.text = "11/08/1987"
        newApplicationVC?.txtFldBusinessLegalName.text = "Sigma"
        newApplicationVC?.txtFldDBA.text = "11111"
        newApplicationVC?.txtFldEIN.text = "11111"
        newApplicationVC?.txtFldBusinessAddress.text = "Naranpura"
        newApplicationVC?.txtFldBusinessCity.text = "Ahmedabad"
        newApplicationVC?.txtFldBusinessState.text = "Gujarat"
        newApplicationVC?.txtFldBusinessZipCode.text = ""
        XCTAssertFalse((newApplicationVC?.validateForm(btn: (newApplicationVC?.btnNext)!))!)
    }
    
    func testWithBlank_OfficePhone(){
        newApplicationVC?.txtFldFirstName.text = "Amit"
        newApplicationVC?.txtFldLastName.text = "Patel"
        newApplicationVC?.txtFldSSN.text = "123-45-6789"
        newApplicationVC?.txtFldPrimaryPhone.text = "1234567890"
        newApplicationVC?.txtFldSecondaryPhone.text = ""
        newApplicationVC?.txtFldEmailAddress.text = "test@gmail.com"
        newApplicationVC?.txtFldHomeAddress.text = "Naranpura"
        newApplicationVC?.txtFldCity.text = "Ahmedabad"
        newApplicationVC?.txtFldState.text = "Gujarat"
        newApplicationVC?.txtFldZipCode.text = "38233"
        newApplicationVC?.txtFldDateOfBirth.text = "11/08/1987"
        newApplicationVC?.txtFldBusinessLegalName.text = "Sigma"
        newApplicationVC?.txtFldDBA.text = "11111"
        newApplicationVC?.txtFldEIN.text = "11111"
        newApplicationVC?.txtFldBusinessAddress.text = "Naranpura"
        newApplicationVC?.txtFldBusinessCity.text = "Ahmedabad"
        newApplicationVC?.txtFldBusinessState.text = "Gujarat"
        newApplicationVC?.txtFldBusinessZipCode.text = "38233"
        newApplicationVC?.txtFldOfficePhone.text = ""
        XCTAssertFalse((newApplicationVC?.validateForm(btn: (newApplicationVC?.btnNext)!))!)
    }
    
    func testWithBlank_RequestedAmount(){
        newApplicationVC?.txtFldFirstName.text = "Amit"
        newApplicationVC?.txtFldLastName.text = "Patel"
        newApplicationVC?.txtFldSSN.text = "123-45-6789"
        newApplicationVC?.txtFldPrimaryPhone.text = "1234567890"
        newApplicationVC?.txtFldSecondaryPhone.text = ""
        newApplicationVC?.txtFldEmailAddress.text = "test@gmail.com"
        newApplicationVC?.txtFldHomeAddress.text = "Naranpura"
        newApplicationVC?.txtFldCity.text = "Ahmedabad"
        newApplicationVC?.txtFldState.text = "Gujarat"
        newApplicationVC?.txtFldZipCode.text = "38233"
        newApplicationVC?.txtFldDateOfBirth.text = "11/08/1987"
        newApplicationVC?.txtFldBusinessLegalName.text = "Sigma"
        newApplicationVC?.txtFldDBA.text = "11111"
        newApplicationVC?.txtFldEIN.text = "11111"
        newApplicationVC?.txtFldBusinessAddress.text = "Naranpura"
        newApplicationVC?.txtFldBusinessCity.text = "Ahmedabad"
        newApplicationVC?.txtFldBusinessState.text = "Gujarat"
        newApplicationVC?.txtFldBusinessZipCode.text = "38233"
        newApplicationVC?.txtFldOfficePhone.text = "1234567890"
        newApplicationVC?.txtFldRequestedAmount.text = ""
        XCTAssertFalse((newApplicationVC?.validateForm(btn: (newApplicationVC?.btnNext)!))!)
    }
    
    
}
