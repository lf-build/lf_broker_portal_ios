//
//  LoginViewTests.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 8/30/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import XCTest
@testable import BusinessLending

class LoginViewTests: XCTestCase {
    var loginVC:LogInViewController? = nil
    override func setUp() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        loginVC = storyboard.instantiateViewController(withIdentifier: "LogInViewController") as? LogInViewController
        loginVC?.loadView()
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testWithNilPartner(){
        loginVC?.txtFldPartnerType.text = ""
        loginVC?.txtFldUserName.text = "amit"
        loginVC?.txtFldPassword.text = "password"
        XCTAssertFalse((loginVC?.validateForm())!)
    }
    
    func testWithInvalidUsername(){
        loginVC?.txtFldPartnerType.text = "divergent-capital"
        loginVC?.txtFldUserName.text = "a"
        loginVC?.txtFldPassword.text = "password"
        XCTAssertFalse((loginVC?.validateForm())!)
    }
    func testWithInvalidPassword(){
        loginVC?.txtFldPartnerType.text = "divergent-capital"
        loginVC?.txtFldUserName.text = "amit"
        loginVC?.txtFldPassword.text = "p"
        XCTAssertFalse((loginVC?.validateForm())!)
    }
    
    func testWithValidData(){
        loginVC?.txtFldPartnerType.text = "divergent-capital"
        loginVC?.txtFldUserName.text = "amit"
        loginVC?.txtFldPassword.text = "password"
        XCTAssertTrue((loginVC?.validateForm())!)
    }
    
//    // MARK: Validate Email
//    func validateEmail(emailString: String?) -> Bool {
//        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
//        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
//        return emailPredicate.evaluate(with: emailString)
//    }
    
}
