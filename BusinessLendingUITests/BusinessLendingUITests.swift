//
//  BusinessLendingUITests.swift
//  BusinessLendingUITests
//
//  Created by Amitkumar Patel on 6/28/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import XCTest

class BusinessLendingUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
       
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
//        XCUIApplication().launch()
        
        let app = XCUIApplication()
        setupSnapshot(app)
         snapshot("01 Splashscreen")
        app.launch()
       
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        
        let app = XCUIApplication()
        let whiteMenuButton = app.navigationBars["Dashboard"].buttons["white menu"]
        whiteMenuButton.tap()
        whiteMenuButton.tap()
        
        let tablesQuery = app.tables
        tablesQuery.staticTexts["Applications"].tap()
        whiteMenuButton.tap()
        tablesQuery.staticTexts["\tAll"].tap()
        
        let whiteMenuButton2 = app.navigationBars["All Application"].buttons["white menu"]
        whiteMenuButton2.tap()
        whiteMenuButton2.tap()
        whiteMenuButton2.tap()
        
        let closedStaticText = tablesQuery.staticTexts["\tClosed"]
        closedStaticText.tap()
        app.navigationBars["Closed Application"].buttons["white menu"].tap()
        tablesQuery.staticTexts["\tPersonal details"].tap()
        app.navigationBars["Personal Details"].buttons["white menu"].tap()
        closedStaticText.tap()
        tablesQuery.cells.containing(.staticText, identifier:"DC0000012").staticTexts["Peter DAIZ"].tap()
        app.navigationBars["Funded"].children(matching: .button).matching(identifier: "Back").element(boundBy: 0).tap()
        tablesQuery.searchFields["Search"].tap()
        app.searchFields["Search"].typeText("T")
        app.keys["e"].tap()
        app.typeText("es")
        
        let table = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element(boundBy: 0).children(matching: .other).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .table).element
        table.tap()
        table.tap()
        app.typeText("t\n")
        app.buttons["Cancel"].tap()
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
}
