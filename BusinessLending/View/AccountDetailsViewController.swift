//
//  AccountDetailsViewController.swift
//  Business Lending
//
//  Created by Amitkumar Patel on 6/27/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit

class AccountDetailsViewController: UITableViewController, ENSideMenuDelegate {

    @IBOutlet weak var txtFldEmailId: UITextField!
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
      var responseString:String = ""
    
    override func viewDidLoad() {
        self.sideMenuController()?.sideMenu?.delegate = self
        setUI()
        txtFldEmailId.text = UserData.sharedInstance.email
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.sideMenuController()?.sideMenu?.delegate = self
    }
    // MARK: - ENSideMenu Delegate
    @IBAction func toggleSideMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    func sideMenuWillOpen() {
        
    }
    
    func sideMenuWillClose() {
        
    }
    
    func sideMenuDidClose() {
        self.view.isUserInteractionEnabled = true
    }
    
    func sideMenuDidOpen() {
        self.view.isUserInteractionEnabled = false
    }
    
    
    func sideMenuShouldOpenSideMenu() -> Bool {
        
        return true
    }
    
    func setUI() {
        
        self.navigationItem.title = "Account Details"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : NAVIGATIONBAR_TINTCOLOR];
        
        self.navigationController?.navigationBar.tintColor = NAVIGATIONBAR_TINTCOLOR
    
        
        
        let button = UIButton.init(type: .custom)
        button.setMenuButtonStyle()
        button.addTarget(self, action:#selector(SettingViewController.toggleSideMenu(sender:)), for: UIControlEvents.touchUpInside)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        self.view.backgroundColor = Utility.hexStringToUIColor(hex: VIEW_BACKGROUND_COLOR)
        activityIndicator.setDefaultThemeStyle()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if (section == tableView.numberOfSections-1) {
            return 100.0
        }
        return 0.0
    }
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if (section == tableView.numberOfSections-1) {
            let btnUpdate = UIButton()
            let footerView = UIView()
            footerView.frame = CGRect(x: 0.0, y: 0.0, width: self.view.bounds.size.width, height: 100.0)
            
            
            btnUpdate.setTitle("RESET PASSWORD", for: .normal)
            btnUpdate.frame = CGRect(x: 8.0, y: 30.0, width: self.view.bounds.size.width-16.0, height: 40.0)
            btnUpdate.addTarget(self, action: #selector(AccountDetailsViewController.btnResetPasswordClicked(_:)), for: .touchUpInside)
            btnUpdate.setDefaultThemeStyle()
            footerView.addSubview(btnUpdate)
            return footerView
        }
        let footerView = UIView()
        return footerView
    }
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        guard let header = view as? UITableViewHeaderFooterView else { return }
        header.textLabel?.font = UIFont.boldSystemFont(ofSize: CGFloat(TABLEVIEW_HEADERTITLE_FONT_SIZE))
        header.textLabel?.frame = header.frame
        header.textLabel?.textAlignment = .left
        header.textLabel?.textColor = UIColor.black
        
        if section == 0 {
            header.textLabel?.font = UIFont.fontAwesome(ofSize: (header.textLabel?.font.pointSize)!)
            header.textLabel?.text = (String.fontAwesomeIcon(code: FAICON_ACCOUNT_DETAILS)! + "\t" + (header.textLabel?.text)!)
        }
    }
    
    
    @IBAction func btnResetPasswordClicked(_ sender: Any) {
        if (Reachability.isConnectedToNetwork()){
            // Create and add the view to the screen.
            self.view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            
            // All done!
            
            let paramsDict: [String : Any] = ["username" : txtFldEmailId.text ?? "",
                                                "realm":APPLICATION_ACCESS_REALM
                                            ]

            Utility.requestWithPostURLWithData(strUrl: BASE_URL + RESET_PASSWORD_PATH, paramDict: paramsDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
                
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    if error == nil{
                        let httpResponse = urlResponse as! HTTPURLResponse
                        self.responseString = String(data: dataResponse!, encoding: .utf8)!
                        let dict = Utility.convertToDictionary(text: self.responseString)
                        
                        if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK && dict != nil){
                                                    }
                        else{
                            self.activityIndicator.stopAnimating()
                            if((dict?["message"]) != nil){
                                Utility.showErrorAlert( message: (dict?["message"] as? String)!)
                            }
                            else if((dict?["details"]) != nil){
                                let dictError = (dict?["details"] as![Dictionary<String,Any>])[0]
                                if((dictError["message"]) != nil){
                                    Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                }
                            }
                            
                        }
                    }
                    else{
                        self.activityIndicator.stopAnimating()
                        Utility.showErrorAlert(message: (error?.localizedDescription)!)
                        
                    }
                }
            })
        }
        else{
            self.activityIndicator.stopAnimating()
            Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
        }
    }

  

}
