//
//  SignUpViewController.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 6/23/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit

class SignUpViewController: UITableViewController, UITextFieldDelegate, KeyboardToolbarDelegate {

    @IBOutlet weak var txtFldFirstName: UITextField!
    @IBOutlet weak var txtFldLastName: UITextField!
    @IBOutlet weak var txtFldEmailId: UITextField!
    @IBOutlet weak var txtFldPhoneNumber: UITextField!
    @IBOutlet weak var txtFldCompanyName: UITextField!
    @IBOutlet weak var txtFldDBAName: UITextField!
    @IBOutlet weak var txtFldAddress: UITextField!
    @IBOutlet weak var txtFldCity: UITextField!
    @IBOutlet weak var txtFldState: UITextField!
    @IBOutlet weak var txtFldZip: UITextField!
    @IBOutlet weak var txtFldWebsite: UITextField!
    
    @IBOutlet weak var txtFldTaxID: UITextField!
    var objKeyboardToolbar: KeyboarToolbar = KeyboarToolbar.sharedInstance
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    var  strWebSite: String = ""
    var activeTextField = UITextField()
    
    
    override func viewDidLoad() {
        objKeyboardToolbar.objKeyboardToolbarDelegate = self
        setUI()
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(SignUpViewController.hideKeyboard))
        tapGesture.cancelsTouchesInView = true
        
        tableView.addGestureRecognizer(tapGesture)
        
        setLeftviewInTextfield()
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    func setLeftviewInTextfield() {
        txtFldFirstName.setFaIconAsLeftView(strFaIcon: FieldFaIcon.FirstName.rawValue)
        txtFldLastName.setFaIconAsLeftView(strFaIcon: FieldFaIcon.FirstName.rawValue)
        txtFldEmailId.setFaIconAsLeftView(strFaIcon: FieldFaIcon.EmailAddress.rawValue)
        txtFldPhoneNumber.setFaIconAsLeftView(strFaIcon: FieldFaIcon.PhoneNumber.rawValue)
        txtFldAddress.setFaIconAsLeftView(strFaIcon: FieldFaIcon.Address.rawValue)
        txtFldCity.setFaIconAsLeftView(strFaIcon: FieldFaIcon.Map.rawValue)
        txtFldState.setFaIconAsLeftView(strFaIcon: FieldFaIcon.Map.rawValue)
        txtFldZip.setFaIconAsLeftView(strFaIcon: FieldFaIcon.Address.rawValue)
        
        
        txtFldCompanyName.setFaIconAsLeftView(strFaIcon: FieldFaIcon.Company.rawValue)
        txtFldDBAName.setFaIconAsLeftView(strFaIcon: FieldFaIcon.Temp.rawValue)
        txtFldWebsite.setFaIconAsLeftView(strFaIcon: FieldFaIcon.Website.rawValue)
        txtFldTaxID.setFaIconAsLeftView(strFaIcon: FieldFaIcon.TaxID.rawValue)
        
    }
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }
    func setUI(){
        self.title = "Sign Up"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : NAVIGATIONBAR_TINTCOLOR];
        
        self.navigationController?.navigationBar.tintColor = NAVIGATIONBAR_TINTCOLOR
        self.view.backgroundColor = Utility.hexStringToUIColor(hex: VIEW_BACKGROUND_COLOR)
       
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view Delegate

    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if (section == tableView.numberOfSections-1) {
            return 100.0
        }
        return 0.0
    }
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if (section == tableView.numberOfSections-1) {
            let btnUpdate = UIButton()
            let footerView = UIView()
            footerView.frame = CGRect(x: 0.0, y: 0.0, width: self.view.bounds.size.width, height: 100.0)
            
            
            btnUpdate.setTitle("Sign Up", for: .normal)
            btnUpdate.frame = CGRect(x: 8.0, y: 30.0, width: self.view.bounds.size.width-16.0, height: 40.0)
            btnUpdate.addTarget(self, action: #selector(SignUpViewController.btnSignUpClicked(_:)), for: .touchUpInside)
            btnUpdate.setDefaultThemeStyle()
            footerView.addSubview(btnUpdate)
            return footerView
        }
        let footerView = UIView()
        return footerView
        
    }
   
    override func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
            view.tintColor = UIColor.clear
    }

    // MARK: - UITextField Delegate Method
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtFldPhoneNumber{
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            if  (text.characters.count == 0 && newLength == 1){
                textField.text = "(" + textField.text!
            }
            
            if  (text.characters.count == 2 && newLength == 1){
                textField.text = textField.text?.substring(to: (textField.text?.index(before: (textField.text?.endIndex)!))!)
            }
            
            
            
            if  (text.characters.count == 4 && newLength == 5){
                textField.text =  textField.text! + ") "
            }
            
            if  (text.characters.count == 7 && newLength == 6){
//                textField.text = textField.text?.substring(to: (textField.text?.index(before: (textField.text?.endIndex)!))!)
//                textField.text = textField.text?.substring(to: (textField.text?.index(before: (textField.text?.endIndex)!))!)
                textField.text = String(describing: textField.text?.characters.dropLast())
                textField.text = String(describing: textField.text?.characters.dropLast())
            }
            
            if  (text.characters.count == 9 && newLength == 10){
                textField.text =  textField.text! + "-"
            }
            
            if  (text.characters.count == 11 && newLength == 10){
                textField.text = textField.text?.substring(to: (textField.text?.index(before: (textField.text?.endIndex)!))!)
            }
            return newLength <= PHONE_NUMBER_LENGTH
        }
        if textField == txtFldZip{
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= ZIP_CODE_LENGTH
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case txtFldFirstName:
            txtFldLastName.becomeFirstResponder()
            break
        case txtFldLastName:
            txtFldEmailId.becomeFirstResponder()
            break
        case txtFldEmailId:
            txtFldPhoneNumber.becomeFirstResponder()
            break
        case txtFldPhoneNumber:
            txtFldAddress.becomeFirstResponder()
            break
        case txtFldAddress:
            txtFldCity.becomeFirstResponder()
            break
        case txtFldCity:
            txtFldState.becomeFirstResponder()
            break
        case txtFldState:
            txtFldZip.becomeFirstResponder()
            break
        case txtFldZip:
            txtFldCompanyName.becomeFirstResponder()
            break
        case txtFldCompanyName:
            txtFldDBAName.becomeFirstResponder()
            break
        case txtFldDBAName:
            txtFldWebsite.becomeFirstResponder()
            break
        case txtFldWebsite:
            txtFldTaxID.becomeFirstResponder()
            break
        case txtFldTaxID:
            txtFldTaxID.resignFirstResponder()
            break
        default:
            self.view.endEditing(true)
            break
        }
        return false
    }
    
    // MARK: - UITextField Delegate Method
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.inputAccessoryView == nil {
            textField.inputAccessoryView = objKeyboardToolbar
        }
        activeTextField = textField
        return true
    }
    
    func keyboardToolbarNextButtonClicked() {
        switch activeTextField {
        case txtFldFirstName:
            txtFldLastName.becomeFirstResponder()
            break
        case txtFldLastName:
            txtFldEmailId.becomeFirstResponder()
            break
        case txtFldEmailId:
            txtFldPhoneNumber.becomeFirstResponder()
            break
        case txtFldPhoneNumber:
            txtFldAddress.becomeFirstResponder()
            break
        case txtFldAddress:
            txtFldCity.becomeFirstResponder()
            break
        case txtFldCity:
            txtFldState.becomeFirstResponder()
            break
        case txtFldState:
            txtFldZip.becomeFirstResponder()
            break
        case txtFldZip:
            txtFldCompanyName.becomeFirstResponder()
            break
        case txtFldCompanyName:
            txtFldDBAName.becomeFirstResponder()
            break
        case txtFldDBAName:
            txtFldWebsite.becomeFirstResponder()
            break
        case txtFldWebsite:
            txtFldTaxID.becomeFirstResponder()
            break
        case txtFldTaxID:
            txtFldTaxID.resignFirstResponder()
            break
        default:
            self.view.endEditing(true)
            break
        }
        
    }
    func keyboardToolbarPreviousButtonClicked() {
        switch activeTextField {
        case txtFldTaxID:
            txtFldWebsite.becomeFirstResponder()
            break
        case txtFldWebsite:
            txtFldDBAName.becomeFirstResponder()
            break
        case txtFldDBAName:
            txtFldCompanyName.becomeFirstResponder()
            break
        case txtFldCompanyName:
            txtFldZip.becomeFirstResponder()
            break
        case txtFldZip:
            txtFldState.becomeFirstResponder()
            break
        case txtFldState:
            txtFldCity.becomeFirstResponder()
            break
        case txtFldCity:
            txtFldAddress.becomeFirstResponder()
            break
        case txtFldAddress:
            txtFldPhoneNumber.becomeFirstResponder()
            break
        case txtFldPhoneNumber:
            txtFldEmailId.becomeFirstResponder()
            break
        case txtFldEmailId:
            txtFldLastName.becomeFirstResponder()
            break
        case txtFldLastName:
            txtFldFirstName.becomeFirstResponder()
            break
        case txtFldFirstName:
            txtFldFirstName.becomeFirstResponder()
            break
        default:
            self.view.endEditing(true)
            break
        }
    }
    func keyboardToolbarHideKeyboard() {
        hideKeyboard()
    }
    
    @IBAction func btnSignUpClicked(_ sender: Any) {
        if (!txtFldWebsite.text!.isEmpty) {
            strWebSite = Utility.checkAndAddURLPrefic(strURL: txtFldWebsite.text!)
        }
        
        if validateForm() {
            if (Reachability.isConnectedToNetwork()){
                // Create and add the view to the screen.
                self.view.addSubview(activityIndicator)
                activityIndicator.startAnimating()
                // All done!
            
                
                let paramsDict: [String : Any] = ["firstName" :txtFldFirstName.text!,
                                              "lastName" : txtFldLastName.text!,
                                              "email" : txtFldEmailId.text!,
                                              "phoneNumber" : txtFldPhoneNumber.text!.removeExtraCharFromPhoneNumber(),
                                              "companyName" : txtFldCompanyName.text!,
                                              "dba" : txtFldDBAName.text!,
                                              "address" : txtFldAddress.text!,
                                              "city" : txtFldCity.text!,
                                              "state" : txtFldState.text!,
                                              "zip" : txtFldZip.text!,
                                              "website" : strWebSite,
                                              "taxID" : txtFldTaxID.text!,
                ]
                
                Utility.requestWithPostURLWithData(strUrl: BASE_URL + SIGNUP_PATH, paramDict:paramsDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
                    
                    DispatchQueue.main.async {
                        self.activityIndicator.stopAnimating()
                        if error == nil{
                            let httpResponse = urlResponse as! HTTPURLResponse
                            let responseString = String(data: dataResponse!, encoding: .utf8)
                            
                            let dict = Utility.convertToDictionary(text: responseString!)
                            
                            if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK && dict != nil){
                                Utility.showSuccessAlert(message: "Thank you for your interest in our partnership program. A representative will reach out to you in the next business day.")
                                self.navigationController?.popViewController(animated: true)
                            }
                            else{
                                if((dict?["message"]) != nil){
                                    Utility.showErrorAlert( message: (dict?["message"] as? String)!)
                                }
                                else if((dict?["details"]) != nil){
                                    let dictError = (dict?["details"] as![Dictionary<String,Any>])[0]
                                    if((dictError["message"]) != nil){
                                        Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                    }
                                }
                            }
                        }
                        else{
                            Utility.showErrorAlert( message: (error?.localizedDescription)! )
                            
                        }
                    }
                })
            }
            else{
                self.activityIndicator.stopAnimating()
                Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
            }
            
        }
    }
    
    func validateForm() -> Bool {
        
        /// First Name Validation
        if (txtFldFirstName.text?.isEmpty)!{
            Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.FirstName.rawValue) )
            txtFldFirstName.setRequiredFildErrorStyle()
            return false;
        }
        else if( !Utility.checkLengthValidation(strInput: txtFldFirstName.text, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100)){
            Utility.showErrorAlert( message: Message.getFieldLengthErrorMessage(strFiledName: Fields.FirstName.rawValue, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100))
            txtFldFirstName.setRequiredFildErrorStyle()
            return false;
        }
        
        /// Last Name Validation
        if (txtFldLastName.text?.isEmpty)!{
            Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.LastName.rawValue))
            txtFldLastName.setRequiredFildErrorStyle()
            return false;
        }
        else if( !Utility.checkLengthValidation(strInput: txtFldLastName.text, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100)){
           
            Utility.showErrorAlert( message: Message.getFieldLengthErrorMessage(strFiledName: Fields.LastName.rawValue, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100))
            txtFldLastName.setRequiredFildErrorStyle()
            return false;
        }
        
        /// Email Validation
        if (txtFldEmailId.text?.isEmpty)!{
            Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.EmailAddress.rawValue))
            txtFldEmailId.setRequiredFildErrorStyle()
            return false;
        }
        else if(!Utility.validateEmail(emailString:txtFldEmailId.text)){
            Utility.showErrorAlert( message: Message.getFieldInvalidInputErrorMessage(strFiledName: Fields.EmailAddress.rawValue))
            return false;
        }
        
        /// Phone Validation
        if (txtFldPhoneNumber.text?.isEmpty)!{
            Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.PhoneNumber.rawValue))
            txtFldPhoneNumber.setRequiredFildErrorStyle()
            return false;
        }
        else if(!Utility.validatePhone(phoneString:txtFldPhoneNumber.text!)){
            Utility.showErrorAlert( message: Message.getFieldInvalidInputErrorMessage(strFiledName: Fields.PhoneNumber.rawValue))
            return false;
        }
        
        /// Address Validation
        if (txtFldAddress.text?.isEmpty)!{
            Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.Address.rawValue))
            txtFldAddress.setRequiredFildErrorStyle()
            return false;
        }
        else if( !Utility.checkLengthValidation(strInput: txtFldAddress.text, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100)){
            Utility.showErrorAlert( message: Message.getFieldLengthErrorMessage(strFiledName: Fields.Address.rawValue, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100))
            return false;
        }
        
        /// City Validation
        if (txtFldCity.text?.isEmpty)!{
            Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.City.rawValue))
            txtFldCity.setRequiredFildErrorStyle()
            return false;
        }
        else if( !Utility.checkLengthValidation(strInput: txtFldCity.text, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100)){
            Utility.showErrorAlert( message: Message.getFieldLengthErrorMessage(strFiledName: Fields.City.rawValue, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100))
            return false;
        }
        
        /// State Validation
        if (txtFldState.text?.isEmpty)!{
            Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.State.rawValue))
            txtFldState.setRequiredFildErrorStyle()
            return false;
        }
        else if( !Utility.checkLengthValidation(strInput: txtFldState.text, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100)){
            Utility.showErrorAlert( message: Message.getFieldLengthErrorMessage(strFiledName: Fields.State.rawValue, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100))
            return false;
        }
        
        /// Zip Validation
        if (txtFldZip.text?.isEmpty)!{
            Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.Zip.rawValue))
            txtFldZip.setRequiredFildErrorStyle()
            return false;
        }
        else if( !Utility.checkLengthValidation(strInput: txtFldZip.text, minLength: MINLENGTH_5, maxLength: MAXLENGTH_5)){
            Utility.showErrorAlert( message: Message.getFieldLengthErrorMessage(strFiledName: Fields.Zip.rawValue, minLength: MINLENGTH_5, maxLength: MAXLENGTH_5))
            return false;
        }
        
        /// Company Name Validation
        if (txtFldCompanyName.text?.isEmpty)!{
            Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.CompanyName.rawValue))
            txtFldCompanyName.setRequiredFildErrorStyle()
            return false;
        }
        else if( !Utility.checkLengthValidation(strInput: txtFldCompanyName.text, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100)){
            Utility.showErrorAlert( message: Message.getFieldLengthErrorMessage(strFiledName: Fields.CompanyName.rawValue, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100))
            return false;
        }
        
        
        /// Website Validation
        if !strWebSite.isEmpty {
            if( !Utility.validateUrl(urlString: strWebSite)){
                Utility.showErrorAlert( message: Message.getFieldInvalidInputErrorMessage(strFiledName: Fields.Website.rawValue))
                return false;
            }
        }
        
        
        /// Tax Id Validation
        if (txtFldTaxID.text?.isEmpty)!{
            Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.TaxID.rawValue))
            txtFldTaxID.setRequiredFildErrorStyle()
            return false;
        }
        else if( !Utility.checkLengthValidation(strInput: txtFldTaxID.text, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100)){
            Utility.showErrorAlert( message: Message.getFieldLengthErrorMessage(strFiledName: Fields.TaxID.rawValue, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100))
            return false;
        }
        
        return true;
    }
    
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
