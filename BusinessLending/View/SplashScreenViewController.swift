//
//  SplashScreenViewController.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 7/4/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit

class SplashScreenViewController: UIViewController {

    @IBOutlet weak var activityInd: UIActivityIndicatorView!
    override func viewDidLoad() {
        self.view.backgroundColor = Utility.hexStringToUIColor(hex: VIEW_BACKGROUND_COLOR)
        activityInd.setDefaultThemeStyle()
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
//        appDelegate.loadLoginScreen()
          getApplicationAccess()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getApplicationAccess() {
       
        if (Reachability.isConnectedToNetwork()){
            
            
            let paramsDict: [String : Any] = ["client" : APPLICATION_ACCESS_PARTNERTYPE,
                                          "username" : APPLICATION_ACCESS_USERNAME,
                                          "password" : APPLICATION_ACCESS_PASSWORD,
                                          "realm" : APPLICATION_ACCESS_REALM
                                        ]
          
        
        Utility.requestWithPostURLWithData(strUrl: APPLICATION_ACCESS_URL, paramDict:paramsDict, strToken: "", completion: { (dataResponse, urlResponse, error) in
            DispatchQueue.main.sync {
                self.activityInd.stopAnimating()
                if error == nil{
                    
                    let httpResponse = urlResponse as! HTTPURLResponse
                    let responseString = String(data: dataResponse!, encoding: .utf8)
                    print("Res => " + responseString!)
                    let dict = Utility.convertToDictionary(text: responseString!)
                    
                    if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK && dict != nil){
                        ApplicationAccess.sharedInstance.isUserAlreadyLogIn = true
                        ApplicationAccess.sharedInstance.userId = dict?["userId"] as! String
                        ApplicationAccess.sharedInstance.partnerId = dict?["partnerId"] as! String
                        ApplicationAccess.sharedInstance.token = dict?["token"] as! String
                        
                        
                        
                        if !UserData.sharedInstance.isUserAlreadyLogIn {
                            appDelegate.loadLoginScreen()
                        }
                        else{
                            Utility.getWhoAmIData()
                            appDelegate.loadDashboard()
                        }
                        
                    }
                    else{
                        
                        if((dict?["message"]) != nil){
                            Utility.showErrorAlert( message: (dict?["message"] as? String)!)
                        }
                        else if((dict?["details"]) != nil){
                            let dictError = (dict?["details"] as![Dictionary<String,Any>])[0]
                            if((dictError["message"]) != nil){
                                Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                            }
                        }
                        else{
                            Utility.showErrorAlert( message: "Server error")
                        }
                    }
                }
                else{
                      Utility.showErrorAlert( message: (error?.localizedDescription)!)
                    
                }
            }
        })
        }
        else{
            self.activityInd.stopAnimating()
            Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
