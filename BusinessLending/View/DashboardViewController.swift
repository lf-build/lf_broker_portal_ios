//
//  DashboardViewController.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 6/22/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController, ENSideMenuDelegate, NewApplicationDelegate, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tblView: UITableView!

    @IBOutlet weak var btnNewApplication: UIButton!
//    @IBOutlet weak var piChartView: PieChartView!
    @IBOutlet weak var segDataType: UISegmentedControl!
//    var responseString:String = ""
    
     var responseDict:Dictionary<String,Any>!
    var arrayItems: [Dictionary<String, Any>] = []
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
//    @IBOutlet weak var chart: HorizontalBarChartView!
    
    
    var applicationStates: [String]!
    var values = [Double]()
    
    
    override func viewDidLoad() {
        self.sideMenuController()?.sideMenu?.delegate = self
        setUI()

        getData()
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    func reloadData() {
        getData()
    }

    func getData() {
        
        if (Reachability.isConnectedToNetwork()){
            // Create and add the view to the screen.
            self.view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            // All done!
//            let strBody = "partnerId=" + UserData.sharedInstance.partnerId
            
            let paramsDict: [String : Any] = ["partnerId" : UserData.sharedInstance.partnerId]
            Utility.requestWithPostURLWithData(strUrl: BASE_URL + DASHBOARD_COUNT_PATH, paramDict:paramsDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
                
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    if error == nil{
                        let httpResponse = urlResponse as! HTTPURLResponse
                        let responseString = String(data: dataResponse!, encoding: .utf8)!
                        self.responseDict = Utility.convertToDictionary(text: responseString)
                        
                        if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK && self.responseDict != nil){
                            self.arrayItems.removeAll()
//                            self.loadChart()
                             let dictDataForPeriod:Dictionary<String,Any>!
                            if self.segDataType.selectedSegmentIndex == 0 {
                                dictDataForPeriod = self.responseDict?["last30Days"] as! Dictionary<String,Any>!
                            }
                            else{
                               dictDataForPeriod = self.responseDict?["lifeTime"] as! Dictionary<String,Any>!
                            }
                            
                            
                            for (key, value) in dictDataForPeriod {
                                self.arrayItems.append(["applicationStatus":key, "value":value])
                            }
                            self.tblView.reloadData()
                        }
                        else{
                            if((self.responseDict?["message"]) != nil){
                                Utility.showErrorAlert( message: (self.responseDict?["message"] as? String)!)
                            }
                            else if((self.responseDict?["details"]) != nil){
                                let dictError = (self.responseDict?["details"] as![Dictionary<String,Any>])[0]
                                if((dictError["message"]) != nil){
                                    Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                }
                            }

                        }
                    }
                    else{
                        Utility.showErrorAlert( message: (error?.localizedDescription)!)
                        
                    }
                }
            })
        }
        else{
            self.activityIndicator.stopAnimating()
            Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
        }
        
    }
    @IBAction func segValueChanged(_ sender: Any) {
        let dictDataForPeriod:Dictionary<String,Any>!
        if segDataType.selectedSegmentIndex == 0 {
            dictDataForPeriod = responseDict?["last30Days"] as! Dictionary<String,Any>!
        }
        else{
            dictDataForPeriod = responseDict?["lifeTime"] as! Dictionary<String,Any>!
        }
      
        arrayItems.removeAll()
        for (key, value) in dictDataForPeriod {
            arrayItems.append(["applicationStatus":key, "value":value])
        }
        tblView.reloadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.sideMenuController()?.sideMenu?.delegate = self
    }
    @IBAction func toggleSideMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    
    // MARK: - ENSideMenu Delegate
    func sideMenuWillOpen() {
        
    }
    
    func sideMenuWillClose() {
        
    }
    
    func sideMenuShouldOpenSideMenu() -> Bool {
       
        return true
    }
    
    func sideMenuDidClose() {
        self.view.isUserInteractionEnabled = true
    }
    
    func sideMenuDidOpen() {
        self.view.isUserInteractionEnabled = false
    }
    
    
    func setUI(){
        
        self.navigationItem.title = "Dashboard"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : NAVIGATIONBAR_TINTCOLOR];
        
        self.navigationController?.navigationBar.tintColor = NAVIGATIONBAR_TINTCOLOR
        
        let button = UIButton.init(type: .custom)
       button.setMenuButtonStyle()
        button.addTarget(self, action:#selector(SettingViewController.toggleSideMenu(sender:)), for: UIControlEvents.touchUpInside)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        self.view.backgroundColor = Utility.hexStringToUIColor(hex: VIEW_BACKGROUND_COLOR)
        btnNewApplication.setDefaultThemeStyle()
        activityIndicator.setDefaultThemeStyle()
        segDataType.tintColor = Utility.hexStringToUIColor(hex: THEME_COLOR)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        let dictionaryA = [
//            "name": "amit",
//            "company": "sigma",
//            "employeecode": "773",
//            ]
//        let dictionaryB = [
//            "name": "sehul",
//            "company": "sigma",
//            "employeecode": "554",
//            ]
//        let arrayData = [dictionaryA,dictionaryB]
//        let objSecondVC =  segue.destination as! SettingViewController
//        objSecondVC.arrayData = arrayData;
    }
   
    @IBAction func btnNewApplicationClicked(_ sender: Any) {
        if (self.sideMenuController()?.sideMenu?.isMenuOpen)!{
            self.sideMenuController()?.sideMenu?.toggleMenu()
        }
        var mainView: UIStoryboard!
        mainView = UIStoryboard(name: "Main", bundle: nil)
        let objNewApplicationVC : NewApplicationViewController = mainView.instantiateViewController(withIdentifier: "NewApplicationViewController") as! NewApplicationViewController
        objNewApplicationVC.objDelegate = self
        self.navigationController?.pushViewController(objNewApplicationVC, animated: true)
    }
    
    @IBAction func unwindToDashboard(segue:UIStoryboardSegue) {
        if let sourceViewController = segue.source as? NewApplicationViewController {
            print(sourceViewController.dictNewApplicationDetails)
            self.getApplicationData(dictApplicationData: sourceViewController.dictNewApplicationDetails)
        }
    }
    
    func getApplicationData(dictApplicationData:Dictionary<String,Any>) {
        if (Reachability.isConnectedToNetwork()){
            // Create and add the view to the screen.
            self.view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            
            let arraySearchField:[String]=["ApplicationNumber","BusinessApplicantName","LegalBusinessName","StatusName"]
            // All done!
            var paramsDict: [String : Any] = [:]
            paramsDict = ["page" : 1,
                          "searchFields" : arraySearchField,
                          "sortBy" : "LastProgressDate.Time.Ticks",
                          "raw" : ["PartnerId":UserData.sharedInstance.partnerId],
                          "searchText" : dictApplicationData["applicationNumber"] ?? ""
            ]
            
            
            Utility.requestWithPostURLWithData(strUrl: BASE_URL + DASHBOARD_GRID_PATH, paramDict: paramsDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
                
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    if error == nil{
                        let httpResponse = urlResponse as! HTTPURLResponse
                        let responseString = String(data: dataResponse!, encoding: .utf8)!
                        let dictData = Utility.convertToDictionary(text: responseString)
                        
                        if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK && (dictData != nil)){
                            if !responseString.isEmpty {
                               self.getData()
                                
                                let arrayItems = dictData?["items"] as! [Dictionary<String, Any>]
                                self.redirectToVerificationPage(dictApplicationDetails: arrayItems[0])
                            }
                        }
                        else{
                            self.activityIndicator.stopAnimating()
                            if((dictData?["message"]) != nil){
                                Utility.showErrorAlert( message: (dictData?["message"] as? String)!)
                            }
                            else if((dictData?["details"]) != nil){
                                let dictError = (dictData?["details"] as![Dictionary<String,Any>])[0]
                                if((dictError["message"]) != nil){
                                    Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                }
                            }
                            
                        }
                    }
                    else{
                        
                        Utility.showErrorAlert(message: (error?.localizedDescription)!)
                        
                    }
                }
            })
        }
        else{
            self.activityIndicator.stopAnimating()
            Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
        }
    }
    func redirectToVerificationPage(dictApplicationDetails:Dictionary<String,Any>) {
        var mainView: UIStoryboard!
        mainView = UIStoryboard(name: "Main", bundle: nil)
        let objApplicationVerificationVC : ApplicationVerificationViewController = mainView.instantiateViewController(withIdentifier: "ApplicationVerificationView") as! ApplicationVerificationViewController
        objApplicationVerificationVC.dictApplicationDetails = dictApplicationDetails
        self.navigationController?.pushViewController(objApplicationVerificationVC, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return arrayItems.count
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dicData = arrayItems[indexPath.row]
        let applicationStatus = (dicData["applicationStatus"] as! String).lowercased()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ApplicationCell", for: indexPath) as! DashboardTableViewCell
        cell.lblApplicationType.text = "\((dicData["applicationStatus"] as! String).capitalized)"
       cell.imgView.image = UIImage(named: dicData["applicationStatus"] as! String)
        
        
        
        if (applicationStatus == "commission") {
            cell.lblNoOfApplication.text =  String(format:"$%.2f",dicData["value"] as! Float)
            cell.borderView.backgroundColor = Utility.hexStringToUIColor(hex: BORDER_GREEN_COLOR)
            cell.lblApplicationType.text = "Commission Earned"
            return cell
        }
        else{
          
             cell.lblNoOfApplication.text = "\(dicData["value"] as! Int)"
            cell.borderView.backgroundColor = Utility.hexStringToUIColor(hex: BORDER_RED_COLOR)
            
            return cell
        }
    }
}

