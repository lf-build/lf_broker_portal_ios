//
//  BankDetailsViewController.swift
//  Business Lending
//
//  Created by Amitkumar Patel on 6/27/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit

class BankDetailsViewController: UITableViewController, ENSideMenuDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate, KeyboardToolbarDelegate{

    @IBOutlet weak var txtFldBankName: UITextField!
    @IBOutlet weak var txtFldBankAccountName: UITextField!
    @IBOutlet weak var txtFldBankAccountNumber: UITextField!
    @IBOutlet weak var txtFldAccountType: UITextField!
    @IBOutlet weak var txtFldABA: UITextField!
    var activeTextField = UITextField()
    let pickerView = UIPickerView()
    var pickOption = ["Saving","Checking"]
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()

    var responseDict:Dictionary<String,Any> = [:]
    var objKeyboardToolbar: KeyboarToolbar = KeyboarToolbar.sharedInstance
    
    override func viewDidLoad() {
        objKeyboardToolbar.objKeyboardToolbarDelegate = self
        self.sideMenuController()?.sideMenu?.delegate = self
        setUI()
        getData()
        
        pickerView.showsSelectionIndicator = true
        pickerView.delegate = self
        pickerView.dataSource = self
        
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = BLACK_TINTCOLOR
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(LogInViewController.donePicker))
        doneButton.tintColor = UIColor.black
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(LogInViewController.cancelPicker))
        cancelButton.tintColor = UIColor.black
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        txtFldAccountType.inputView = pickerView
        txtFldAccountType.inputAccessoryView = toolBar
        
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    
    
    func getData() {
        if (Reachability.isConnectedToNetwork()){
            // Create and add the view to the screen.
            self.view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            // All done!
            //            let strBody = "partnerId=" + UserData.sharedInstance.partnerId
            
            let paramsDict: [String : Any] = ["partnerId" : UserData.sharedInstance.partnerId]
            Utility.requestWithPostURLWithData(strUrl: BASE_URL + BANKS_DETAILS_PATH, paramDict:paramsDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
                
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    if error == nil{
                        let httpResponse = urlResponse as! HTTPURLResponse
                        let responseString = String(data: dataResponse!, encoding: .utf8)!
                         self.responseDict = Utility.convertToDictionary(text: responseString)!
                        
                        if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK && self.responseDict.count > 0){
                            self.txtFldBankName.text = self.responseDict["bankName"] as? String
                            self.txtFldBankAccountName.text = self.responseDict["bankAccountName"] as? String
                            self.txtFldBankAccountNumber.text = self.responseDict["bankAccountNumber"] as? String
                            let accountType = self.responseDict["accountType"] as? String
                            self.txtFldAccountType.text = self.pickOption[Int(accountType!)! - 1]
                            self.txtFldABA.text = self.responseDict["aba"] as? String
                        }
                        else{
                            if((self.responseDict["message"]) != nil){
                                Utility.showErrorAlert( message: (self.responseDict["message"] as? String)!)
                            }
                            else if((self.responseDict["details"]) != nil){
                                let dictError = (self.responseDict["details"] as![Dictionary<String,Any>])[0]
                                if((dictError["message"]) != nil){
                                    Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                }
                            }
                        }
                    }
                    else{
                        Utility.showErrorAlert(message: (error?.localizedDescription)!)
                        
                    }
                }
            })
        }
        else{
            self.activityIndicator.stopAnimating()
            Utility.showErrorAlert(message: NO_INTERNET_MESSAGE)
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.sideMenuController()?.sideMenu?.delegate = self
    }
    
    // MARK: - ENSideMenu Delegate
    @IBAction func toggleSideMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    func sideMenuWillOpen() {
        self.view.endEditing(true)
    }
    
    func sideMenuWillClose() {
        
    }
    
    func sideMenuDidClose() {
        self.view.isUserInteractionEnabled = true
    }
    
    func sideMenuDidOpen() {
        self.view.isUserInteractionEnabled = false
    }
    
    
    func sideMenuShouldOpenSideMenu() -> Bool {
        
        return true
    }
    
    func setUI() {
        
        self.navigationItem.title = "Bank Details"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : NAVIGATIONBAR_TINTCOLOR];
        
        self.navigationController?.navigationBar.tintColor = NAVIGATIONBAR_TINTCOLOR
        let button = UIButton.init(type: .custom)
       button.setMenuButtonStyle()
        button.addTarget(self, action:#selector(SettingViewController.toggleSideMenu(sender:)), for: UIControlEvents.touchUpInside)
      
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        self.view.backgroundColor = Utility.hexStringToUIColor(hex: VIEW_BACKGROUND_COLOR)
        activityIndicator.setDefaultThemeStyle()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if (section == tableView.numberOfSections-1) {
            return 100.0
        }
        return 0.0
    }
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if (section == tableView.numberOfSections-1) {
            let btnUpdate = UIButton()
            let footerView = UIView()
            footerView.frame = CGRect(x: 0.0, y: 0.0, width: self.view.bounds.size.width, height: 100.0)
            
            
            btnUpdate.setTitle("Update", for: .normal)
            btnUpdate.frame = CGRect(x: 8.0, y: 30.0, width: self.view.bounds.size.width-16.0, height: 40.0)
            btnUpdate.addTarget(self, action: #selector(BankDetailsViewController.btnUpdateClicked(_:)), for: .touchUpInside)
            btnUpdate.setDefaultThemeStyle()
            footerView.addSubview(btnUpdate)
            return footerView
        }
        let footerView = UIView()
        return footerView
    }
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        guard let header = view as? UITableViewHeaderFooterView else { return }
        header.textLabel?.font = UIFont.boldSystemFont(ofSize: CGFloat(TABLEVIEW_HEADERTITLE_FONT_SIZE))
        header.textLabel?.frame = header.frame
        header.textLabel?.textAlignment = .left
        header.textLabel?.textColor = UIColor.black
        
        if section == 0 {
            header.textLabel?.font = UIFont.fontAwesome(ofSize: (header.textLabel?.font.pointSize)!)
            header.textLabel?.text = (String.fontAwesomeIcon(code: FAICON_BANK_DETAILS)! + "\t" + (header.textLabel?.text)!)
        }
        
    }
    
    @IBAction func btnUpdateClicked(_ sender: Any) {
        if (self.sideMenuController()?.sideMenu?.isMenuOpen)!{
            return;
        }
        if validateForm() {
            self.view.endEditing(true)
            if (Reachability.isConnectedToNetwork()){
                // Create and add the view to the screen.
                self.view.addSubview(activityIndicator)
                activityIndicator.startAnimating()
                // All done!
                //            let strBody = "partnerId=" + UserData.sharedInstance.partnerId
                
                //                {"":"PER-0000000007","":"0ad59d87069e4e3b91f13d5e80c4da9d","":"HDFC","":"Sorjat","":"111","":"112","":"2"}
                
                let paramsDict: [String : Any] = ["partnerId" : UserData.sharedInstance.partnerId,
                                                  "bankId" : self.responseDict["bankId"] ?? "" ,
                                                  "bankName" : txtFldBankName.text!,
                                                  "bankAccountName" : txtFldBankAccountName.text!,
                                                  "bankAccountNumber" : txtFldBankAccountNumber.text!,
                                                  "aba" : txtFldABA.text!,
                                                  "accountType" : "\(pickOption.index(of: txtFldAccountType.text!)! + 1 )"
                ]
                Utility.requestWithPostURLWithData(strUrl: BASE_URL + SET_BANK_DETAILS_PATH, paramDict:paramsDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
                    
                    DispatchQueue.main.async {
                        self.activityIndicator.stopAnimating()
                        if error == nil{
                            let httpResponse = urlResponse as! HTTPURLResponse
                            let responseString = String(data: dataResponse!, encoding: .utf8)!
                            let dict = Utility.convertToDictionary(text: responseString)
                            
                            if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK && dict != nil){
                                
                                // ×
                                Utility.showSuccessAlert(message: "×Bank details updated successfully")
                                
                                self.txtFldBankName.text = dict?["bankName"] as? String
                                self.txtFldBankAccountName.text = dict?["bankAccountName"] as? String
                                self.txtFldBankAccountNumber.text = dict?["bankAccountNumber"] as? String
                                let accountType = dict?["accountType"] as? String
                                self.txtFldAccountType.text = self.pickOption[Int(accountType!)! - 1]
                                self.txtFldABA.text = dict?["aba"] as? String

                            }
                            else{
                                if((dict?["message"]) != nil){
                                    Utility.showErrorAlert( message: (dict?["message"] as? String)!)
                                }
                                else if((dict?["details"]) != nil){
                                    let dictError = (dict?["details"] as![Dictionary<String,Any>])[0]
                                    if((dictError["message"]) != nil){
                                        Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                    }
                                }
                            }
                        }
                        else{
                            Utility.showErrorAlert( message: (error?.localizedDescription)!)
                            
                        }
                    }
                })
            }
            else{
                self.activityIndicator.stopAnimating()
                Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
            }

        }
    }
    
    func validateForm() -> Bool {
        
        /// BankName Validation
        if (txtFldBankName.text?.isEmpty)!{
            Utility.showErrorAlert(message: Message.getBlankFieldErrorMessage(strFiledName: Fields.BankName.rawValue) )
            txtFldBankName.setRequiredFildErrorStyle()
            return false;
        }
        else if( !Utility.checkLengthValidation(strInput: txtFldBankName.text, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100)){
            Utility.showErrorAlert(message: Message.getFieldLengthErrorMessage(strFiledName: Fields.BankName.rawValue, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100))
    
            return false;
        }
        
        /// BankAccountName Validation
        if (txtFldBankAccountName.text?.isEmpty)!{
            Utility.showErrorAlert(message: Message.getBlankFieldErrorMessage(strFiledName: Fields.BankAccountName.rawValue))
            txtFldBankAccountName.setRequiredFildErrorStyle()
            return false;
        }
        else if( !Utility.checkLengthValidation(strInput: txtFldBankAccountName.text, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100)){
            
            Utility.showErrorAlert(message: Message.getFieldLengthErrorMessage(strFiledName: Fields.BankAccountName.rawValue, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100))
            txtFldBankAccountName.setRequiredFildErrorStyle()
            return false;
        }
        
        
        
        /// BankAccountNumber Validation
        if (txtFldBankAccountNumber.text?.isEmpty)!{
            Utility.showErrorAlert(message: Message.getBlankFieldErrorMessage(strFiledName: Fields.BankAccountNumber.rawValue))
            txtFldBankAccountNumber.setRequiredFildErrorStyle()
            return false;
        }
        else if( !Utility.checkLengthValidation(strInput: txtFldBankAccountNumber.text, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100)){
            Utility.showErrorAlert(message: Message.getFieldLengthErrorMessage(strFiledName: Fields.BankAccountNumber.rawValue, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100))
            txtFldBankAccountNumber.setRequiredFildErrorStyle()
            return false;
        }
        
        /// Company Name Validation
        if (txtFldAccountType.text?.isEmpty)!{
            Utility.showErrorAlert(message: Message.getBlankFieldErrorMessage(strFiledName: Fields.AccountType.rawValue))
            txtFldAccountType.setRequiredFildErrorStyle()
            return false;
        }
        
        
        
        
        /// Street Validation
        if (txtFldABA.text?.isEmpty)!{
            Utility.showErrorAlert(message: Message.getBlankFieldErrorMessage(strFiledName: Fields.ABA.rawValue))
            txtFldABA.setRequiredFildErrorStyle()
            return false;
        }
        else if( !Utility.checkLengthValidation(strInput: txtFldABA.text, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100)){
            Utility.showErrorAlert(message: Message.getFieldLengthErrorMessage(strFiledName: Fields.ABA.rawValue, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100))
            return false;
        }
        
        
        return true;
    }
    
    
    
    
    // MARK: - UITextField Delegate Method
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if (self.sideMenuController()?.sideMenu?.isMenuOpen)!{
            return false;
        }
        
        if textField.inputAccessoryView == nil {
            textField.inputAccessoryView = objKeyboardToolbar
        }
        activeTextField = textField
        

        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.isEqual(txtFldAccountType) {
            if !(txtFldAccountType.text?.isEmpty)! {
                pickerView.selectRow(pickOption.index(of: txtFldAccountType.text!)!, inComponent: 0, animated: true)
            }
        }
    }
    func keyboardToolbarNextButtonClicked() {
        switch activeTextField {
        case txtFldBankName:
            txtFldBankAccountName.becomeFirstResponder()
            break
        case txtFldBankAccountName:
            txtFldBankAccountNumber.becomeFirstResponder()
            break
        case txtFldBankAccountNumber:
            txtFldAccountType.becomeFirstResponder()
            break
        case txtFldAccountType:
            txtFldABA.becomeFirstResponder()
            break
        case txtFldABA:
            txtFldABA.resignFirstResponder()
            break
        default:
            self.view.endEditing(true)
            break
        }

        
    }
    func keyboardToolbarPreviousButtonClicked() {
        switch activeTextField {
        case txtFldABA:
            txtFldAccountType.becomeFirstResponder()
            break
        case txtFldAccountType:
            txtFldBankAccountNumber.becomeFirstResponder()
            break
        case txtFldBankAccountNumber:
            txtFldBankAccountName.becomeFirstResponder()
            break
        case txtFldBankAccountName:
            txtFldBankName.becomeFirstResponder()
            break
        case txtFldBankName:
            txtFldBankName.becomeFirstResponder()
            break
        default:
            self.view.endEditing(true)
            break
        }

    }
    func keyboardToolbarHideKeyboard() {
        self.view.endEditing(true)
    }
    
   
    
    
    // MARK: - UIPickerView Delegate Method
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickOption[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
    func cancelPicker(){
        txtFldAccountType.resignFirstResponder()
    }
    func donePicker(){
        txtFldAccountType.text = pickOption[pickerView.selectedRow(inComponent: 0)]
        txtFldAccountType.resignFirstResponder()
        txtFldABA.becomeFirstResponder()
    }


}
