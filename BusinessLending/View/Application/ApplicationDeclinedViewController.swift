//
//  ApplicationDeclinedViewController.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 7/14/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit

class ApplicationDeclinedViewController: UIViewController {
    var dictApplicationDetails:Dictionary<String,Any>!
    
    @IBOutlet weak var lblApplicantName: UILabel!
    @IBOutlet weak var lblBusinessName: UILabel!
    @IBOutlet weak var lblLoanAmount: UILabel!
    @IBOutlet weak var lblRepaymentAmount: UILabel!
    @IBOutlet weak var lblSellAmount: UILabel!
    @IBOutlet weak var lblTerm: UILabel!
    @IBOutlet weak var lblCommission: UILabel!
    
    @IBOutlet weak var lblApplicationReason: UILabel!
    @IBOutlet weak var backgrounfView: UIView!
    @IBOutlet weak var applicationStatusBGView: UIView!
    @IBOutlet weak var applicationDetailsBGView: UIView!
    
    @IBOutlet weak var applicationResonView: UIView!
    
    @IBOutlet weak var lblResonHeighConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var viewReasonHeightConstraint: NSLayoutConstraint!
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    override func viewDidLoad() {
        self.title = dictApplicationDetails["StatusName"] as? String
       
        applicationStatusBGView.layer.borderWidth = 0.0
        applicationStatusBGView.layer.borderColor = Utility.hexStringToUIColor(hex: GRAY_COLOR).cgColor
        applicationStatusBGView.layer.masksToBounds = true
        applicationStatusBGView.layer.cornerRadius = 4.0
        
        applicationDetailsBGView.layer.borderWidth = 0.0
        applicationDetailsBGView.layer.borderColor = Utility.hexStringToUIColor(hex: GRAY_COLOR).cgColor
        applicationDetailsBGView.layer.masksToBounds = true
        applicationDetailsBGView.layer.cornerRadius = 4.0
        
        applicationResonView.layer.borderWidth = 0.0
        applicationResonView.layer.borderColor = Utility.hexStringToUIColor(hex: GRAY_COLOR).cgColor
        applicationResonView.layer.masksToBounds = true
        applicationResonView.layer.cornerRadius = 4.0
        
        backgrounfView.backgroundColor = UIColor.clear
        
        self.view.backgroundColor = Utility.hexStringToUIColor(hex: VIEW_BACKGROUND_COLOR)
        activityIndicator.setDefaultThemeStyle()
        getData()
        
        
        applicationStatusBGView.isHidden = true
        applicationDetailsBGView.isHidden = true
        applicationResonView.isHidden = true

        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func getData() {
        
        if (Reachability.isConnectedToNetwork()){
            // Create and add the view to the screen.
            self.view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            
           // All done!
            
            let paramsDict = ["applicationNumber":dictApplicationDetails["ApplicationNumber"] ?? ""] as Dictionary<String,Any>
            
            
            
            
            Utility.requestWithPostURLWithData(strUrl: BASE_URL + GET_DECLINED, paramDict: paramsDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
                
                DispatchQueue.main.async {
                    
                    self.applicationStatusBGView.isHidden = false
                    self.applicationDetailsBGView.isHidden = false
                    self.lblApplicantName.text = self.dictApplicationDetails["BusinessApplicantName"] as? String
                    self.lblBusinessName.text = self.dictApplicationDetails["LegalBusinessName"] as? String
                    self.lblLoanAmount.text = String(format:"$%.2f",self.dictApplicationDetails["LoanAmount"] as! Float)
                    self.lblRepaymentAmount.text = String(format:"$%.2f",self.dictApplicationDetails["RepaymentAmount"] as! Float)
                    self.lblSellAmount.text = String(format:"%.2f",self.dictApplicationDetails["SellRate"] as! Float)
                    self.lblTerm.text = String(format:"%.2f",self.dictApplicationDetails["Term"] as! Float)
                    self.lblCommission.text = String(format:"%.2f",self.dictApplicationDetails["CommissionAmount"] as! Float)
                   
                    self.activityIndicator.stopAnimating()
                    if error == nil{
                        let httpResponse = urlResponse as! HTTPURLResponse
                        let responseString = String(data: dataResponse!, encoding: .utf8)!
                        let responseDict = Utility.convertToDictionary(text: responseString)
                        
                        if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK && responseDict != nil){
                            if !responseString.isEmpty {
                                if((responseDict?["declineReasons"]) != nil){
                                    let arrayReson = responseDict?["declineReasons"] as! [Any]
                                    if (arrayReson.count > 0) {
                                        let rejectionReson = arrayReson[0] as! Dictionary<String, Any>
                                        self.lblApplicationReason.text = "\(rejectionReson["reason"] as! String) : \(rejectionReson["subReason"] as! String)"
                                        self.lblApplicationReason.sizeToFit()
                                        self.lblApplicationReason.setNeedsDisplay()
                                        self.lblApplicationReason.backgroundColor = .clear
                                        let lblHeight:CGFloat = self.lblApplicationReason.frame.size.height;
                                        print("Lbl Height => \(lblHeight)")

                                        self.viewReasonHeightConstraint.constant = lblHeight + 60
                                        self.lblResonHeighConstraint.constant = lblHeight
                                        if ((self.lblApplicationReason.text?.characters.count)! > 0) {
                                            self.applicationResonView.isHidden = false
                                        }
                                    }
                                }
                                
                            }
                        }
                        else{
                            self.activityIndicator.stopAnimating()
                   
                            if((responseDict?["message"]) != nil){
                                Utility.showErrorAlert( message: (responseDict?["message"] as? String)!)
                            }
                            else if((responseDict?["details"]) != nil){
                                let dictError = (responseDict?["details"] as![Dictionary<String,Any>])[0]
                                if((dictError["message"]) != nil){
                                    Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                }
                            }
                            
                        }
                    }
                    else{
                       
                        self.activityIndicator.stopAnimating()
                        Utility.showErrorAlert(message: (error?.localizedDescription)!)
                        
                    }
                }
            })
        }
        else{
            self.activityIndicator.stopAnimating()
            Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
