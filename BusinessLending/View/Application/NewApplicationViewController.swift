//
//  NewApplicationViewController.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 7/6/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit
import ActiveLabel
protocol NewApplicationDelegate: class {
    func reloadData()
}
class NewApplicationViewController: UITableViewController,UITextFieldDelegate , UIPickerViewDataSource, UIPickerViewDelegate,KeyboardToolbarDelegate {

    
    var activeTextField = UITextField()
    // MARK: - Section 1 Textfield
    @IBOutlet weak var txtFldFirstName: UITextField!
    @IBOutlet weak var txtFldLastName: UITextField!
    @IBOutlet weak var txtFldSSN: UITextField!
    @IBOutlet weak var txtFldPrimaryPhone: UITextField!
    @IBOutlet weak var txtFldSecondaryPhone: UITextField!
    @IBOutlet weak var txtFldEmailAddress: UITextField!
    @IBOutlet weak var txtFldHomeAddress: UITextField!
    @IBOutlet weak var txtFldCity: UITextField!
    @IBOutlet weak var txtFldState: UITextField!
    @IBOutlet weak var txtFldZipCode: UITextField!
    @IBOutlet weak var txtFldDateOfBirth: UITextField!
    
    // MARK: - Section 2 Textfield
    @IBOutlet weak var txtFldBusinessLegalName: UITextField!
    @IBOutlet weak var txtFldDBA: UITextField!
    @IBOutlet weak var txtFldEIN: UITextField!
    @IBOutlet weak var txtFldBusinessAddress: UITextField!
    @IBOutlet weak var txtFldBusinessCity: UITextField!
    @IBOutlet weak var txtFldBusinessState: UITextField!
    @IBOutlet weak var txtFldBusinessZipCode: UITextField!
    @IBOutlet weak var txtFldOfficePhone: UITextField!
    
    // MARK: - Section 3 Textfield
    @IBOutlet weak var txtFldRequestedAmount: UITextField!
    
    var dictDraftedApplication:Dictionary<String, Any>!
    var checkbox: VKCheckbox!
    let datePickerView = UIDatePicker()
     let statePickerView = UIPickerView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var btnSaveDraft:UIButton!
    var btnNext:UIButton!
    var consents:Bool = false
    weak var objDelegate: NewApplicationDelegate? = nil
    var objKeyboardToolbar: KeyboarToolbar = KeyboarToolbar.sharedInstance
    var dictNewApplicationDetails: Dictionary<String, Any>!
    
    override func viewDidLoad() {
        objKeyboardToolbar.objKeyboardToolbarDelegate = self
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = BLACK_TINTCOLOR
        toolBar.sizeToFit()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        tapGesture.cancelsTouchesInView = true
        tableView.addGestureRecognizer(tapGesture)
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.donePicker))
        doneButton.tintColor = UIColor.black
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelPicker))
        cancelButton.tintColor = UIColor.black
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.maximumDate = Date()
        txtFldDateOfBirth.inputView = datePickerView
        txtFldDateOfBirth.inputAccessoryView = toolBar
        
        txtFldState.placeholder = "State"
        txtFldBusinessState.placeholder = "State"

        addStatePicker()
        
        if (dictDraftedApplication != nil) {
            getApplicationData()
        }
        
        setUI()
        txtFldRequestedAmount.setleftViewString(strText: CURRENCY_SIGN)
        super.viewDidLoad()
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    
    
    func getApplicationData() {
        if (Reachability.isConnectedToNetwork()){
            // Create and add the view to the screen.
            self.view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            
            
            
            var paramsDict: [String : Any] = [:]
            
            
                paramsDict = ["applicationNumber" : dictDraftedApplication["ApplicationNumber"] ?? ""]
           
            
            Utility.requestWithPostURLWithData(strUrl: BASE_URL + GET_APPLICATION_DETAILS_PATH, paramDict: paramsDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
                
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    if error == nil{
                        let httpResponse = urlResponse as! HTTPURLResponse
                        let responseString = String(data: dataResponse!, encoding: .utf8)!
                        let dict = Utility.convertToDictionary(text: responseString)
                        
                        if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK && dict != nil){
                            if !responseString.isEmpty {
                                let dict = Utility.convertToDictionary(text: responseString)
                                if ((dict?["owners"]) != nil){
                                    let arrayOwner = dict?["owners"] as! Array<Dictionary<String,Any>>
                                    self.txtFldFirstName.text = arrayOwner[0]["firstName"] as? String
                                    self.txtFldLastName.text = arrayOwner[0]["lastName"] as? String
                                    self.txtFldSSN.text = arrayOwner[0]["ssn"] as? String
                                    self.txtFldPrimaryPhone.text = (arrayOwner[0]["primaryPhone"] as? String)?.formatePhoneNumber()
                                    self.txtFldSecondaryPhone.text = (arrayOwner[0]["secondaryPhone"] as? String)?.formatePhoneNumber()
                                    self.txtFldEmailAddress.text = arrayOwner[0]["emailAddress"] as? String
                                    self.txtFldHomeAddress.text = arrayOwner[0]["homeAddress"] as? String
                                    self.txtFldCity.text = arrayOwner[0]["city"] as? String
                                    
                                    if ((arrayOwner[0]["state"] as? String) != nil){
                                        self.txtFldState.text = arrayState[self.indexOfState( value: (arrayOwner[0]["state"] as? String)!)]["stateDisplayName"]
                                    }
                                    
                                    
                                    
                                    self.txtFldZipCode.text = arrayOwner[0]["zipCode"] as? String
                                    self.txtFldDateOfBirth.text = arrayOwner[0]["dateOfBirth"] as? String
                                }
                                self.txtFldBusinessLegalName.text = dict?["businessLegalName"] as? String
                                self.txtFldDBA.text = dict?["dba"] as? String
                                self.txtFldEIN.text = dict?["ein"] as? String
                                self.txtFldBusinessAddress.text = dict?["businessAddress"] as? String
                                self.txtFldBusinessCity.text = dict?["city"] as? String
                                
                                var strSate:String = ""
                                
    
                                if let title = dict?["state"] as? String
                                {
                                    strSate = title
                                }
                                else
                                {
                                    strSate = ""
                                }
                                if (strSate.characters.count > 0){
                                    
                               
                                    
                                     self.txtFldBusinessState.text = arrayState[self.indexOfState(value: strSate)]["stateDisplayName"]
                                }
                                
                               
                                self.txtFldBusinessZipCode.text = dict?["zipCode"] as? String
                                self.txtFldOfficePhone.text = (dict?["officePhone"] as? String)?.formatePhoneNumber()
                                self.txtFldRequestedAmount.text = "\(dict?["requestedAmount"] ?? "0")"
                                
                                self.consents = dict?["consents"] as! Bool
                                
                            }
                        }
                        else{
                            self.activityIndicator.stopAnimating()
                            if((dict?["message"]) != nil){
                                Utility.showErrorAlert( message: (dict?["message"] as? String)!)
                            }
                            else if((dict?["details"]) != nil){
                                let dictError = (dict?["details"] as![Dictionary<String,Any>])[0]
                                if((dictError["message"]) != nil){
                                    Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                }
                            }
                            
                        }
                    }
                    else{
                        self.activityIndicator.stopAnimating()
                        Utility.showErrorAlert(message: (error?.localizedDescription)!)
                        
                    }
                }
            })
        }
        else{
            self.activityIndicator.stopAnimating()
            Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
        }
    }
    
    
    func addStatePicker() {
        statePickerView.showsSelectionIndicator = true
        statePickerView.delegate = self
        statePickerView.dataSource = self
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = BLACK_TINTCOLOR
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.doneStatePicker))
        doneButton.tintColor = UIColor.black
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelStatePicker))
        cancelButton.tintColor = UIColor.black
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        txtFldState.inputView = statePickerView
        txtFldState.inputAccessoryView = toolBar
        
        txtFldBusinessState.inputView = statePickerView
        txtFldBusinessState.inputAccessoryView = toolBar
    }
    
    func setUI() {
        
        self.navigationItem.title = "New Application"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : NAVIGATIONBAR_TINTCOLOR];
        self.navigationController?.navigationBar.tintColor = NAVIGATIONBAR_TINTCOLOR
       
        
        self.view.backgroundColor = Utility.hexStringToUIColor(hex: VIEW_BACKGROUND_COLOR)
        activityIndicator.setDefaultThemeStyle()
        activityIndicator.center = self.view.center
//        self.navigationController?.navigationBar.topItem?.title = "Back"
        
       
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segWebView" {
            let navCtrlr = segue.destination as! UINavigationController
            let objWebCtrlr:WebViewController = navCtrlr.viewControllers[0] as! WebViewController
            if sender as! String == "Terms of Service" {
                objWebCtrlr.strURL = Terms_of_Service_URL
            }
            if sender as! String == "Privacy Policy" {
                objWebCtrlr.strURL = Privacy_Policy_URL
            }
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btnDiscardClicked(_ sender: Any) {
        self.dismiss(animated: true) { 
            
        }
    }

    @IBAction func btnDoneClicked(_ sender: Any) {
        self.dismiss(animated: true) {
            
        }
    }
    
    
    
    // MARK: - Table view Delegate
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if (section == tableView.numberOfSections-1) {
            return 200.0
        }
        return 0.0
    }
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if (section == tableView.numberOfSections-1) {
            
            let footerView = UIView()
            footerView.frame = CGRect(x: 0.0, y: 0.0, width: self.view.bounds.size.width, height: 150.0)
            
            
            checkbox =  VKCheckbox.init(frame: CGRect(x: 8, y: 30, width: 30, height: 30))
            footerView.addSubview(checkbox)
            
            checkbox.checkboxValueChangedBlock = {
                isOn in
            
            }
            if (self.consents == true) {
                checkbox.setOn(true, animated: true)
            }
            else{
                checkbox.setOn(false, animated: true)
            }
            
            let label = ActiveLabel()
            label.frame = CGRect(x: 50, y: 30, width: self.view.bounds.size.width - 60, height: 40)
            label.numberOfLines = 0
             let customTypeTermOfService = ActiveType.custom(pattern: "Terms of Service")
            let customTypePrivacyPolicy = ActiveType.custom(pattern: "Privacy Policy")
            label.enabledTypes = [.mention, .hashtag, .url, customTypeTermOfService, customTypePrivacyPolicy]
            label.customColor[customTypeTermOfService] = UIColor.blue
            label.customSelectedColor[customTypeTermOfService] = UIColor.purple
//
            label.customColor[customTypePrivacyPolicy] = UIColor.blue
            label.customSelectedColor[customTypePrivacyPolicy] = UIColor.purple
            
            label.font = UIFont.systemFont(ofSize: 15)
            label.text = "I agree to the Terms of Service & Privacy Policy."
            label.textColor = .black
            label.handleCustomTap(for: customTypeTermOfService) { element in
        
                self.performSegue(withIdentifier: "segWebView", sender: element)
            }
            label.handleCustomTap(for: customTypePrivacyPolicy) { element in
                
                self.performSegue(withIdentifier: "segWebView", sender: element)
            }
            footerView.addSubview(label)
            
            btnSaveDraft = UIButton()
            btnSaveDraft.setTitle("Save Draft", for: .normal)
            btnSaveDraft.frame = CGRect(x: 8.0, y: 80.0, width: self.view.bounds.size.width-16.0, height: 40.0)
            btnSaveDraft.addTarget(self, action: #selector(self.btnSaveDraftClicked(_:)), for: .touchUpInside)
            btnSaveDraft.setDefaultThemeStyle()
            footerView.addSubview(btnSaveDraft)
            
            print("Table Reload called")
            
            btnNext = UIButton()
            btnNext.setTitle("Next", for: .normal)
            btnNext.frame = CGRect(x: 8.0, y: 130.0, width: self.view.bounds.size.width-16.0, height: 40.0)
            btnNext.addTarget(self, action: #selector(self.btnNextClicked(_:)), for: .touchUpInside)
            btnNext.setDefaultThemeStyle()
            footerView.addSubview(btnNext)
            return footerView
        }
        let footerView = UIView()
        return footerView
        
    }
    override func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        view.tintColor = UIColor.clear
        
    }
    
    func indexOfState( value: String) -> Int {
        return arrayState.index { (question) -> Bool in
            return question["stateId"] == value || question["stateDisplayName"] == value
            } ?? NSNotFound
    }
    
    @IBAction func btnNextClicked(_ sender: Any) {
        
        
        
        
        if validateForm(btn: sender as! UIButton) {
            if (Reachability.isConnectedToNetwork()){
                // Create and add the view to the screen.
                self.view.addSubview(activityIndicator)
                activityIndicator.startAnimating()
                // All done!
                
                
                //                        {"owners":[{"lastName":"Patel","city":"Ahmedabad","primaryPhone":"9876543210","zipCode":"38001","ssn":"123456789","state":"TX","homeAddress":"Naranpura","emailAddress":"amit@gmail.com","dateOfBirth":"08/11/1987","firstName":"Amit"}]}
                
                let ownerState  = arrayState[self.indexOfState( value: txtFldState.text!)]["stateId"] ?? "TX"
                let businessState  = arrayState[self.indexOfState(value:  txtFldBusinessState.text!)]["stateId"] ?? "TX"
                
        
                
                let ownerData:[Dictionary<String,Any>] = [["lastName" : txtFldLastName.text!,
                                                           "city":txtFldCity.text!,
                                                           "primaryPhone":txtFldPrimaryPhone.text!.removeExtraCharFromPhoneNumber(),
                                                           "zipCode":txtFldZipCode.text!,
                                                           "ssn":txtFldSSN.text?.replacingOccurrences(of: "-", with: "") ?? "",
                                                           "state":ownerState,
                                                           "homeAddress":txtFldHomeAddress.text!,
                                                           "emailAddress":txtFldEmailAddress.text!,
                                                           "dateOfBirth":txtFldDateOfBirth.text!,
                                                           "firstName":txtFldFirstName.text!]]
                
                
                
         
                
                
                var paramsDict: [String : Any] = ["dba" : txtFldDBA.text!,
                                                  "businessLegalName" :txtFldBusinessLegalName.text!,
                                                  "requestedAmount" :txtFldRequestedAmount.text!,
                                                  "ein" : txtFldEIN.text!,
                                                  "city" :txtFldBusinessCity.text!,
                                                  "zipCode" :txtFldBusinessZipCode.text!,
                                                  "state" :businessState,
                                                  "officePhone" :txtFldOfficePhone.text!.removeExtraCharFromPhoneNumber(),
                                                  "businessAddress" :txtFldBusinessAddress.text!,
                                                  "consents" :checkbox.isOn(),
                                                  "partnerUserId" :UserData.sharedInstance.userId,
                                                  "owners" :ownerData,
                                                  ]
                if dictDraftedApplication != nil {
                    if ((dictDraftedApplication["ApplicationNumber"]) != nil){
                        paramsDict["applicationNumber"] = dictDraftedApplication["ApplicationNumber"]
                    }
                }
            
            
                
                Utility.requestWithPostURLWithData(strUrl: BASE_URL + SET_APPLICATION, paramDict:paramsDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
                    
                    DispatchQueue.main.async {
                        self.activityIndicator.stopAnimating()
                        if error == nil{
                            let httpResponse = urlResponse as! HTTPURLResponse
                            let responseString = String(data: dataResponse!, encoding: .utf8)
                            var dictApplicationData = Utility.convertToDictionary(text: responseString!)
                            
                            if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK && dictApplicationData != nil){
                               
//                                self.navigationController?.popViewController(animated: true)
//                                 self.objDelegate?.reloadData()
                                self.dictNewApplicationDetails = dictApplicationData
                                switch self.backViewController() {
                                case is AllApplicationViewController:
                                     self.performSegue(withIdentifier: "unwindToAllApp", sender: self)
                                    break
                                case is ActiveApplicationViewController:
                                    self.performSegue(withIdentifier: "unwindToActiveApp", sender: self)
                                    break
                                case is ClosedApplicationViewController:
                                    self.performSegue(withIdentifier: "unwindToCloseApp", sender: self)
                                    break
                                case is DashboardViewController:
                                    self.performSegue(withIdentifier: "unwindToDashboard", sender: self)
                                    break
                                default:
                                    break
                                }
                                
//                                self.navigationController?.popViewController(animated: false)
                            }
                            else{
                                if((dictApplicationData?["message"]) != nil){
                                    Utility.showErrorAlert( message: (dictApplicationData?["message"] as? String)!)
                                }
                                else if((dictApplicationData?["details"]) != nil){
                                    let dictError = (dictApplicationData?["details"] as![Dictionary<String,Any>])[0]
                                    if((dictError["message"]) != nil){
                                        Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                    }
                                }
                            }
                        }
                        else{
                            Utility.showErrorAlert( message: (error?.localizedDescription)! )
                            
                        }
                    }
                })
            }
            else{
                self.activityIndicator.stopAnimating()
                Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
            }
            
        }
    }
    
    
   
    
    
    @IBAction func btnSaveDraftClicked(_ sender: Any) {
        
                if validateForm(btn: sender as! UIButton) {
                    if (Reachability.isConnectedToNetwork()){
                        // Create and add the view to the screen.
                        appDelegate.window?.addSubview(activityIndicator)
                        activityIndicator.startAnimating()
                        // All done!
        
                        
//                        {"owners":[{"lastName":"Patel","city":"Ahmedabad","primaryPhone":"9876543210","zipCode":"38001","ssn":"123456789","state":"TX","homeAddress":"Naranpura","emailAddress":"amit@gmail.com","dateOfBirth":"08/11/1987","firstName":"Amit"}]}
                        
                        
                        
                        var ownerData:[Dictionary<String,Any>] = [[
                            "emailAddress":txtFldEmailAddress.text!]]
                        if !(txtFldLastName.text?.isEmpty)!{ownerData[0]["lastName"] = txtFldLastName.text!}
                        if !(txtFldCity.text?.isEmpty)!{ownerData[0]["city"] = txtFldCity.text!}
                        if !(txtFldPrimaryPhone.text?.isEmpty)!{ownerData[0]["primaryPhone"] = txtFldPrimaryPhone.text!.removeExtraCharFromPhoneNumber()}
                        if !(txtFldZipCode.text?.isEmpty)!{ownerData[0]["zipCode"] = txtFldZipCode.text!}
                        if !(txtFldSSN.text?.isEmpty)!{ownerData[0]["ssn"] = txtFldSSN.text?.replacingOccurrences(of: "-", with: "") ?? ""}
                        if (!((txtFldState.text?.isEmpty)!) && txtFldState.text?.lowercased() != "state" ){ownerData[0]["state"] = arrayState[statePickerView.selectedRow(inComponent: 0)]["stateId"] ?? "TX"}
                        if !(txtFldHomeAddress.text?.isEmpty)!{ownerData[0]["homeAddress"] = txtFldHomeAddress.text!}
                        if !(txtFldDateOfBirth.text?.isEmpty)!{ownerData[0]["dateOfBirth"] = txtFldDateOfBirth.text!}
                        if !(txtFldFirstName.text?.isEmpty)!{ownerData[0]["firstName"] = txtFldFirstName.text!}
                        
                        
        
                        var paramsDict: [String : Any] = [
                                                          "partnerUserId" :UserData.sharedInstance.userId,
                                                          "owners" :ownerData,
                                                          ]
                        
                        if !(txtFldDBA.text?.isEmpty)!{paramsDict["dba"] = txtFldDBA.text!}
                        if !(txtFldBusinessLegalName.text?.isEmpty)!{paramsDict["businessLegalName"] = txtFldBusinessLegalName.text!}
                        if !(txtFldRequestedAmount.text?.isEmpty)!{paramsDict["requestedAmount"] = txtFldRequestedAmount.text!}
                        if !(txtFldEIN.text?.isEmpty)!{paramsDict["ein"] = txtFldEIN.text!}
                        if (!((txtFldBusinessState.text?.isEmpty)!) && txtFldBusinessState.text?.lowercased() != "state" ){paramsDict["state"] = txtFldBusinessState.text!}
                        if !(txtFldBusinessCity.text?.isEmpty)!{paramsDict["city"] = txtFldBusinessCity.text!}
                        if !(txtFldBusinessZipCode.text?.isEmpty)!{paramsDict["zipCode"] = txtFldBusinessZipCode.text!}
                        if !(txtFldOfficePhone.text?.isEmpty)!{paramsDict["officePhone"] = txtFldOfficePhone.text!.removeExtraCharFromPhoneNumber() }
                        if !(txtFldBusinessAddress.text?.isEmpty)!{paramsDict["businessAddress"] = txtFldBusinessAddress.text!}
                        if (checkbox.isOn()){paramsDict["consents"] = checkbox.isOn() }
                        
                        if dictDraftedApplication != nil {
                            if ((dictDraftedApplication["ApplicationNumber"]) != nil){paramsDict["applicationNumber"] = dictDraftedApplication["ApplicationNumber"]}
                        }
                        
                        
        
                        Utility.requestWithPostURLWithData(strUrl: BASE_URL + SET_APPLICATION_DRAFT, paramDict:paramsDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
        
                            DispatchQueue.main.async {
                                self.activityIndicator.stopAnimating()
                                if error == nil{
                                    let httpResponse = urlResponse as! HTTPURLResponse
                                    let responseString = String(data: dataResponse!, encoding: .utf8)
                                   
                                    let dict = Utility.convertToDictionary(text: responseString!)
        
                                    if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK && dict != nil){
                                        self.objDelegate?.reloadData()
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                    else{
                                        if((dict?["message"]) != nil){
                                            Utility.showErrorAlert( message: (dict?["message"] as? String)!)
                                        }
                                        else if((dict?["details"]) != nil){
                                            let dictError = (dict?["details"] as![Dictionary<String,Any>])[0]
                                            if((dictError["message"]) != nil){
                                                Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                            }
                                        }

                                    }
                                }
                                else{
                                    Utility.showErrorAlert( message: (error?.localizedDescription)! )
                                    
                                }
                            }
                        })
                    }
                    else{
                        self.activityIndicator.stopAnimating()
                        Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
                    }
                    
                }
    }
    
    func validateForm(btn:UIButton) -> Bool {
    
        if btn == btnNext {
            
            
            /// First Name Validation
            if (txtFldFirstName.text?.isEmpty)!{
                Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.FirstName.rawValue) )
                txtFldFirstName.setRequiredFildErrorStyle()
                return false;
            }
            else if( !Utility.checkLengthValidation(strInput: txtFldFirstName.text, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100)){
                Utility.showErrorAlert( message: Message.getFieldLengthErrorMessage(strFiledName: Fields.FirstName.rawValue, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100))
                txtFldFirstName.setRequiredFildErrorStyle()
                return false;
            }
            
            /// Last Name Validation
            if (txtFldLastName.text?.isEmpty)!{
                Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.LastName.rawValue))
                txtFldLastName.setRequiredFildErrorStyle()
                return false;
            }
            else if( !Utility.checkLengthValidation(strInput: txtFldLastName.text, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100)){
                
                Utility.showErrorAlert( message: Message.getFieldLengthErrorMessage(strFiledName: Fields.LastName.rawValue, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100))
                txtFldLastName.setRequiredFildErrorStyle()
                return false;
            }
            
            /// SSN Validation
            if (txtFldSSN.text?.isEmpty)!{
                Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.SSN.rawValue))
                txtFldSSN.setRequiredFildErrorStyle()
                return false;
            }
            else if(!Utility.validateSSN(ssnString:txtFldSSN.text)){
                Utility.showErrorAlert( message: Message.getFieldInvalidInputErrorMessage(strFiledName: Fields.SSN.rawValue))
                return false;
            }
            
            /// PrimaryPhone Validation
            if (txtFldPrimaryPhone.text?.isEmpty)!{
                Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.PrimaryPhone.rawValue))
                txtFldPrimaryPhone.setRequiredFildErrorStyle()
                return false;
            }
            else if(!Utility.validatePhone(phoneString:txtFldPrimaryPhone.text!)){
                Utility.showErrorAlert( message: Message.getFieldInvalidInputErrorMessage(strFiledName: Fields.PrimaryPhone.rawValue))
                return false;
            }
            
            /// SecondaryPhone Validation
            if (!(txtFldSecondaryPhone.text?.isEmpty)!){
                if(!Utility.validatePhone(phoneString:txtFldSecondaryPhone.text!)){
                    Utility.showErrorAlert( message: Message.getFieldInvalidInputErrorMessage(strFiledName: Fields.SecondaryPhone.rawValue))
                    return false;
                }
            }
            
            /// Email Validation
            if (txtFldEmailAddress.text?.isEmpty)!{
                Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.EmailAddress.rawValue))
                txtFldEmailAddress.setRequiredFildErrorStyle()
                return false;
            }
            else if(!Utility.validateEmail(emailString:txtFldEmailAddress.text)){
                Utility.showErrorAlert( message: Message.getFieldInvalidInputErrorMessage(strFiledName: Fields.EmailAddress.rawValue))
                return false;
            }
            
            
            /// Home Address Validation
            if (txtFldHomeAddress.text?.isEmpty)!{
                Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.HomeAddress.rawValue))
                txtFldHomeAddress.setRequiredFildErrorStyle()
                return false;
            }
            else if( !Utility.checkLengthValidation(strInput: txtFldHomeAddress.text, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100)){
                Utility.showErrorAlert( message: Message.getFieldLengthErrorMessage(strFiledName: Fields.HomeAddress.rawValue, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100))
                return false;
            }
            
            /// City Validation
            if (txtFldCity.text?.isEmpty)!{
                Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.City.rawValue))
                txtFldCity.setRequiredFildErrorStyle()
                return false;
            }
            else if( !Utility.checkLengthValidation(strInput: txtFldCity.text, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100)){
                Utility.showErrorAlert( message: Message.getFieldLengthErrorMessage(strFiledName: Fields.City.rawValue, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100))
                return false;
            }
            
            /// State Validation
            if (txtFldState.text?.isEmpty)! || txtFldState.text == "State"{
                Utility.showErrorAlert( message: "Please select state")
                txtFldState.setRequiredFildErrorStyle()
                return false;
            }
            
            /// Zip Validation
            if (txtFldZipCode.text?.isEmpty)!{
                Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.Zip.rawValue))
                txtFldZipCode.setRequiredFildErrorStyle()
                return false;
            }
            else if( !Utility.checkLengthValidation(strInput: txtFldZipCode.text, minLength: MINLENGTH_5, maxLength: MAXLENGTH_5)){
                Utility.showErrorAlert( message: Message.getFieldLengthErrorMessage(strFiledName: Fields.Zip.rawValue, minLength: MINLENGTH_5, maxLength: MAXLENGTH_5))
                return false;
            }
            
            
            /// DOB Validation
            if (txtFldDateOfBirth.text?.isEmpty)!{
                Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.DOB.rawValue))
                txtFldDateOfBirth.setRequiredFildErrorStyle()
                return false;
            }
            
            
            
            
            /// BusinessLegalName Validation
            if (txtFldBusinessLegalName.text?.isEmpty)!{
                Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.BusinessLegalName.rawValue))
                txtFldBusinessLegalName.setRequiredFildErrorStyle()
                return false;
            }
            else if( !Utility.checkLengthValidation(strInput: txtFldBusinessLegalName.text, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100)){
                Utility.showErrorAlert( message: Message.getFieldLengthErrorMessage(strFiledName: Fields.BusinessLegalName.rawValue, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100))
                return false;
            }
            
            
            /// DBA Validation
            if (txtFldDBA.text?.isEmpty)!{
                Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.DBA.rawValue))
                txtFldDBA.setRequiredFildErrorStyle()
                return false;
            }
            
            /// Company Name Validation
            if (txtFldEIN.text?.isEmpty)!{
                Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.EIN.rawValue))
                txtFldEIN.setRequiredFildErrorStyle()
                return false;
            }
            
            
            /// Business Address Validation
            if (txtFldBusinessAddress.text?.isEmpty)!{
                Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.BusinessAddress.rawValue))
                txtFldBusinessAddress.setRequiredFildErrorStyle()
                return false;
            }
            else if( !Utility.checkLengthValidation(strInput: txtFldBusinessAddress.text, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100)){
                Utility.showErrorAlert( message: Message.getFieldLengthErrorMessage(strFiledName: Fields.BusinessAddress.rawValue, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100))
                return false;
            }
            
            /// Business City Validation
            if (txtFldBusinessCity.text?.isEmpty)!{
                Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.City.rawValue))
                txtFldBusinessCity.setRequiredFildErrorStyle()
                return false;
            }
            else if( !Utility.checkLengthValidation(strInput: txtFldBusinessCity.text, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100)){
                Utility.showErrorAlert( message: Message.getFieldLengthErrorMessage(strFiledName: Fields.City.rawValue, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100))
                return false;
            }
            
            /// Business State Validation
            if (txtFldBusinessState.text?.isEmpty)! || txtFldBusinessState.text == "State"{
                Utility.showErrorAlert( message: "Please select state")
                txtFldBusinessState.setRequiredFildErrorStyle()
                return false;
            }
            
            /// Business Zip Code Validation
            if (txtFldBusinessZipCode.text?.isEmpty)!{
                Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.Zip.rawValue))
                txtFldBusinessZipCode.setRequiredFildErrorStyle()
                return false;
            }
            else if( !Utility.checkLengthValidation(strInput: txtFldBusinessZipCode.text, minLength: MINLENGTH_5, maxLength: MAXLENGTH_5)){
                Utility.showErrorAlert( message: Message.getFieldLengthErrorMessage(strFiledName: Fields.Zip.rawValue, minLength: MINLENGTH_5, maxLength: MAXLENGTH_5))
                return false;
            }
            
            
            /// OfficePhone Validation
            if (txtFldOfficePhone.text?.isEmpty)!{
                Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.OfficePhone.rawValue))
                txtFldOfficePhone.setRequiredFildErrorStyle()
                return false;
            }
            else if(!Utility.validatePhone(phoneString:txtFldOfficePhone.text!)){
                Utility.showErrorAlert( message: Message.getFieldInvalidInputErrorMessage(strFiledName: Fields.OfficePhone.rawValue))
                return false;
            }
            
            
            /// Reqiested amount Validation
            if (txtFldRequestedAmount.text?.isEmpty)!{
                Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.RequestedAmount.rawValue))
                txtFldRequestedAmount.setRequiredFildErrorStyle()
                return false;
            }
            else if(!Utility.validateAmount(amountString:txtFldRequestedAmount.text!)){
                Utility.showErrorAlert( message: "We currently accept requests between $5,000 and $100,000")
                return false;
            }
            
            
            if !checkbox.isOn() {
                //Please check consent checkobox <#code#>
                Utility.showErrorAlert( message: "Please check consent checkobox")
            }
            
            return true;
            
        }
        else if btn == btnSaveDraft{
            /// Email Validation
            if (txtFldEmailAddress.text?.isEmpty)!{
                Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.EmailAddress.rawValue))
                txtFldEmailAddress.setRequiredFildErrorStyle()
                return false;
            }
            else if(!Utility.validateEmail(emailString:txtFldEmailAddress.text)){
                Utility.showErrorAlert( message: Message.getFieldInvalidInputErrorMessage(strFiledName: Fields.EmailAddress.rawValue))
                return false;
            }
            /// Reqiested amount Validation
            if (txtFldRequestedAmount.text?.isEmpty)!{
                Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.RequestedAmount.rawValue))
                txtFldRequestedAmount.setRequiredFildErrorStyle()
                return false;
            }
            else if(!Utility.validateAmount(amountString:txtFldRequestedAmount.text!)){
                Utility.showErrorAlert( message: "We currently accept requests between $5,000 and $100,000")
                return false;
            }

            return true;
        }
        return true;
    }
    
    // MARK: - UITextField Delegate Method
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField.inputAccessoryView == nil {
            textField.inputAccessoryView = objKeyboardToolbar
        }
        activeTextField = textField
        
      
        return true
    }
    @objc func cancelPicker(){
        txtFldDateOfBirth.resignFirstResponder()
    }
    @objc func donePicker(){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DATE_FORMAT
        let dateString = dateFormatter.string(from: datePickerView.date)
        txtFldDateOfBirth.text = dateString
        txtFldDateOfBirth.resignFirstResponder()
        txtFldBusinessLegalName.becomeFirstResponder()
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.isEqual(txtFldDateOfBirth) {
            if !(txtFldDateOfBirth.text?.isEmpty)! {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = DATE_FORMAT
                let selectedDate = dateFormatter.date(from: txtFldDateOfBirth.text!)
                datePickerView.setDate(selectedDate!, animated: true)
            }
        }
        if textField.isEqual(txtFldState) {
            if !(txtFldState.text?.isEmpty)! {
                statePickerView.selectRow(arrayState.index{ $0["stateDisplayName"]! == txtFldState.text! }!, inComponent: 0, animated: true)
            }
        }
        if textField.isEqual(txtFldBusinessState) {
            if !(txtFldBusinessState.text?.isEmpty)! {
                statePickerView.selectRow(arrayState.index{ $0["stateDisplayName"]! == txtFldBusinessState.text! }!, inComponent: 0, animated: true)
            }
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtFldPrimaryPhone || textField == txtFldSecondaryPhone || textField == txtFldOfficePhone{
            guard let text = textField.text else { return true }
            
            let newLength = text.characters.count + string.characters.count - range.length
            
            if  (text.characters.count == 0 && newLength == 1){
                textField.text = "(" + textField.text!
            }
            
            if  (text.characters.count == 2 && newLength == 1){
                textField.text = textField.text?.substring(to: (textField.text?.index(before: (textField.text?.endIndex)!))!)
            }
            
            if  (text.characters.count == 4 && newLength == 5){
                textField.text =  textField.text! + ") "
            }
            
            if  (text.characters.count == 7 && newLength == 6){
                textField.text = textField.text?.substring(to: (textField.text?.index(before: (textField.text?.endIndex)!))!)
                textField.text = textField.text?.substring(to: (textField.text?.index(before: (textField.text?.endIndex)!))!)
            }
            
            if  (text.characters.count == 9 && newLength == 10){
                textField.text =  textField.text! + "-"
            }
            
            if  (text.characters.count == 11 && newLength == 10){
                textField.text = textField.text?.substring(to: (textField.text?.index(before: (textField.text?.endIndex)!))!)
            }
            return newLength <= PHONE_NUMBER_LENGTH
        }
        if textField == txtFldZipCode || textField == txtFldBusinessZipCode{
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= ZIP_CODE_LENGTH
        }
        if textField == txtFldSSN{
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            if  (text.characters.count == 3 && newLength == 4) || (text.characters.count == 6 && newLength == 7){
                textField.text = textField.text! + "-"
            }
            if  (text.characters.count == 5 && newLength == 4) || (text.characters.count == 8 && newLength == 7){
                textField.text = textField.text?.substring(to: (textField.text?.index(before: (textField.text?.endIndex)!))!)
            }
             return newLength <= SSN_MAX_LEGNTH
        }
        return true
    }
    
    func keyboardToolbarNextButtonClicked() {
        switch activeTextField {
        case txtFldFirstName:
            txtFldLastName.becomeFirstResponder()
            break
        case txtFldLastName:
            txtFldSSN.becomeFirstResponder()
            break
        case txtFldSSN:
            txtFldPrimaryPhone.becomeFirstResponder()
            break
        case txtFldPrimaryPhone:
            txtFldSecondaryPhone.becomeFirstResponder()
            break
        case txtFldSecondaryPhone:
            txtFldEmailAddress.becomeFirstResponder()
            break
        case txtFldEmailAddress:
            txtFldHomeAddress.becomeFirstResponder()
            break
        case txtFldHomeAddress:
            txtFldCity.becomeFirstResponder()
            break
        case txtFldCity:
            txtFldState.becomeFirstResponder()
            break
        case txtFldState:
            txtFldZipCode.becomeFirstResponder()
            break
        case txtFldZipCode:
            txtFldDateOfBirth.becomeFirstResponder()
            break
        case txtFldDateOfBirth:
            txtFldBusinessLegalName.becomeFirstResponder()
            break
        case txtFldBusinessLegalName:
            txtFldDBA.becomeFirstResponder()
            break
            
        case txtFldDBA:
            txtFldEIN.becomeFirstResponder()
            break
        case txtFldEIN:
            txtFldBusinessAddress.becomeFirstResponder()
            break
        case txtFldBusinessAddress:
            txtFldBusinessCity.becomeFirstResponder()
            break
        case txtFldBusinessCity:
            txtFldBusinessState.becomeFirstResponder()
            break
        case txtFldBusinessState:
            txtFldBusinessZipCode.becomeFirstResponder()
            break
        case txtFldBusinessZipCode:
            txtFldOfficePhone.becomeFirstResponder()
            break
        case txtFldOfficePhone:
            txtFldRequestedAmount.becomeFirstResponder()
            break
        case txtFldRequestedAmount:
            txtFldRequestedAmount.resignFirstResponder()
            break
            
        default:
            self.view.endEditing(true)
            break
        }
        
    }
    func keyboardToolbarPreviousButtonClicked() {
        switch activeTextField {
        case txtFldFirstName:
            txtFldFirstName.becomeFirstResponder()
            break
        case txtFldLastName:
            txtFldFirstName.becomeFirstResponder()
            break
        case txtFldSSN:
            txtFldLastName.becomeFirstResponder()
            break
        case txtFldPrimaryPhone:
            txtFldSSN.becomeFirstResponder()
            break
        case txtFldSecondaryPhone:
            txtFldPrimaryPhone.becomeFirstResponder()
            break
        case txtFldEmailAddress:
            txtFldSecondaryPhone.becomeFirstResponder()
            break
        case txtFldHomeAddress:
            txtFldEmailAddress.becomeFirstResponder()
            break
        case txtFldCity:
            txtFldHomeAddress.becomeFirstResponder()
            break
        case txtFldState:
            txtFldCity.becomeFirstResponder()
            break
        case txtFldZipCode:
            txtFldState.becomeFirstResponder()
            break
        case txtFldDateOfBirth:
            txtFldZipCode.becomeFirstResponder()
            break
        case txtFldBusinessLegalName:
            txtFldDateOfBirth.becomeFirstResponder()
            break
            
        case txtFldDBA:
            txtFldBusinessLegalName.becomeFirstResponder()
            break
        case txtFldEIN:
            txtFldDBA.becomeFirstResponder()
            break
        case txtFldBusinessAddress:
            txtFldEIN.becomeFirstResponder()
            break
        case txtFldBusinessCity:
            txtFldBusinessAddress.becomeFirstResponder()
            break
        case txtFldBusinessState:
            txtFldBusinessCity.becomeFirstResponder()
            break
        case txtFldBusinessZipCode:
            txtFldBusinessState.becomeFirstResponder()
            break
        case txtFldOfficePhone:
            txtFldBusinessZipCode.becomeFirstResponder()
            break
        case txtFldRequestedAmount:
            txtFldOfficePhone.becomeFirstResponder()
            break
            
        default:
            self.view.endEditing(true)
            break
        }
    }
    func keyboardToolbarHideKeyboard() {
        hideKeyboard()
    }
    
    // MARK: - UIPickerView Delegate Method
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayState.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayState[row]["stateDisplayName"]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
    @objc func cancelStatePicker(){
        if txtFldState.isEditing {
            txtFldState.resignFirstResponder()
        }
        else if txtFldBusinessState.isEditing {
            txtFldBusinessState.resignFirstResponder()
        }
        
    }
    @objc func doneStatePicker(){
        if txtFldState.isEditing {
            txtFldState.text = arrayState[statePickerView.selectedRow(inComponent: 0)]["stateDisplayName"]
            txtFldZipCode.becomeFirstResponder()
        }
        else if txtFldBusinessState.isEditing {
            txtFldBusinessState.text = arrayState[statePickerView.selectedRow(inComponent: 0)]["stateDisplayName"]
            txtFldBusinessZipCode.becomeFirstResponder()
        }
    }
    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}



