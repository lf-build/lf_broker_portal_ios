//
//  ApplicationPricingViewController.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 7/12/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit

class ApplicationPricingViewController: UIViewController {

    var dictApplicationDetails:Dictionary<String,Any>!
    @IBOutlet weak var lblApplicationId: UILabel!
    @IBOutlet weak var lblApplicantName: UILabel!
    @IBOutlet weak var lblBusinessName: UILabel!
    @IBOutlet weak var lblLoanAmount: UILabel!
    @IBOutlet weak var lblRepaymentAmount: UILabel!
    @IBOutlet weak var lblSellAmount: UILabel!
    @IBOutlet weak var lblTerm: UILabel!
    @IBOutlet weak var lblCommission: UILabel!
    
    @IBOutlet weak var lblGrade: UILabel!
    
    @IBOutlet weak var lblAmountFunded: UILabel!
    @IBOutlet weak var lblPaybackAmount: UILabel!
    @IBOutlet weak var lblCommissionAmount: UILabel!
    @IBOutlet weak var lblCommissionPer: UILabel!
    
    
    
    
    @IBOutlet weak var backgrounfView: UIView!
    @IBOutlet weak var applicationStatusBGView: UIView!
    @IBOutlet weak var applicationDetailsBGView: UIView!
    @IBOutlet weak var divergentGradeBGView: UIView!
    
    @IBOutlet weak var voidedDocumentCheckBGView: UIView!
   
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var lineViewSlider: UIView!
    @IBOutlet weak var scrlView: UIScrollView!
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    

    
    var dictResponse:Dictionary<String,Any> = [:]
    
    
    
    @IBOutlet weak var sliderDailyPayment: UISlider!
    @IBOutlet weak var lblDailyPaymentMinValue: UILabel!
    @IBOutlet weak var lblDailyPaymentCurrentValue: UILabel!
    @IBOutlet weak var lblDailyPaymentMaxValue: UILabel!
    
    @IBOutlet weak var sliderSellRate: UISlider!
    @IBOutlet weak var lblSellRateMinValue: UILabel!
    @IBOutlet weak var lblSellRateCurrentValue: UILabel!
    @IBOutlet weak var lblSellRateMaxValue: UILabel!
    
    @IBOutlet weak var sliderTerm: UISlider!
    @IBOutlet weak var lblTermMinValue: UILabel!
    @IBOutlet weak var lblTermCurrentValue: UILabel!
    
    @IBOutlet weak var lblTermMaxValue: UILabel!
    @IBOutlet weak var btnCommission: UIButton!
    @IBOutlet weak var btnAdvance: UIButton!
    
    @IBOutlet weak var btnBGView: UIView!
    
    
    @IBOutlet weak var btnSendContract: UIButton!
    
    
    override func viewDidLoad() {
        self.title = dictApplicationDetails["StatusName"] as? String
        applicationStatusBGView.layer.borderWidth = 0.0
        applicationStatusBGView.layer.borderColor = Utility.hexStringToUIColor(hex: GRAY_COLOR).cgColor
        applicationStatusBGView.layer.masksToBounds = true
        applicationStatusBGView.layer.cornerRadius = 4.0
        
        applicationDetailsBGView.layer.borderWidth = 0.0
        applicationDetailsBGView.layer.borderColor = Utility.hexStringToUIColor(hex: GRAY_COLOR).cgColor
        applicationDetailsBGView.layer.masksToBounds = true
        applicationDetailsBGView.layer.cornerRadius = 4.0
        
        divergentGradeBGView.layer.borderWidth = 0.0
        divergentGradeBGView.layer.borderColor = Utility.hexStringToUIColor(hex: GRAY_COLOR).cgColor
        divergentGradeBGView.layer.masksToBounds = true
        divergentGradeBGView.layer.cornerRadius = 4.0
        
        btnBGView.layer.borderWidth = 0.0
        btnBGView.layer.borderColor = Utility.hexStringToUIColor(hex: GRAY_COLOR).cgColor
        btnBGView.layer.masksToBounds = true
        btnBGView.layer.cornerRadius = 4.0
        
        
        btnCommission.setDefaultThemeStyle()
        btnAdvance.setDefaultThemeStyle()
        btnSendContract.setDefaultThemeStyle()
        
        
        backgrounfView.backgroundColor = UIColor.clear
        self.lblApplicationId.text = self.dictApplicationDetails["ApplicationNumber"] as? String
        self.lblApplicantName.text = self.dictApplicationDetails["BusinessApplicantName"] as? String
        self.lblBusinessName.text = self.dictApplicationDetails["LegalBusinessName"] as? String
        self.lblLoanAmount.text = String(format:"$%.2f",self.dictApplicationDetails["LoanAmount"] as! Float)
        self.lblRepaymentAmount.text = String(format:"$%.2f",self.dictApplicationDetails["RepaymentAmount"] as! Float)
        self.lblSellAmount.text = String(format:"%.2f",self.dictApplicationDetails["SellRate"] as! Float)
        self.lblTerm.text = String(format:"%.2f",self.dictApplicationDetails["Term"] as! Float)
        self.lblCommission.text = String(format:"$%.2f",self.dictApplicationDetails["CommissionAmount"] as! Float)
        
        self.view.backgroundColor = Utility.hexStringToUIColor(hex: VIEW_BACKGROUND_COLOR)
        activityIndicator.setDefaultThemeStyle()
        
        applicationStatusBGView.isHidden = true
        applicationDetailsBGView.isHidden = true
        divergentGradeBGView.isHidden = true
        btnBGView.isHidden = true
       
        
        let paramsDict = ["applicationNumber":dictApplicationDetails["ApplicationNumber"] ?? ""] as Dictionary<String,Any>
        
        getData(strUrl: BASE_URL + GET_PRICE_OFFER, paramDict: paramsDict)
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        scrlView.contentSize = CGSize(width: self.view.frame.size.width, height: backgrounfView.frame.size.height);
    }
    func getData(strUrl: String, paramDict:Dictionary<String,Any>) {
        
        if (Reachability.isConnectedToNetwork()){
            // Create and add the view to the screen.
            self.view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            
            // All done!
            Utility.requestWithPostURLWithData(strUrl: strUrl, paramDict: paramDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
                
                DispatchQueue.main.async {
                    
                    self.applicationStatusBGView.isHidden = false
                    self.applicationDetailsBGView.isHidden = false
                    self.divergentGradeBGView.isHidden = false
                    self.btnBGView.isHidden = false
                    
                    self.lblApplicationId.text = self.dictApplicationDetails["ApplicationNumber"] as? String
                    self.lblApplicantName.text = self.dictApplicationDetails["BusinessApplicantName"] as? String
                    
                    
                    self.activityIndicator.stopAnimating()
                    if error == nil{
                        let httpResponse = urlResponse as! HTTPURLResponse
                        let responseString = String(data: dataResponse!, encoding: .utf8)!
                        self.dictResponse = Utility.convertToDictionary(text: responseString)!
                        
                        if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK && self.dictResponse.count > 0){
                            if !responseString.isEmpty {
                                let strGrade = self.dictResponse["divergentGrade"] as? String
                                 self.lineView.backgroundColor = Utility.hexStringToUIColor(hex: VIEW_BACKGROUND_COLOR)
                                self.lineViewSlider.backgroundColor = Utility.hexStringToUIColor(hex: VIEW_BACKGROUND_COLOR)
                                
                                
                                let yourAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black]
                                
                                let yourOtherAttributes = [NSAttributedStringKey.foregroundColor: Utility.hexStringToUIColor(hex:BORDER_RED_COLOR)]
                                
                                let partOne = NSMutableAttributedString(string: "Divergent Grade", attributes: yourAttributes)
                                let partTwo = NSMutableAttributedString(string: "  " + strGrade!, attributes: yourOtherAttributes)
                                let combination = NSMutableAttributedString()
                                
                                combination.append(partOne)
                                combination.append(partTwo)
                                
                                self.lblGrade.attributedText = combination
                                
                                self.lblAmountFunded.text =  String(format:"$%.2f",self.dictResponse["amountFunded"] as! Float)  //self.dictResponse["amountFunded"] as? String
                                self.lblPaybackAmount.text = String(format:"$%.2f",self.dictResponse["amountPayback"] as! Float) //self.dictResponse["amountPayback"] as? String
                                self.lblCommissionAmount.text = String(format:"$%.2f",self.dictResponse["commission"] as! Float) //self.dictResponse["commission"] as? String
                                
                                self.lblCommissionPer.text = String(format:"%.2f%%",self.dictResponse["commissionPercentage"] as! Float) //self.dictResponse["commissionPercentage"] as? String
                                
                                
                                /// Slider Daily Payment
                                let dictRang = self.dictResponse["range"] as! Dictionary<String,Any>
                                self.sliderDailyPayment.minimumValue = 0.0
                                 self.sliderDailyPayment.maximumValue = self.calculateDailyRepayment(monthlyRevenue: self.dictResponse["monthlyRevenue"] as! Float, avgDailyBalance: self.dictResponse["averageDailyBalance"] as! Float, minDailyPayment: (((dictRang["dailyPayment"] as! Dictionary<String,Any>)["min"]) as! Float), maxDailyPayment: (((dictRang["dailyPayment"] as! Dictionary<String,Any>)["max"]) as! Float))
                                
                                
                                
                                
                                self.sliderDailyPayment.value = self.dictResponse["dailyPayment"] as! Float
                                 self.lblDailyPaymentMinValue.text = String.init(format: "$%d",Int(round(self.sliderDailyPayment.minimumValue)))
                                self.lblDailyPaymentMaxValue.text = String.init(format: "$%d",Int(round(self.sliderDailyPayment.maximumValue)))
                                self.lblDailyPaymentCurrentValue.text = String.init(format: "$%d",Int(round(self.sliderDailyPayment.value)))
                                
                                
                                /// Slider SellRate
                                self.sliderSellRate.minimumValue = ((dictRang["sellRate"] as! Dictionary<String,Any>)["min"]) as! Float
                                self.sliderSellRate.maximumValue = ((dictRang["sellRate"] as! Dictionary<String,Any>)["max"]) as! Float
                                self.sliderSellRate.value = self.dictResponse["sellRate"] as! Float
                                self.lblSellRateMinValue.text = String.init(format: "%.2f",self.sliderSellRate.minimumValue)
                                self.lblSellRateMaxValue.text = String.init(format: "%.2f",self.sliderSellRate.maximumValue)
                                 self.lblSellRateCurrentValue.text = String.init(format: "%.2f",self.sliderSellRate.value)
                                
                                
                                /// Slider Term
                                self.sliderTerm.minimumValue = ((dictRang["term"] as! Dictionary<String,Any>)["min"]) as! Float
                                self.sliderTerm.maximumValue = ((dictRang["term"] as! Dictionary<String,Any>)["max"]) as! Float
                                self.sliderTerm.value = self.dictResponse["term"] as! Float
                                self.lblTermMinValue.text = String.init(format: "%.d",Int(round(self.sliderTerm.minimumValue)))
                                self.lblTermMaxValue.text = String.init(format: "%d",Int(round(self.sliderTerm.maximumValue)))
                                self.lblTermCurrentValue.text = String.init(format: "%d",Int(round(self.sliderTerm.value)))
                                
                                
                                
                                
                            
                            }
                        }
                        else{
                            self.activityIndicator.stopAnimating()
                            
                            if((self.dictResponse["message"]) != nil){
                                Utility.showErrorAlert( message: (self.dictResponse["message"] as? String)!)
                            }
                            else if((self.dictResponse["details"]) != nil){
                                let dictError = (self.dictResponse["details"] as![Dictionary<String,Any>])[0]
                                if((dictError["message"]) != nil){
                                    Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                }
                            }
                            
                        }
                    }
                    else{
                        
                        self.activityIndicator.stopAnimating()
                        Utility.showErrorAlert(message: (error?.localizedDescription)!)
                        
                    }
                }
            })
        }
        else{
            self.activityIndicator.stopAnimating()
            Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func sliderValueChanged(_ sender: Any) {
        switch sender as! UISlider {
        case sliderDailyPayment:
            lblDailyPaymentCurrentValue.text = String.init(format: "$%d",Int(round(sliderDailyPayment.value)))
            break
        case sliderSellRate:
            lblSellRateCurrentValue.text = String.init(format: "%.2f",sliderSellRate.value)
            break
        case sliderTerm:
            lblTermCurrentValue.text = String.init(format: "%d",Int(round(sliderTerm.value)))
            break
        default:
            break
        }
       getIntrimData()
    }
    
    
    func getIntrimData() {
        let dailyPaymenNumber = NSNumber(value:Int(round(sliderDailyPayment.value)))
        self.dictResponse["dailyPayment"] = dailyPaymenNumber
        
        let sellRateNumber = NSNumber(value:sliderSellRate.value)
        self.dictResponse["sellRate"] = sellRateNumber
        
        let termNumber = NSNumber(value:Int(round(sliderTerm.value)))
        self.dictResponse["term"] = termNumber
        
        getData(strUrl: BASE_URL + GET_INTRIM_OFFER, paramDict: self.dictResponse)
    }
    @IBAction func btnCommissionClicked(_ sender: Any) {
        sliderDailyPayment.value = sliderDailyPayment.maximumValue
        sliderSellRate.value = sliderSellRate.maximumValue
        sliderTerm.value = sliderTerm.maximumValue
        
        lblDailyPaymentCurrentValue.text = String.init(format: "$%d",Int(round(sliderDailyPayment.value)))
        lblSellRateCurrentValue.text = String.init(format: "%.2f",sliderSellRate.value)
        lblTermCurrentValue.text = String.init(format: "%d",Int(round(sliderTerm.value)))
        
        getIntrimData()
      
        
    }
   
    @IBAction func btnAdvanceClicked(_ sender: Any) {
        
        sliderDailyPayment.value = sliderDailyPayment.maximumValue
         sliderSellRate.value = sliderSellRate.minimumValue
        sliderTerm.value = sliderTerm.maximumValue
        
        lblDailyPaymentCurrentValue.text = String.init(format: "$%d",Int(round(sliderDailyPayment.value)))
        lblSellRateCurrentValue.text = String.init(format: "%.2f",sliderSellRate.value)
        lblTermCurrentValue.text = String.init(format: "%d",Int(round(sliderTerm.value)))
        
        getIntrimData()
    }
   
    
    @IBAction func btnSendContractClicked(_ sender: Any) {
        if(self.dictResponse["amountFunded"] as! Float > 5000){
            var dictParameter = self.dictResponse
            dictParameter.removeValue(forKey: "range")
            getData(strUrl: BASE_URL + SET_DEAL, paramDict: dictParameter)
        }
        else{
            Utility.showErrorAlert(message: "The minimum advance amount is $5000")
        }
    }
    
    
    
    // Calculate max daily payment based on data received on page load
    func calculateDailyRepayment(monthlyRevenue:Float, avgDailyBalance:Float,
                  minDailyPayment:Float, maxDailyPayment:Float) -> Float {
        var dailyRepayment:Float = 0.0
        let dailyRevanue:Float = monthlyRevenue / 20
        if dailyRevanue < avgDailyBalance {
            dailyRepayment = dailyRevanue * maxDailyPayment
        }
        else {
            dailyRepayment = avgDailyBalance * minDailyPayment
        }
        return dailyRepayment
    }
    
    

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
