//
//  ApplicationAgreementViewController.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 7/12/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit

import ActiveLabel

let BTN_CLIENTSIGN_TAG = 101
let BTN_VOIDED_CHECK_TAG = 102
let BTN_PHOTO_ID_TAG = 103

protocol AgreementDelegate: class {
    func reloadData()
}

class ApplicationAgreementViewController: UIViewController, DocumentUpdatesDelegate {
    var dictApplicationDetails:Dictionary<String,Any>!
    @IBOutlet weak var lblApplicationId: UILabel!
    @IBOutlet weak var lblApplicantName: UILabel!
    @IBOutlet weak var lblBusinessName: UILabel!
    @IBOutlet weak var lblLoanAmount: UILabel!
    @IBOutlet weak var lblRepaymentAmount: UILabel!
    @IBOutlet weak var lblSellAmount: UILabel!
    @IBOutlet weak var lblTerm: UILabel!
    @IBOutlet weak var lblCommission: UILabel!
    @IBOutlet weak var lblSignStatus: UILabel!
    @IBOutlet weak var lblResendEmailText: ActiveLabel!
    
    
    
    @IBOutlet weak var backgrounfView: UIView!
    @IBOutlet weak var applicationStatusBGView: UIView!
    @IBOutlet weak var applicationDetailsBGView: UIView!
    @IBOutlet weak var verificationStatusBGView: UIView!
    @IBOutlet weak var voidedDocumentCheckBGView: UIView!
    @IBOutlet weak var photoIdDocumentBGView: UIView!
    @IBOutlet weak var taxReturnBgView: UIView!
    @IBOutlet weak var confessionBgView: UIView!
    @IBOutlet weak var lienBgView: UIView!
    
    
    @IBOutlet weak var taxreturnViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var confessionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lienHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var bgViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var scrlView: UIScrollView!
   
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var lineViewDocuSign: UIView!
    @IBOutlet weak var lineUploadDocument: UIView!
    
    @IBOutlet weak var lblEmailText: ActiveLabel!
    
    @IBOutlet weak var btnFinish: UIButton!
    @IBOutlet weak var btnClientNotIntrested: UIButton!
    weak var objDelegate: NewApplicationDelegate? = nil
    
    var dictResponse:Dictionary<String,Any> = [:]
    
    
        override func viewDidLoad() {
            
            setUI()
            
        self.title = dictApplicationDetails["StatusName"] as? String
        
        self.lblApplicationId.text = self.dictApplicationDetails["ApplicationNumber"] as? String
        self.lblApplicantName.text = self.dictApplicationDetails["BusinessApplicantName"] as? String
        self.lblBusinessName.text = self.dictApplicationDetails["LegalBusinessName"] as? String
        self.lblLoanAmount.text = String(format:"$%.2f",self.dictApplicationDetails["LoanAmount"] as! Float)
        self.lblRepaymentAmount.text = String(format:"$%.2f",self.dictApplicationDetails["RepaymentAmount"] as! Float)
        self.lblSellAmount.text = String(format:"%.2f",self.dictApplicationDetails["SellRate"] as! Float)
        self.lblTerm.text = String(format:"%.2f",self.dictApplicationDetails["Term"] as! Float)
        self.lblCommission.text = String(format:"$%.2f",self.dictApplicationDetails["CommissionAmount"] as! Float)
        
        self.view.backgroundColor = Utility.hexStringToUIColor(hex: VIEW_BACKGROUND_COLOR)
        activityIndicator.setDefaultThemeStyle()
        
        applicationStatusBGView.isHidden = true
        applicationDetailsBGView.isHidden = true
        verificationStatusBGView.isHidden = true
        voidedDocumentCheckBGView.isHidden = true
        photoIdDocumentBGView.isHidden = true
        
       
        getData()
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    func setUI() {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : NAVIGATIONBAR_TINTCOLOR];
        self.navigationController?.navigationBar.tintColor = NAVIGATIONBAR_TINTCOLOR
        self.view.backgroundColor = Utility.hexStringToUIColor(hex: VIEW_BACKGROUND_COLOR)
        
        applicationStatusBGView.layer.borderWidth = 0.0
        applicationStatusBGView.layer.borderColor = Utility.hexStringToUIColor(hex: GRAY_COLOR).cgColor
        applicationStatusBGView.layer.masksToBounds = true
        applicationStatusBGView.layer.cornerRadius = 4.0
        
        applicationDetailsBGView.layer.borderWidth = 0.0
        applicationDetailsBGView.layer.borderColor = Utility.hexStringToUIColor(hex: GRAY_COLOR).cgColor
        applicationDetailsBGView.layer.masksToBounds = true
        applicationDetailsBGView.layer.cornerRadius = 4.0
        
        verificationStatusBGView.layer.borderWidth = 0.0
        verificationStatusBGView.layer.borderColor = Utility.hexStringToUIColor(hex: GRAY_COLOR).cgColor
        verificationStatusBGView.layer.masksToBounds = true
        verificationStatusBGView.layer.cornerRadius = 4.0
        
        voidedDocumentCheckBGView.layer.borderWidth = 0.0
        voidedDocumentCheckBGView.layer.borderColor = Utility.hexStringToUIColor(hex: GRAY_COLOR).cgColor
        voidedDocumentCheckBGView.layer.masksToBounds = true
        voidedDocumentCheckBGView.layer.cornerRadius = 4.0
        
        photoIdDocumentBGView.layer.borderWidth = 0.0
        photoIdDocumentBGView.layer.borderColor = Utility.hexStringToUIColor(hex: GRAY_COLOR).cgColor
        photoIdDocumentBGView.layer.masksToBounds = true
        photoIdDocumentBGView.layer.cornerRadius = 4.0
        
        
        taxReturnBgView.layer.borderWidth = 0.0
        taxReturnBgView.layer.borderColor = Utility.hexStringToUIColor(hex: GRAY_COLOR).cgColor
        taxReturnBgView.layer.masksToBounds = true
        taxReturnBgView.layer.cornerRadius = 4.0
        
        confessionBgView.layer.borderWidth = 0.0
        confessionBgView.layer.borderColor = Utility.hexStringToUIColor(hex: GRAY_COLOR).cgColor
        confessionBgView.layer.masksToBounds = true
        confessionBgView.layer.cornerRadius = 4.0
        
        lienBgView.layer.borderWidth = 0.0
        lienBgView.layer.borderColor = Utility.hexStringToUIColor(hex: GRAY_COLOR).cgColor
        lienBgView.layer.masksToBounds = true
        lienBgView.layer.cornerRadius = 4.0
        
        
        backgrounfView.backgroundColor = UIColor.clear
        
        btnFinish.setDefaultThemeStyle()
        btnClientNotIntrested.setDefaultThemeStyle()
        
    }
    override func viewDidLayoutSubviews() {
        scrlView.contentSize = CGSize(width: self.view.frame.size.width, height: bgViewHeightConstraint.constant);
    }
    func getData() {
        
        if (Reachability.isConnectedToNetwork()){
            // Create and add the view to the screen.
            self.view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            
            // All done!
            let paramsDict = ["applicationNumber":dictApplicationDetails["ApplicationNumber"] ?? ""] as Dictionary<String,Any>
            
            
            Utility.requestWithPostURLWithData(strUrl: BASE_URL + GET_AGREEMENT, paramDict: paramsDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
                
                DispatchQueue.main.async {
                    
                    self.applicationStatusBGView.isHidden = false
                    self.applicationDetailsBGView.isHidden = false
                    self.verificationStatusBGView.isHidden = false
                    self.voidedDocumentCheckBGView.isHidden = false
                    self.photoIdDocumentBGView.isHidden = false
                    
                    self.lineView.backgroundColor = Utility.hexStringToUIColor(hex: VIEW_BACKGROUND_COLOR)
                    self.lineViewDocuSign.backgroundColor = Utility.hexStringToUIColor(hex: VIEW_BACKGROUND_COLOR)
                    self.lineUploadDocument.backgroundColor = Utility.hexStringToUIColor(hex: VIEW_BACKGROUND_COLOR)
                    
                    self.lblApplicationId.text = self.dictApplicationDetails["ApplicationNumber"] as? String
                    self.lblApplicantName.text = self.dictApplicationDetails["BusinessApplicantName"] as? String
                    
                    
                    self.activityIndicator.stopAnimating()
                    if error == nil{
                        let httpResponse = urlResponse as! HTTPURLResponse
                        let responseString = String(data: dataResponse!, encoding: .utf8)!
                        self.dictResponse = Utility.convertToDictionary(text: responseString)!
                        
                        if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK && self.dictResponse.count > 0){
                            if !responseString.isEmpty {
                                self.setData()
                            }
                        }
                        else{
                            self.activityIndicator.stopAnimating()
                            
                            if((self.dictResponse["message"]) != nil){
                                Utility.showErrorAlert( message: (self.dictResponse["message"] as? String)!)
                            }
                            else if((self.dictResponse["details"]) != nil){
                                let dictError = (self.dictResponse["details"] as![Dictionary<String,Any>])[0]
                                if((dictError["message"]) != nil){
                                    Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                }
                            }
                            
                        }
                    }
                    else{
                        
                        self.activityIndicator.stopAnimating()
                        Utility.showErrorAlert(message: (error?.localizedDescription)!)
                        
                    }
                }
            })
        }
        else{
            self.activityIndicator.stopAnimating()
            Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
        }
    }
    
    
    func setData(){
        let strResendText = "Resend"
        let resend = ActiveType.custom(pattern: strResendText)
        self.lblResendEmailText.enabledTypes = [.mention, .hashtag, .url, resend]
        self.lblResendEmailText.customColor[resend] = UIColor.blue
        self.lblResendEmailText.customSelectedColor[resend] = UIColor.purple
        self.lblResendEmailText.text = self.lblResendEmailText.text! + " \(strResendText)"
        self.lblResendEmailText.handleCustomTap(for: resend) { element in
            self.resendDocument()
        }

        let strEmailId = (self.dictResponse["contract"] as! Dictionary<String,Any>)["email"] as! String
        let email = ActiveType.custom(pattern: strEmailId)
        self.lblEmailText.enabledTypes = [.mention, .hashtag, .url, email]
        self.lblEmailText.customColor[email] = UIColor.blue
        self.lblEmailText.customSelectedColor[email] = UIColor.purple
        self.lblEmailText.text = self.lblEmailText.text! + "\n \(strEmailId)"
        self.lblEmailText.handleCustomTap(for: email) { element in
            if let url = URL(string: "mailto:\(element)") {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                    // Fallback on earlier versions
                }
            }
        }
        
        if (self.dictResponse["confessionofjudgment"] != nil) {
             btnFinish.isUserInteractionEnabled = true
            btnFinish.alpha = 1.0
        }
        else{
             btnFinish.isUserInteractionEnabled = false
            btnFinish.alpha = 0.8
        }
       
        
        self.lblSignStatus.text = (self.dictResponse["contract"] as! Dictionary<String,Any>)["docuSignStatus"] as? String
        
        if (self.dictResponse["taxReturn"] != nil) {
            if ((self.dictResponse["taxReturn"] as! Dictionary<String,Any>) ["isDisplay"] as! Bool != true){
                bgViewHeightConstraint.constant = bgViewHeightConstraint.constant - taxreturnViewHeightConstraint.constant
                taxreturnViewHeightConstraint.constant = 0
            }
        
        }
        if (self.dictResponse["confession"] != nil) {
            if ((self.dictResponse["confession"] as! Dictionary<String,Any>) ["isDisplay"] as! Bool != true){
                bgViewHeightConstraint.constant = bgViewHeightConstraint.constant - confessionHeightConstraint.constant
                confessionHeightConstraint.constant = 0
            }
            
        }
        if (self.dictResponse["lien"] != nil) {
            if ((self.dictResponse["lien"] as! Dictionary<String,Any>) ["isDisplay"] as! Bool != true){
                bgViewHeightConstraint.constant = bgViewHeightConstraint.constant - lienHeightConstraint.constant
                lienHeightConstraint.constant = 0
            }
            
        }
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func resendDocument(){
        if (Reachability.isConnectedToNetwork()){
            // Create and add the view to the screen.
            self.view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            
            // All done!
            let paramsDict = ["applicationNumber":dictApplicationDetails["ApplicationNumber"] ?? ""] as Dictionary<String,Any>

            Utility.requestWithPostURLWithData(strUrl: BASE_URL + RESEND_DOCUMENT, paramDict: paramsDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
                
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    if error == nil{
                        let httpResponse = urlResponse as! HTTPURLResponse
                        let responseString = String(data: dataResponse!, encoding: .utf8)!
                        let responseDict = Utility.convertToDictionary(text: responseString)
                        
                        if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK && responseDict != nil){
                            if !responseString.isEmpty {
                                
                            }
                        }
                        else{
                            self.activityIndicator.stopAnimating()
                            
                            if((responseDict?["message"]) != nil){
                                Utility.showErrorAlert( message: (responseDict?["message"] as? String)!)
                            }
                            else if((responseDict?["details"]) != nil){
                                let dictError = (responseDict?["details"] as![Dictionary<String,Any>])[0]
                                if((dictError["message"]) != nil){
                                    Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                }
                            }
                            
                        }
                    }
                    else{
                        
                        self.activityIndicator.stopAnimating()
                        Utility.showErrorAlert(message: (error?.localizedDescription)!)
                        
                    }
                }
            })
        }
        else{
            self.activityIndicator.stopAnimating()
            Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
        }
    }
    
    @IBAction func btnManageDocumentClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "segManageDocument", sender: sender)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segManageDocument" {
            let objManageDocumnet:ManageDocumentViewController = segue.destination as! ManageDocumentViewController
           
            objManageDocumnet.applicationNumber = dictApplicationDetails["ApplicationNumber"] as! String
            
            switch (sender as! UIButton).tag {
            case 101:
                objManageDocumnet.documentType = DocumentType.Signeddocument
                let arrayData = (dictResponse["contract"] as? Dictionary<String,Any>)?["files"] as? Array<Dictionary<String,Any>>
                objManageDocumnet.arrayItems = arrayData!
                break
            case 102:
                objManageDocumnet.documentType = DocumentType.Bankstatement
                let arrayData = (dictResponse["voidedCheck"] as? Dictionary<String,Any>)?["files"] as? Array<Dictionary<String,Any>>
                objManageDocumnet.arrayItems = arrayData!
                break
            case 103:
                objManageDocumnet.documentType = DocumentType.Photoid
                let arrayData = (dictResponse["photoId"] as? Dictionary<String,Any>)?["files"] as? Array<Dictionary<String,Any>>
                objManageDocumnet.arrayItems = arrayData!
                break
            case 104:
                objManageDocumnet.documentType = DocumentType.TaxReturn
                let arrayData = (dictResponse["taxReturn"] as? Dictionary<String,Any>)?["files"] as? Array<Dictionary<String,Any>>
                objManageDocumnet.arrayItems = arrayData!
                break
            case 105:
                objManageDocumnet.documentType = DocumentType.Confession
                let arrayData = (dictResponse["confession"] as? Dictionary<String,Any>)?["files"] as? Array<Dictionary<String,Any>>
                objManageDocumnet.arrayItems = arrayData!
                break
            case 106:
                objManageDocumnet.documentType = DocumentType.Lien
                let arrayData = (dictResponse["lien"] as? Dictionary<String,Any>)?["files"] as? Array<Dictionary<String,Any>>
                objManageDocumnet.arrayItems = arrayData!
                break
            default:
                break
            }
            
            objManageDocumnet.delegate = self
        }
        if segue.identifier == "showFinalizePage" {
            let objAppFinalizPage:ApplicationFinalizationViewController = segue.destination as! ApplicationFinalizationViewController
            objAppFinalizPage.dictApplicationDetails = self.dictApplicationDetails
        }
    }
    func updateData(updatedDict : Dictionary<String, Any>) {
        self.dictResponse = updatedDict
        self.setData()
    }
    
    @IBAction func btnFinishClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "showFinalizePage", sender: nil)
    }
    
    @IBAction func btnClientNotIntrestedClicked(_ sender: Any) {
        if (Reachability.isConnectedToNetwork()){
            // Create and add the view to the screen.
            self.view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            
            // All done!
            let paramsDict = ["applicationNumber" : dictApplicationDetails["ApplicationNumber"] ?? "",
                              "rejectCode" : "700.20",
                              "reason" : "r5"
                ] as Dictionary<String,Any>
            
            Utility.requestWithPostURLWithData(strUrl: BASE_URL + DISCARD_APPLICATION, paramDict: paramsDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
                
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    if error == nil{
                        let httpResponse = urlResponse as! HTTPURLResponse
                        let responseString = String(data: dataResponse!, encoding: .utf8)!
                        let responseDict = Utility.convertToDictionary(text: responseString)
                        
                        if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK && responseDict != nil){
                            if !responseString.isEmpty {
                                self.objDelegate?.reloadData()
                                self.navigationController?.popViewController(animated: true)
                            }
                        }
                        else{
                            self.activityIndicator.stopAnimating()
                            
                            if((responseDict?["message"]) != nil){
                                Utility.showErrorAlert( message: (responseDict?["message"] as? String)!)
                            }
                            else if((responseDict?["details"]) != nil){
                                let dictError = (responseDict?["details"] as![Dictionary<String,Any>])[0]
                                if((dictError["message"]) != nil){
                                    Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                }
                            }
                            
                        }
                    }
                    else{
                        
                        self.activityIndicator.stopAnimating()
                        Utility.showErrorAlert(message: (error?.localizedDescription)!)
                        
                    }
                }
            })
        }
        else{
            self.activityIndicator.stopAnimating()
            Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
        }
//        {"applicationNumber":"DC0000021","rejectCode":"700.20","reason":"r5"}
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
