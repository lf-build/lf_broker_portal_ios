//
//  DocumentDisplayViewController.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 7/26/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit

class DocumentDisplayViewController: UIViewController {
    var applicationNumber:String!
    var fileId:String!
      var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    @IBOutlet weak var webView: UIWebView!
    
    var fileType:String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.setDefaultThemeStyle()
       getData()
        // Do any additional setup after loading the view.
    }
  
    func getData() {
        
        if (Reachability.isConnectedToNetwork()){
            // Create and add the view to the screen.
            self.view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            let paramsDict: [String : Any] = ["applicationNumber" : applicationNumber,
                                              "fileId" : fileId
                                            ]
            
            Utility.requestWithPostURLWithData(strUrl: BASE_URL + DOWNLOAD_DOCUMENT, paramDict: paramsDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
                
                DispatchQueue.main.async {

                    self.activityIndicator.stopAnimating()
                   
                    if error == nil{
                        let httpResponse = urlResponse as! HTTPURLResponse
                        let responseString = String(data: dataResponse!, encoding: .utf8)!
                        let responseDict = Utility.convertToDictionary(text: responseString)
                        
                        if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK && responseDict != nil){
                            if !responseString.isEmpty {
                                if let decodeData = NSData(base64Encoded: responseDict!["downloadString"] as! String, options: .ignoreUnknownCharacters) {
                                    if (self.fileType == "pdf"){
                                        self.webView.load(decodeData as Data, mimeType: "application/pdf", textEncodingName: "utf-8", baseURL:URL(string: "https://www.google.com")!)
                                    }
                                    else{
                                         self.webView.load(decodeData as Data, mimeType: "image/jpeg", textEncodingName: "utf-8", baseURL:URL(string: "https://www.google.com")!)
                                    }
                                    self.webView.scalesPageToFit = true
                                   self.webView.isMultipleTouchEnabled = true
                                    self.webView.allowsLinkPreview = true
                                    self.webView.allowsPictureInPictureMediaPlayback = true
                                } // since you don't have url, only encoded String

                            }
                        }
                        else{
                            self.activityIndicator.stopAnimating()
                            if((responseDict?["message"]) != nil){
                                Utility.showErrorAlert( message: (responseDict?["message"] as? String)!)
                            }
                            else if((responseDict?["details"]) != nil){
                                let dictError = (responseDict?["details"] as![Dictionary<String,Any>])[0]
                                if((dictError["message"]) != nil){
                                    Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                }
                            }
                            
                        }
                    }
                    else{
                        self.activityIndicator.stopAnimating()
                        Utility.showErrorAlert(message: (error?.localizedDescription)!)
                        
                    }
                }
            })
        }
        else{
            self.activityIndicator.stopAnimating()
            Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
