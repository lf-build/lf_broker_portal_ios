//
//  ApplicationDetailsViewController.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 6/28/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit

class ApplicationDetailsViewController: UIViewController{

    override func viewDidLoad() {
        
        setUI()
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
   
    func setUI() {
        
        self.navigationItem.title = "Application Details"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : NAVIGATIONBAR_TINTCOLOR];
        
        self.navigationController?.navigationBar.tintColor = NAVIGATIONBAR_TINTCOLOR
              self.view.backgroundColor = Utility.hexStringToUIColor(hex: VIEW_BACKGROUND_COLOR)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
