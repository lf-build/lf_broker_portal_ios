//
//  ApplicationFundedViewController.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 7/12/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit

class ApplicationFundedViewController: UIViewController {
    var dictApplicationDetails:Dictionary<String,Any>!
    @IBOutlet weak var lblApplicationId: UILabel!
    @IBOutlet weak var lblApplicantName: UILabel!
    @IBOutlet weak var lblBusinessName: UILabel!
    @IBOutlet weak var lblLoanAmount: UILabel!
    @IBOutlet weak var lblRepaymentAmount: UILabel!
    @IBOutlet weak var lblSellAmount: UILabel!
    @IBOutlet weak var lblTerm: UILabel!
    @IBOutlet weak var lblCommission: UILabel!
    

    @IBOutlet weak var backgrounfView: UIView!
    @IBOutlet weak var applicationStatusBGView: UIView!
    @IBOutlet weak var applicationDetailsBGView: UIView!
    
    
    override func viewDidLoad() {
        self.title = dictApplicationDetails["StatusName"] as? String
        applicationStatusBGView.layer.borderWidth = 0.0
        applicationStatusBGView.layer.borderColor = Utility.hexStringToUIColor(hex: GRAY_COLOR).cgColor
        applicationStatusBGView.layer.masksToBounds = true
        applicationStatusBGView.layer.cornerRadius = 4.0
        
        applicationDetailsBGView.layer.borderWidth = 0.0
        applicationDetailsBGView.layer.borderColor = Utility.hexStringToUIColor(hex: GRAY_COLOR).cgColor
        applicationDetailsBGView.layer.masksToBounds = true
        applicationDetailsBGView.layer.cornerRadius = 4.0
        

        
        backgrounfView.backgroundColor = UIColor.clear
         self.lblApplicationId.text = self.dictApplicationDetails["ApplicationNumber"] as? String
        self.lblApplicantName.text = self.dictApplicationDetails["BusinessApplicantName"] as? String
        self.lblBusinessName.text = self.dictApplicationDetails["LegalBusinessName"] as? String
        self.lblLoanAmount.text = String(format:"$%.2f",self.dictApplicationDetails["LoanAmount"] as! Float)
        self.lblRepaymentAmount.text = String(format:"$%.2f",self.dictApplicationDetails["RepaymentAmount"] as! Float)
        self.lblSellAmount.text = String(format:"%.2f",self.dictApplicationDetails["SellRate"] as! Float)
        self.lblTerm.text = String(format:"%.2f",self.dictApplicationDetails["Term"] as! Float)
        self.lblCommission.text = String(format:"$%.2f",self.dictApplicationDetails["CommissionAmount"] as! Float)
        
        self.view.backgroundColor = Utility.hexStringToUIColor(hex: VIEW_BACKGROUND_COLOR)
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
