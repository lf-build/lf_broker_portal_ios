//
//  AllApplicationViewController.swift
//  Business Lending
//
//  Created by Amitkumar Patel on 6/27/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit



class AllApplicationViewController: UIViewController {
   


    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnNewApplication: UIButton!
    var refreshControl: UIRefreshControl!
//   var arrayItems: [Dictionary] = [Dictionary<>]
      var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var arrayItems: [Dictionary<String, Any>] = []
    var pageNo:Int = 1
    var loadingData:Bool = true
    let searchController = UISearchController(searchResultsController: nil)
    var responseDict:Dictionary<String,Any>!
    
    override func viewDidLoad() {
       
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Refresh")
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        
        searchController.hidesNavigationBarDuringPresentation = true
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.sizeToFit()
        searchController.searchBar.delegate = self
        searchController.delegate=self
        tblView.tableHeaderView = searchController.searchBar
        tblView.contentOffset = CGPoint(x: 0, y: searchController.searchBar.frame.height)
       
      
        // Add to Table View
        if #available(iOS 10.0, *) {
            tblView.refreshControl = refreshControl
        } else {
            tblView.addSubview(refreshControl)
        }


        setUI()
        if self.arrayItems.count == 0{
            getData()
        }
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
         self.sideMenuController()?.sideMenu?.delegate = self
    }
    
    
    
    func setUI() {
        
        self.navigationItem.title = "All Application"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : NAVIGATIONBAR_TINTCOLOR];
        
        self.navigationController?.navigationBar.tintColor = NAVIGATIONBAR_TINTCOLOR
        let button = UIButton.init(type: .custom)
       button.setMenuButtonStyle()
        button.addTarget(self, action:#selector(SettingViewController.toggleSideMenu(sender:)), for: UIControlEvents.touchUpInside)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        self.view.backgroundColor = Utility.hexStringToUIColor(hex: VIEW_BACKGROUND_COLOR)
        tblView.backgroundColor = Utility.hexStringToUIColor(hex: VIEW_BACKGROUND_COLOR)
        btnNewApplication.setDefaultThemeStyle()
         activityIndicator.setDefaultThemeStyle()
        
    }
   
    
    
    
    func getData() {
        
        if (Reachability.isConnectedToNetwork()){
            // Create and add the view to the screen.
            self.view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            
            let arraySearchField:[String]=["ApplicationNumber","BusinessApplicantName","LegalBusinessName","StatusName"]
            // All done!
            
           
            
            var paramsDict: [String : Any] = [:]
           
            if (searchController.searchBar.text?.isEmpty)! {
               paramsDict = ["page" : pageNo,
                 "searchFields" : arraySearchField,
                 "sortBy" : "LastProgressDate.Time.Ticks",
                 "raw" : ["PartnerId":UserData.sharedInstance.partnerId]
                ]
            }
            else{
                paramsDict = ["page" : pageNo,
                              "searchFields" : arraySearchField,
                              "sortBy" : "LastProgressDate.Time.Ticks",
                              "raw" : ["PartnerId":UserData.sharedInstance.partnerId],
                              "searchText" : searchController.searchBar.text ?? ""
                ]
            }
            
            Utility.requestWithPostURLWithData(strUrl: BASE_URL + DASHBOARD_GRID_PATH, paramDict: paramsDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
                
                DispatchQueue.main.async {
                    self.refreshControl.endRefreshing()
                    self.activityIndicator.stopAnimating()
                    self.loadingData = false
                    if error == nil{
                        let httpResponse = urlResponse as! HTTPURLResponse
                        let responseString = String(data: dataResponse!, encoding: .utf8)!
                        self.responseDict = Utility.convertToDictionary(text: responseString)
                        
                        if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK && self.responseDict != nil){
                            if !responseString.isEmpty {
                                if self.pageNo == 1{
                                    self.arrayItems = self.responseDict?["items"] as! [Dictionary<String, Any>]
                                }
                                else{
                                    self.arrayItems.append(contentsOf: self.responseDict?["items"] as! [Dictionary<String, Any>])
                                }
                                self.tblView.reloadData()
                            }
                        }
                        else{
                            self.activityIndicator.stopAnimating()
                            self.tblView.reloadData()
                            if((self.responseDict?["message"]) != nil){
                                Utility.showErrorAlert( message: (self.responseDict?["message"] as? String)!)
                            }
                            else if((self.responseDict?["details"]) != nil){
                                let dictError = (self.responseDict?["details"] as![Dictionary<String,Any>])[0]
                                if((dictError["message"]) != nil){
                                    Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                }
                            }

                        }
                    }
                    else{
                        self.refreshControl.endRefreshing()
                        self.activityIndicator.stopAnimating()
                        Utility.showErrorAlert(message: (error?.localizedDescription)!)
                        
                    }
                }
            })
        }
        else{
            self.activityIndicator.stopAnimating()
            Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
        }
    }
    
    @objc func refreshData(refreshControl: UIRefreshControl) {
        pageNo = 1
        getData()
    }
    func stopLoading() {
        refreshControl.endRefreshing()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func redirectToVerificationPage(dictApplicationDetails:Dictionary<String,Any>) {
       
        let objApplicationVerificationVC : ApplicationVerificationViewController = objStoryBoard.instantiateViewController(withIdentifier: "ApplicationVerificationView") as! ApplicationVerificationViewController
        objApplicationVerificationVC.dictApplicationDetails = dictApplicationDetails
        self.navigationController?.pushViewController(objApplicationVerificationVC, animated: true)
    }
    func redirectToFinalizationPage(dictApplicationDetails:Dictionary<String,Any>){
        let objApplicationFinalizationVC : ApplicationFinalizationViewController = objStoryBoard.instantiateViewController(withIdentifier: "ApplicationFinalizationView") as! ApplicationFinalizationViewController
        objApplicationFinalizationVC.dictApplicationDetails = dictApplicationDetails
        self.navigationController?.pushViewController(objApplicationFinalizationVC, animated: true)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueShowDraftApplicationDetails" {
            let objNewApplicationVC:NewApplicationViewController = segue.destination as! NewApplicationViewController
            objNewApplicationVC.dictDraftedApplication = sender as! Dictionary<String, Any>
            objNewApplicationVC.objDelegate = self
        }
        
        
    }
    
    
    @IBAction func btnNewApplicationClicked(_ sender: Any) {
        if (self.sideMenuController()?.sideMenu?.isMenuOpen)!{
            self.sideMenuController()?.sideMenu?.toggleMenu()
        }
        let objNewApplicationVC : NewApplicationViewController = objStoryBoard.instantiateViewController(withIdentifier: "NewApplicationViewController") as! NewApplicationViewController
        objNewApplicationVC.objDelegate = self
        self.navigationController?.pushViewController(objNewApplicationVC, animated: true)
    }
 
    
    
    @IBAction func unwindToAllApplication(segue:UIStoryboardSegue) {
        if let objNewApplicationVC = segue.source as? NewApplicationViewController {
            print(objNewApplicationVC.dictNewApplicationDetails)
             self.getApplicationData(dictApplicationData: objNewApplicationVC.dictNewApplicationDetails)
        }
    }
    
    func getApplicationData(dictApplicationData:Dictionary<String,Any>) {
        if (Reachability.isConnectedToNetwork()){
            // Create and add the view to the screen.
            self.view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            
            let arraySearchField:[String]=["ApplicationNumber","BusinessApplicantName","LegalBusinessName","StatusName"]
            // All done!
            var paramsDict: [String : Any] = [:]
            paramsDict = ["page" : 1,
                          "searchFields" : arraySearchField,
                          "sortBy" : "LastProgressDate.Time.Ticks",
                          "raw" : ["PartnerId":UserData.sharedInstance.partnerId],
                          "searchText" : dictApplicationData["applicationNumber"] ?? ""
            ]
            
            
            Utility.requestWithPostURLWithData(strUrl: BASE_URL + DASHBOARD_GRID_PATH, paramDict: paramsDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
                
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    if error == nil{
                        let httpResponse = urlResponse as! HTTPURLResponse
                        let responseString = String(data: dataResponse!, encoding: .utf8)!
                        let dictData = Utility.convertToDictionary(text: responseString)
                        
                        if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK && (dictData != nil)){
                            if !responseString.isEmpty {
                                self.refreshData(refreshControl: self.refreshControl)
                               
                                let arrayItems = dictData?["items"] as! [Dictionary<String, Any>]
                              self.redirectToVerificationPage(dictApplicationDetails: arrayItems[0])
                            }
                        }
                        else{
                            self.activityIndicator.stopAnimating()
                            if((dictData?["message"]) != nil){
                                Utility.showErrorAlert( message: (dictData?["message"] as? String)!)
                            }
                            else if((dictData?["details"]) != nil){
                                let dictError = (dictData?["details"] as![Dictionary<String,Any>])[0]
                                if((dictError["message"]) != nil){
                                    Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                }
                            }
                            
                        }
                    }
                    else{
                        
                        Utility.showErrorAlert(message: (error?.localizedDescription)!)
                        
                    }
                }
            })
        }
        else{
            self.activityIndicator.stopAnimating()
            Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
        }
    }


}


// MARK: - NewApplicationDelegate

extension AllApplicationViewController:NewApplicationDelegate{
    func reloadData() {
        pageNo = 1
        getData()
    }
}

// MARK: - Side Menu
extension AllApplicationViewController:ENSideMenuDelegate{
    @IBAction func toggleSideMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    func sideMenuWillOpen() {
        
    }
    
    func sideMenuWillClose() {
        
    }
    
    func sideMenuDidClose() {
        self.view.isUserInteractionEnabled = true
    }
    
    func sideMenuDidOpen() {
        
        searchController.dismiss(animated: true) {
            
        }
        self.view.isUserInteractionEnabled = false
    }
    
    
    func sideMenuShouldOpenSideMenu() -> Bool {
        
        return true
    }
}

 // MARK: - Table view
extension AllApplicationViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if (arrayItems.count == 0 && self.loadingData == false){
            return 1
        }
        return arrayItems.count
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (arrayItems.count == 0){
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath)
            cell.textLabel?.text = "No Application Found"
            cell.textLabel?.textAlignment = .center
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "ApplicationTableViewCell", for: indexPath) as! ApplicationTableViewCell
        cell.setApplicationCellDetails(dictData: arrayItems[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (arrayItems.count == 0){
            cell.backgroundColor = UIColor.white
            return
        }
        
        if indexPath.row%2 == 1{
            cell.backgroundColor = Utility.hexStringToUIColor(hex: VIEW_BACKGROUND_COLOR)
        }
        else{
            cell.backgroundColor = UIColor.white
        }
        let lastElement = arrayItems.count - 1
        let totoalPage:String = (responseDict["totalPages"] as? String)!
        if  (totoalPage.characters.count > 0) {
            if (!loadingData && indexPath.row == lastElement) && pageNo < Int(totoalPage)!{
                
                loadingData = true
                pageNo+=1
                getData()
            }
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (arrayItems.count == 0){
            
            return
        }
        if (self.sideMenuController()?.sideMenu?.isMenuOpen)!{
            return;
        }
        searchController.dismiss(animated: true) {
            
        }
        
        let dictData = arrayItems[indexPath.row]
        let appStatus = dictData["StatusName"] as! String
        
        
        switch appStatus.trimmingCharacters(in: CharacterSet.whitespaces) {
            
        case APPLICATION_STATUS_DRAFT :
            self.performSegue(withIdentifier: "segueShowDraftApplicationDetails", sender: dictData)
            break
            
        case APPLICATION_STATUS_APPLICATION_SUBMITTED, APPLICATION_STATUS_FINANCIAL_VERIFICATION, APPLICATION_STATUS_PERSONAL_VERIFICATION, APPLICATION_STATUS_CREDIT_VERIFICATION:
            self.redirectToVerificationPage(dictApplicationDetails: dictData)
            break
            
        case APPLICATION_STATUS_PRICING:
            let objApplicationPricingVC : ApplicationPricingViewController = objStoryBoard.instantiateViewController(withIdentifier: "ApplicationPricingView") as! ApplicationPricingViewController
            objApplicationPricingVC.dictApplicationDetails = dictData
            self.navigationController?.pushViewController(objApplicationPricingVC, animated: true)
            break
            
        case APPLICATION_STATUS_AGREEMENT_DOCS_OUT, APPLICATION_STATUS_FUNDING_REVIEW:
            let objApplicationAgreementVC : ApplicationAgreementViewController = objStoryBoard.instantiateViewController(withIdentifier: "ApplicationAgreementView") as! ApplicationAgreementViewController
            objApplicationAgreementVC.dictApplicationDetails = dictData
            self.navigationController?.pushViewController(objApplicationAgreementVC, animated: true)
            break
            
        case APPLICATION_STATUS_APPROCED_FOR_FUNDING:
            redirectToFinalizationPage(dictApplicationDetails: dictData)
            break
            
        case APPLICATION_STATUS_FUNDED:
            let objApplicationFundedVC : ApplicationFundedViewController = objStoryBoard.instantiateViewController(withIdentifier: "ApplicationFundedView") as! ApplicationFundedViewController
            objApplicationFundedVC.dictApplicationDetails = dictData
            self.navigationController?.pushViewController(objApplicationFundedVC, animated: true)
            break
            
        case APPLICATION_STATUS_CANCELLED, APPLICATION_STATUS_NOT_INTERESTED, APPLICATION_STATUS_DECLINED, APPLICATION_STATUS_EXPIRED, APPLICATION_STATUS_DISCARDED:
            let objApplicationDeclinedVC : ApplicationDeclinedViewController = objStoryBoard.instantiateViewController(withIdentifier: "ApplicationDeclinedView") as! ApplicationDeclinedViewController
            objApplicationDeclinedVC.dictApplicationDetails = dictData
            self.navigationController?.pushViewController(objApplicationDeclinedVC, animated: true)
            break
            
        default:
            self.performSegue(withIdentifier: "segueShowApplicationDetails", sender: nil)
            break
            
        }
    }
}

 // MARK: -  Searchbar
extension AllApplicationViewController: UISearchBarDelegate, UISearchControllerDelegate{
   
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        pageNo = 1;
        if self.arrayItems.count > 0 {
            self.arrayItems.removeAll()
        }
        getData()
        searchBar.resignFirstResponder()
    }
    func didDismissSearchController(_ searchController: UISearchController) {
        getData()
    }

}
