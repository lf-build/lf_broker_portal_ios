//
//  ApplicationVerificationViewController.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 7/12/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit


class ApplicationVerificationViewController: UIViewController {
    
    
    @IBOutlet weak var lblApplicationId: UILabel!
    @IBOutlet weak var lblApplicantName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
   
    
    
    
    
    @IBOutlet weak var backgrounfView: UIView!
    @IBOutlet weak var applicationStatusBGView: UIView!
    @IBOutlet weak var applicationDetailsBGView: UIView!
    @IBOutlet weak var applicationEmailStatusView: UIView!
    @IBOutlet weak var applicationBankLinkingView: UIView!
    @IBOutlet weak var applicationunderWritingStatusView: UIView!
    
    @IBOutlet weak var imgViewEmailStatus: UIImageView!
    @IBOutlet weak var imgViewBankStatus: UIImageView!
    
    @IBOutlet weak var imgViewunderWritingStatus: UIImageView!
    
    @IBOutlet weak var lblResonHeighConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var viewReasonHeightConstraint: NSLayoutConstraint!
    
    var dictApplicationDetails:Dictionary<String,Any>!
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    


    override func viewDidLoad() {
        self.title = dictApplicationDetails["StatusName"] as? String
        
        applicationStatusBGView.layer.borderWidth = 0.0
        applicationStatusBGView.layer.borderColor = Utility.hexStringToUIColor(hex: GRAY_COLOR).cgColor
        applicationStatusBGView.layer.masksToBounds = true
        applicationStatusBGView.layer.cornerRadius = 4.0
        
        applicationDetailsBGView.layer.borderWidth = 0.0
        applicationDetailsBGView.layer.borderColor = Utility.hexStringToUIColor(hex: GRAY_COLOR).cgColor
        applicationDetailsBGView.layer.masksToBounds = true
        applicationDetailsBGView.layer.cornerRadius = 4.0
        
        applicationEmailStatusView.layer.borderWidth = 0.0
        applicationEmailStatusView.layer.borderColor = Utility.hexStringToUIColor(hex: GRAY_COLOR).cgColor
        applicationEmailStatusView.layer.masksToBounds = true
        applicationEmailStatusView.layer.cornerRadius = 4.0
        
        applicationBankLinkingView.layer.borderWidth = 0.0
        applicationBankLinkingView.layer.borderColor = Utility.hexStringToUIColor(hex: GRAY_COLOR).cgColor
        applicationBankLinkingView.layer.masksToBounds = true
        applicationBankLinkingView.layer.cornerRadius = 4.0
        
        
        applicationunderWritingStatusView.layer.borderWidth = 0.0
        applicationunderWritingStatusView.layer.borderColor = Utility.hexStringToUIColor(hex: GRAY_COLOR).cgColor
        applicationunderWritingStatusView.layer.masksToBounds = true
        applicationunderWritingStatusView.layer.cornerRadius = 4.0
        
        
        
        backgrounfView.backgroundColor = UIColor.clear
        
        self.view.backgroundColor = Utility.hexStringToUIColor(hex: VIEW_BACKGROUND_COLOR)
        activityIndicator.setDefaultThemeStyle()
        getData()
        
        
        applicationStatusBGView.isHidden = true
        applicationDetailsBGView.isHidden = true
        applicationEmailStatusView.isHidden = true
        applicationBankLinkingView.isHidden = true
        applicationunderWritingStatusView.isHidden = true
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    func getData() {
        
        if (Reachability.isConnectedToNetwork()){
            // Create and add the view to the screen.
            self.view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            
            // All done!
            
            let paramsDict = ["applicationNumber":dictApplicationDetails["ApplicationNumber"] ?? ""] as Dictionary<String,Any>
            
            
            
            
            Utility.requestWithPostURLWithData(strUrl: BASE_URL + GET_VERIFICATION, paramDict: paramsDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
                
                DispatchQueue.main.async {
                    
                    self.applicationStatusBGView.isHidden = false
                    self.applicationDetailsBGView.isHidden = false
                    self.applicationEmailStatusView.isHidden = false
                    self.applicationBankLinkingView.isHidden = false
                    self.applicationunderWritingStatusView.isHidden = false
                    
                    self.lblApplicationId.text = self.dictApplicationDetails["ApplicationNumber"] as? String
                    self.lblApplicantName.text = self.dictApplicationDetails["BusinessApplicantName"] as? String
                    
                    let arrayOwners:Array = self.dictApplicationDetails["Owners"] as! Array<Dictionary<String, Any>>
                    
                    if ((arrayOwners[0]["EmailAddress"] as? String) != nil){
                        self.lblEmail.text = Utility.setPhoneFormat(strPhone: (arrayOwners[0]["EmailAddress"] as? String)!)
                    }
//                    self.lblEmail.text = arrayOwners[0]["EmailAddress"] as? String
                    
                    if ((arrayOwners[0]["PhoneNumber"] as? String) != nil){
                         self.lblPhoneNumber.text = Utility.setPhoneFormat(strPhone: (arrayOwners[0]["PhoneNumber"] as? String)!)
                    }
                    
                  
                    

                    self.activityIndicator.stopAnimating()
                    if error == nil{
                        let httpResponse = urlResponse as! HTTPURLResponse
                        let responseString = String(data: dataResponse!, encoding: .utf8)!
                        let responseDict = Utility.convertToDictionary(text: responseString)
                        
                        if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK && responseDict != nil){
                            if !responseString.isEmpty {
                                if((responseDict?["emailStatus"]) != nil){
                                    if ((responseDict?["emailStatus"] as! Bool) == true) {
                                        self.imgViewEmailStatus.image = UIImage.init(named: "ApplicationStatusVerification")
                                    }
                                    else{
                                        self.imgViewEmailStatus.image = UIImage.init(named: "Application_Status_Declined_Disable")
                                    }
                                }
                                
                                if((responseDict?["bankLinkingStatus"]) != nil){
                                    if ((responseDict?["bankLinkingStatus"] as! Bool) == true) {
                                        self.imgViewBankStatus.image = UIImage.init(named: "ApplicationStatusVerification")
                                    }
                                    else{
                                        self.imgViewBankStatus.image = UIImage.init(named: "Application_Status_Declined_Disable")
                                    }
                                }
                                
                                if((responseDict?["underWritingStatus"]) != nil){
                                    if ((responseDict?["underWritingStatus"] as! Bool) == true) {
                                        self.imgViewunderWritingStatus.image = UIImage.init(named: "ApplicationStatusVerification")
                                    }
                                    else{
                                        self.imgViewunderWritingStatus.image = UIImage.init(named: "Application_Status_Declined_Disable")
                                    }
                                }
                                
                                
                                
                            }
                        }
                        else{
                            self.activityIndicator.stopAnimating()
                            
                            if((responseDict?["message"]) != nil){
                                Utility.showErrorAlert( message: (responseDict?["message"] as? String)!)
                            }
                            else if((responseDict?["details"]) != nil){
                                let dictError = (responseDict?["details"] as![Dictionary<String,Any>])[0]
                                if((dictError["message"]) != nil){
                                    Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                }
                            }
                            
                        }
                    }
                    else{
                        
                        self.activityIndicator.stopAnimating()
                        Utility.showErrorAlert(message: (error?.localizedDescription)!)
                        
                    }
                }
            })
        }
        else{
            self.activityIndicator.stopAnimating()
            Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
