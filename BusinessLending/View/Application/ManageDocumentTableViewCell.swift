//
//  ManageDocumentTableViewCell.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 7/24/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit

class ManageDocumentTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDocumentName: UILabel!
    
    @IBOutlet weak var imgViewDocument: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
