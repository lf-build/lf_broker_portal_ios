//
//  ApplicationTableViewCell.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 6/28/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit

class ApplicationTableViewCell: UITableViewCell {

    @IBOutlet weak var lblApplicationId: UILabel!
    @IBOutlet weak var lblApplicantName: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblRequestedAmount: UILabel!
    @IBOutlet weak var lblApplicationStatus: UILabel!
    @IBOutlet weak var lineView: UIView!
    
    @IBOutlet weak var shortNameBGView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func dropShadow() {
        self.bgView.layer.shadowColor = UIColor.gray.cgColor
        self.bgView.layer.shadowRadius = 2
        self.bgView.layer.shadowOpacity = 1
        self.bgView.layer.masksToBounds = false
        self.bgView.layer.shadowOffset = CGSize(width: 4, height: 4)
    
    }
    
    func setApplicationCellDetails(dictData: Dictionary<String, Any>) {
        self.lblApplicationId.font = UIFont.boldSystemFont(ofSize: 16)
        self.lblApplicationStatus.font = UIFont.boldSystemFont(ofSize: 15)
        self.lblApplicationId.text = dictData["ApplicationNumber"] as? String
        self.lblApplicantName.text = dictData["BusinessApplicantName"] as? String
        self.lblRequestedAmount.text = String(format:"$ %.2f",dictData["RequestedAmount"] as! Float)
        self.lblApplicantName.text = dictData["BusinessApplicantName"] as? String
        self.bgView.backgroundColor = .clear
        let appStatus = (dictData["StatusName"] as? String)?.lowercased()
        self.lblApplicationStatus.text = dictData["StatusName"] as? String
        if appStatus == "declined" || appStatus == "funded" || appStatus == "not interested" {
            self.lblApplicationStatus.textColor = Utility.hexStringToUIColor(hex: CLOSED_APPLICATION_COLOR)
            lineView.backgroundColor = Utility.hexStringToUIColor(hex: CLOSED_APPLICATION_COLOR)
        }
        else{
             self.lblApplicationStatus.textColor =  UIColor.black
            lineView.backgroundColor = UIColor.lightGray
        }
        
//        shortNameBGView.backgroundColor = getRandomColor()
//        shortNameBGView.layer.cornerRadius = 0.5 * shortNameBGView.bounds.size.height
//        shortNameBGView.clipsToBounds = true

        
    }
    
    func getRandomColor() -> UIColor{
        let randomRed:CGFloat = CGFloat(drand48())
        let randomGreen:CGFloat = CGFloat(drand48())
        let randomBlue:CGFloat = CGFloat(drand48())
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
        
    }
    
}
