//
//  ManageDocumentViewController.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 7/24/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit
import MobileCoreServices
import Photos

protocol DocumentUpdatesDelegate {
    func updateData(updatedDict : Dictionary<String, Any>)
}


class ManageDocumentViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UIDocumentMenuDelegate,UIDocumentPickerDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    @IBOutlet weak var tblView: UITableView!
    let imagePicker = UIImagePickerController()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var arrayItems = [Dictionary<String,Any>]()
    var applicationNumber = ""
    var documentType:DocumentType = DocumentType.Signeddocument
    // MARK: - public properties
    var delegate: DocumentUpdatesDelegate?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.setDefaultThemeStyle()
        let add = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(openUploadPicker(_:)))
        self.navigationItem.rightBarButtonItem = add
        imagePicker.delegate = self
        
        switch self.documentType {
        case DocumentType.Signeddocument:
            self.title = "Signature"
            break
        case DocumentType.Bankstatement:
            self.title = "Voided Check"
            break
        case DocumentType.Photoid:
            self.title = "Photo ID"
            break
        case DocumentType.TaxReturn:
            self.title = "Tax Return"
            break
        case DocumentType.Confession:
            self.title = "Confession"
            break
        case DocumentType.Lien:
            self.title = "Lien"
            break
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if (arrayItems.count == 0){
            return 1
        }
        return arrayItems.count
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (arrayItems.count == 0){
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath)
            cell.textLabel?.text = "No Document Found"
            cell.textLabel?.textAlignment = .center
            return cell
        }
        else{
            let cell:ManageDocumentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ManageDocumentTableViewCell", for: indexPath) as! ManageDocumentTableViewCell
            let fileName = arrayItems[indexPath.row]["fileName"] as? String
            cell.lblDocumentName.text = fileName
            let pathExtention = NSURL(fileURLWithPath: fileName!).pathExtension
            if pathExtention?.lowercased() == "pdf"{
                cell.imgViewDocument?.image = UIImage.init(named: "icon_pdf")
            }
            else{
                cell.imgViewDocument?.image = UIImage.init(named: "icon_jpeg")
            }
            return cell
        }
        
       
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if indexPath.row%2 == 1{
//            cell.backgroundColor = Utility.hexStringToUIColor(hex: VIEW_BACKGROUND_COLOR)
//        }
//        else{
//            cell.backgroundColor = UIColor.white
//        }
       
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if (arrayItems.count == 0){
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            deletDocument(fileId: (arrayItems[indexPath.row]["fileId"] as? String)!)
            
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        if (arrayItems.count > 0){
            self.performSegue(withIdentifier: "displayDocumentSeg", sender:arrayItems[indexPath.row])
        }
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "displayDocumentSeg"){
            let objeDisplayDocumentVC:DocumentDisplayViewController = segue.destination as! DocumentDisplayViewController
            objeDisplayDocumentVC.applicationNumber = self.applicationNumber
            objeDisplayDocumentVC.fileId = (sender as! Dictionary<String, Any>)["fileId"] as! String
            let fileName = (sender as! Dictionary<String, Any>)["fileName"] as? String
            let pathExtention = NSURL(fileURLWithPath: fileName!).pathExtension
            objeDisplayDocumentVC.fileType = pathExtention?.lowercased()

        }
    }
    
    
 
    
    @IBAction func openUploadPicker( _ sender: UIBarButtonItem){
        if (self.sideMenuController()?.sideMenu?.isMenuOpen)!{
            return;
        }
        let alert = UIAlertController(title: nil, message: "Choose photo", preferredStyle: UIAlertControllerStyle.actionSheet)
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            alert.addAction(UIAlertAction(title: "Take Photo", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                self.imagePicker.allowsEditing = false
                self.imagePicker.sourceType = .camera
                self.imagePicker.showsCameraControls = true
                self.present(self.imagePicker, animated: true, completion: nil)
            })
        }
        alert.addAction(UIAlertAction(title: "Choose Photo", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .photoLibrary
            // 1. request an UITraitCollection instance
            
            
            self.present(self.imagePicker, animated: true, completion: nil)
        })
        
        alert.addAction(UIAlertAction(title: "Upload Document", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            let importMenu = UIDocumentMenuViewController(documentTypes:[kUTTypePDF as String], in: .import)
            importMenu.delegate = self
            importMenu.modalPresentationStyle = .formSheet
            
            if (device_type == .pad){
                if let popoverController = importMenu.popoverPresentationController {
                    popoverController.barButtonItem = sender
                }
            }
            self.present(importMenu, animated: true, completion: nil)
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
            alert.dismiss(animated: true, completion: {
                
            })
        })
        
        if (device_type == .pad){
            if let popoverController = alert.popoverPresentationController {
                popoverController.barButtonItem = sender
            }
        }
         self.present(alert, animated: true, completion: nil)

        
    }
    
    // MARK: - Document Picker
    
    @available(iOS 8.0, *)
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let documetUrl = url as URL
        
          let pdfdata = NSData(contentsOf: documetUrl)
         var fileName = ""
        if  documetUrl.pathExtension == "pdf"{
            fileName = documetUrl.lastPathComponent
        }
        else{
            fileName = "\(documetUrl.lastPathComponent).pdf"
        }
        if fileName.isEmpty{
            
            switch self.documentType {
            case DocumentType.Signeddocument:
                fileName = "Signature_\(Date().millisecondsSince1970).pdf"
                break
            case DocumentType.Bankstatement:
                fileName = "Voided_Check_\(Date().millisecondsSince1970).pdf"
                break
            case DocumentType.Photoid:
                fileName = "Photo_ID_\(Date().millisecondsSince1970).pdf"
                break
            case DocumentType.TaxReturn:
                 fileName = "Taxt_Return_\(Date().millisecondsSince1970).pdf"
                break
            case DocumentType.Confession:
                 fileName = "Confession_\(Date().millisecondsSince1970).pdf"
                break
            case DocumentType.Lien:
                 fileName = "Lien_\(Date().millisecondsSince1970).pdf"
                break
            }
            
        }

        uploadDocumet(data: pdfdata!,fileName: fileName)
        
    }
    
    @available(iOS 8.0, *)
    public func documentMenu(_ documentMenu:     UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
        
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("we cancelled")
        dismiss(animated: true, completion: nil)
    }
    
    
   
    // MARK: - Image Picker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.imagePicker.dismiss(animated: true, completion: nil)
          let imageData: NSData = UIImageJPEGRepresentation(image!, 0.5)! as NSData
        var fileName = ""
        if let imageURL = info[UIImagePickerControllerReferenceURL] as? URL {
            let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
            let asset = result.firstObject
//            print(asset?.value(forKey: "filename") ?? )
            fileName = asset?.value(forKey: "filename") as! String
            
        }
        if fileName.isEmpty{
           
            switch self.documentType {
                case DocumentType.Signeddocument:
                    fileName = "Signature_\(Date().millisecondsSince1970).jpeg"
                    break
                case DocumentType.Bankstatement:
                    fileName = "Voided_Check_\(Date().millisecondsSince1970).jpeg"
                    break
                case DocumentType.Photoid:
                    fileName = "Photo_ID_\(Date().millisecondsSince1970).jpeg"
                    break
            case DocumentType.TaxReturn:
                fileName = "Tax_Return_\(Date().millisecondsSince1970).jpeg"
                break
            case DocumentType.Confession:
                fileName = "Confession_\(Date().millisecondsSince1970).jpeg"
                break
            case DocumentType.Lien:
                fileName = "Lien_\(Date().millisecondsSince1970).jpeg"
                break
            }
        }
        uploadDocumet(data: imageData,fileName: fileName.lowercased())
        
        
    }
    
    
    
    // MARK: - Delete Document
    
    func deletDocument(fileId: String) {
        if (Reachability.isConnectedToNetwork()){
            self.view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            let paramsDict: [String : Any] = ["applicationNumber" : self.applicationNumber,
                                              "fileId" : fileId,
                                              "fileType" : self.documentType.rawValue
            ]
            Utility.requestWithPostURLWithData(strUrl: BASE_URL + DELET_DOCUMENT, paramDict:paramsDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
                
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    if error == nil{
                        let httpResponse = urlResponse as! HTTPURLResponse
                        let responseString = String(data: dataResponse!, encoding: .utf8)!
                        let dictResponse = Utility.convertToDictionary(text: responseString)
                        
                        if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK){
                            self.getAgreementData()
                        }
                        else{
                            if((dictResponse?["message"]) != nil){
                                Utility.showErrorAlert( message: (dictResponse?["message"] as? String)!)
                            }
                            else if((dictResponse?["details"]) != nil){
                                let dictError = (dictResponse?["details"] as![Dictionary<String,Any>])[0]
                                if((dictError["message"]) != nil){
                                    Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                }
                            }
                        }
                    }
                    else{
                        Utility.showErrorAlert( message: (error?.localizedDescription)!)
                        
                    }
                }
            })
        }
        else{
            self.activityIndicator.stopAnimating()
            Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
        }
    }

    
    // MARK: - Upload Document
    
    func uploadDocumet(data: NSData, fileName:String){
        if (Reachability.isConnectedToNetwork()){
            // Create and add the view to the screen.
          
            let imageStr = data.base64EncodedString(options: .lineLength64Characters)
            
            self.view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            
            let paramsDict: [String : Any] = ["applicationNumber" : self.applicationNumber,
                                              "uploadFileType" : self.documentType.rawValue,
                                              "fileList" : [["file" : fileName,
                                                             "dataUrl" : "data:image/jpeg;base64, \(imageStr)"
                                                ]]
            ]
            Utility.requestWithPostURLWithData(strUrl: BASE_URL + UPLOAD_DOCUMENT, paramDict:paramsDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
                
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    if error == nil{
                        let httpResponse = urlResponse as! HTTPURLResponse
                        let responseString = String(data: dataResponse!, encoding: .utf8)!
                        let dictResponse = Utility.convertToDictionary(text: responseString)
                        
                        if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK ){
                            self.getAgreementData()
                        }
                        else{
                            if((dictResponse?["message"]) != nil){
                                Utility.showErrorAlert( message: (dictResponse?["message"] as? String)!)
                            }
                            else if((dictResponse?["details"]) != nil){
                                let dictError = (dictResponse?["details"] as![Dictionary<String,Any>])[0]
                                if((dictError["message"]) != nil){
                                    Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                }
                            }
                        }
                    }
                    else{
                        Utility.showErrorAlert( message: (error?.localizedDescription)!)
                        
                    }
                }
            })
        }
        else{
            self.activityIndicator.stopAnimating()
            Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
        }
    }

    func getAgreementData() {
        
       
        if (Reachability.isConnectedToNetwork()){
            // Create and add the view to the screen.
            self.view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            
            // All done!
            //            let paramsDict = ["applicationNumber":dictApplicationDetails["ApplicationNumber"] ?? ""] as Dictionary<String,Any>
            let paramsDict = ["applicationNumber": self.applicationNumber] as Dictionary<String,Any>
            
            Utility.requestWithPostURLWithData(strUrl: BASE_URL + GET_AGREEMENT, paramDict: paramsDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
                
                DispatchQueue.main.async {
                    
                    
                    self.activityIndicator.stopAnimating()
                    if error == nil{
                        let httpResponse = urlResponse as! HTTPURLResponse
                        let responseString = String(data: dataResponse!, encoding: .utf8)!
                        let dictResponse = Utility.convertToDictionary(text: responseString)!
                        
                        if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK && dictResponse.count > 0){
                            if !responseString.isEmpty {
                                
                               self.delegate?.updateData(updatedDict: dictResponse)
                                switch self.documentType {
                                case DocumentType.Signeddocument:
                                    self.arrayItems = (dictResponse["contract"] as! Dictionary<String,Any>)["files"] as! Array<Dictionary<String,Any>>
                                    break
                                case DocumentType.Bankstatement:
                                     self.arrayItems = (dictResponse["voidedCheck"] as! Dictionary<String,Any>)["files"] as! Array<Dictionary<String,Any>>
                                    break
                                case DocumentType.Photoid:
                                     self.arrayItems = (dictResponse["photoId"] as! Dictionary<String,Any>)["files"] as! Array<Dictionary<String,Any>>
                                    break
                                case DocumentType.TaxReturn:
                                    self.arrayItems = (dictResponse["taxReturn"] as! Dictionary<String,Any>)["files"] as! Array<Dictionary<String,Any>>
                                    break
                                case DocumentType.Confession:
                                    self.arrayItems = (dictResponse["confession"] as! Dictionary<String,Any>)["files"] as! Array<Dictionary<String,Any>>
                                    break
                                case DocumentType.Lien:
                                    self.arrayItems = (dictResponse["lien"] as! Dictionary<String,Any>)["files"] as! Array<Dictionary<String,Any>>
                                    break

                                }
                                
                                self.tblView.reloadData()
                                
                            }
                        }
                        else{
                            self.activityIndicator.stopAnimating()
                            
                            if((dictResponse["message"]) != nil){
                                Utility.showErrorAlert( message: (dictResponse["message"] as? String)!)
                            }
                            else if((dictResponse["details"]) != nil){
                                let dictError = (dictResponse["details"] as![Dictionary<String,Any>])[0]
                                if((dictError["message"]) != nil){
                                    Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                }
                            }
                            
                        }
                    }
                    else{
                        
                        self.activityIndicator.stopAnimating()
                        Utility.showErrorAlert(message: (error?.localizedDescription)!)
                        
                    }
                }
            })
        }
        else{
            self.activityIndicator.stopAnimating()
            Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
