//
//  SignUpViewController.swift
//  SwiftDemo
//
//  Created by Amitkumar Patel on 6/23/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit

class SignUpViewController1: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setUI(){
        self.navigationItem.title = "Sign Up"
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : Utility.hexStringToUIColor(hex: BUTTON_BACKGROUND_COLOR)];
        
        self.navigationController?.navigationBar.tintColor = Utility.hexStringToUIColor(hex: BUTTON_BACKGROUND_COLOR);
        self.view.backgroundColor = Utility.hexStringToUIColor(hex: VIEW_BACKGROUND_COLOR)

        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
