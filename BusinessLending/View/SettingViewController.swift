//
//  SettingViewController.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 6/22/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController, ENSideMenuDelegate {

    @IBOutlet weak var lblWelcomeMessage: UILabel!
    var settingType:String = "Personal Details"
    
    override func viewDidLoad() {
        self.sideMenuController()?.sideMenu?.delegate = self
        setWelcomeMessage()
        setUI()
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.title = settingType
        self.sideMenuController()?.sideMenu?.delegate = self
    }
    // MARK: - ENSideMenu Delegate
    @IBAction func toggleSideMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    func sideMenuWillOpen() {
        
    }
    
    func sideMenuWillClose() {
        
    }
    
    func sideMenuDidClose() {
        self.view.isUserInteractionEnabled = true
    }
    
    func sideMenuDidOpen() {
        self.view.isUserInteractionEnabled = false
    }
    
    
    func sideMenuShouldOpenSideMenu() -> Bool {
        
        return true
    }
    
    func setUI() {

        self.navigationItem.title = "Setting"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : NAVIGATIONBAR_TINTCOLOR];
        
        self.navigationController?.navigationBar.tintColor = NAVIGATIONBAR_TINTCOLOR      
        let button = UIButton.init(type: .custom)
      button.setMenuButtonStyle()
        button.addTarget(self, action:#selector(SettingViewController.toggleSideMenu(sender:)), for: UIControlEvents.touchUpInside)

        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    
        self.view.backgroundColor = Utility.hexStringToUIColor(hex: VIEW_BACKGROUND_COLOR)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func rightButtonAction(){
        UserData.sharedInstance.isUserAlreadyLogIn = false
        appDelegate.loadLoginScreen()
    }
    
   

    func setWelcomeMessage() {
        let welComeMessage = "Hi your userId id " + UserData.sharedInstance.userId + " and token is " + UserData.sharedInstance.token
        let attributedString = NSMutableAttributedString(string: welComeMessage)
        
        let rangeEmailId = (welComeMessage as NSString).range(of: UserData.sharedInstance.userId)
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.blue, range: rangeEmailId)
        
        let rangeUserType = (welComeMessage as NSString).range(of: UserData.sharedInstance.token)
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.blue, range: rangeUserType)
        
        lblWelcomeMessage.attributedText = attributedString
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
