//
//  LogInViewController.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 6/21/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit
import ActiveLabel
class LogInViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    
    
    @IBOutlet weak var txtFldUserName: UITextField!
    @IBOutlet weak var txtFldPassword: UITextField!
    @IBOutlet weak var txtFldPartnerType: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!

    @IBOutlet weak var topView: UIView!
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    
    var pickOption = ["divergent-capital"]
    let pickerView = UIPickerView()
  

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
//        txtFldUserName.text = "manish.p@mailinator.com"
//        txtFldPassword.text = "Abcd@123"
        
        
//        txtFldUserName.text = "pushpraj.rajput@mailinator.com"
//        txtFldPassword.text = "Sigma@123"
        
//        txtFldUserName.text = "dcbroker004@mailinator.com"
//        txtFldPassword.text = "Sigma@1234"
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(LogInViewController.handleTap(_:)))
        tap.delegate = self as? UIGestureRecognizerDelegate
        self.view.addGestureRecognizer(tap)
        
        pickerView.showsSelectionIndicator = true
        pickerView.delegate = self
        pickerView.dataSource = self
        
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = BLACK_TINTCOLOR
        toolBar.sizeToFit()
//        toolBar.backgroundColor = Utility.hexStringToUIColor(hex: THEME_COLOR)
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(LogInViewController.donePicker))
        doneButton.tintColor = UIColor.black
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(LogInViewController.cancelPicker))
        cancelButton.tintColor = UIColor.black
        
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        txtFldPartnerType.inputView = pickerView
        txtFldPartnerType.inputAccessoryView = toolBar
        
        // Do any additional setup after loading the view.
    }
    
    func setUI() {
        self.navigationItem.title = "Log In"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : NAVIGATIONBAR_TINTCOLOR];
        self.navigationController?.navigationBar.tintColor = NAVIGATIONBAR_TINTCOLOR
        
        
        topView.backgroundColor = Utility.hexStringToUIColor(hex: TOPBAR_BACKGROUND_COLOR)
        self.view.backgroundColor = Utility.hexStringToUIColor(hex: VIEW_BACKGROUND_COLOR)
       
        btnLogin.setDefaultThemeStyle()
        btnSignUp.setDefaultThemeStyle()
       
        activityIndicator.setDefaultThemeStyle()
        let btn = UIButton()
        btn.frame = CGRect(x: 0 , y: 0, width: txtFldPassword.frame.size.height, height: txtFldPassword.frame.size.height)
        btn.translatesAutoresizingMaskIntoConstraints = false
         btn.titleLabel?.font = UIFont.fontAwesome(ofSize: txtFldPassword.frame.size.height-6)
        btn.setTitle(String.fontAwesomeIcon(code: "fa-question-circle-o")!, for: .normal)
        btn.setTitleColor(Utility.hexStringToUIColor(hex: THEME_COLOR), for: .normal)
        btn.addTarget(self, action: #selector(showForgotPasswordAlert), for: UIControlEvents.touchUpInside)

        txtFldPassword.rightView = btn;
        txtFldPassword.rightViewMode = UITextFieldViewMode.always
        
    }
    
    @IBAction func showForgotPasswordAlert() {
        let alertController = UIAlertController(title: "Forgot Password?", message: "Please input your email:", preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "Confirm", style: .default) { (_) in
            if let field = alertController.textFields?[0] {
                // store your data
                if(Utility.validateEmail(emailString: field.text)){
                    self.callForgotPasswordAPI(strEmailId: field.text!)
                }
                else{
                    Utility.showErrorAlert( message: "Please enter valid email")
                    self.showForgotPasswordAlert()
                }
            } else {
                // user did not fill field
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        
        alertController.addTextField { (textField) in
            textField.placeholder = "Email"
        }
        
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    func callForgotPasswordAPI(strEmailId:String){
        if (Reachability.isConnectedToNetwork()){
            // Create and add the view to the screen.
            self.view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            // All done!
            let paramsDict: [String : Any] = ["username" :strEmailId,
                                              "realm" : APPLICATION_ACCESS_REALM
            ]
            Utility.requestWithPostURLWithData(strUrl: BASE_URL + FORGOT_PASSWORD_PATH, paramDict:paramsDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
                
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    if error == nil{
                        let httpResponse = urlResponse as! HTTPURLResponse
                        let responseString = String(data: dataResponse!, encoding: .utf8)
            
                        let dict = Utility.convertToDictionary(text: responseString!)
                        
                        if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK){
                            Utility.showSuccessAlert(message: "Email sent successfully")
                        }
                        else{
                            if((dict?["message"]) != nil){
                                Utility.showErrorAlert( message: (dict?["message"] as? String)!)
                            }
                            else if((dict?["details"]) != nil){
                                let dictError = (dict?["details"] as![Dictionary<String,Any>])[0]
                                if((dictError["message"]) != nil){
                                    Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                }
                            }
                        }
                    }
                    else{
                        Utility.showErrorAlert( message: (error?.localizedDescription)!)
                        
                    }
                }
            })
        }
        else{
            self.activityIndicator.stopAnimating()
            Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
        }
    }
    
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnLoginClicked(_ sender: Any) {
        if validateForm() {
            if (Reachability.isConnectedToNetwork()){
                // Create and add the view to the screen.
                self.view.addSubview(activityIndicator)
                activityIndicator.startAnimating()
                // All done!
                let paramsDict: [String : Any] = ["username" : txtFldUserName.text!,
                                              "password" : txtFldPassword.text!,
                                              "realm" : APPLICATION_ACCESS_REALM,
                                              "client" : txtFldPartnerType.text!
                                              ]
               
                
                Utility.requestWithPostURLWithData(strUrl: BASE_URL + LOGIN_PATH, paramDict:paramsDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
                    
                    DispatchQueue.main.async {
                        self.activityIndicator.stopAnimating()
                        if error == nil{
                            let httpResponse = urlResponse as! HTTPURLResponse
                            let responseString = String(data: dataResponse!, encoding: .utf8)
                            print(responseString!)
                            let dict = Utility.convertToDictionary(text: responseString!)
                            
                            if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK && dict != nil){
                                UserData.sharedInstance.isUserAlreadyLogIn = true
                                UserData.sharedInstance.userId = dict?["userId"] as! String
                                UserData.sharedInstance.partnerId = dict?["partnerId"] as! String
                                UserData.sharedInstance.token = dict?["token"] as! String
                                UserData.sharedInstance.userName = self.txtFldUserName.text!
                                Utility.getWhoAmIData()
                                appDelegate.loadDashboard()
                                
                                
                               
                                
                            }
                            else{
                                if((dict?["message"]) != nil){
                                    Utility.showErrorAlert( message: (dict?["message"] as? String)!)
                                }
                                else if((dict?["details"]) != nil){
                                    let dictError = (dict?["details"] as![Dictionary<String,Any>])[0]
                                    if((dictError["message"]) != nil){
                                        Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                    }
                                }
                            }
                        }
                        else{
                            Utility.showErrorAlert( message: (error?.localizedDescription)!)
                            
                        }
                    }
                })
            }
            else{
                self.activityIndicator.stopAnimating()
                Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
            }
            
        }
    }
   
    func validateForm() -> Bool {
        if (txtFldPartnerType.text?.isEmpty)!{
            Utility.showErrorAlert( message: "Please select partner type")
            txtFldPartnerType.setRequiredFildErrorStyle()
            return false;
        }
        
        else if (txtFldUserName.text?.isEmpty)! {
            Utility.showErrorAlert( message: "UserName should not be blank")
            txtFldUserName.setRequiredFildErrorStyle()
            return false;
        }
        else if( !Utility.checkLengthValidation(strInput: txtFldUserName.text, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100)){
            Utility.showErrorAlert( message: "UserName required minimum \(MINLENGTH_2) and maximum \(MAXLENGTH_100) characters . ")
             txtFldUserName.setRequiredFildErrorStyle()
            return false;
        }
        else if (txtFldPassword.text?.isEmpty)!{
            Utility.showErrorAlert( message: "Password should not be blank")
            txtFldPassword.setRequiredFildErrorStyle()
            return false;
        }
        else if(!Utility.checkLengthValidation(strInput: txtFldPassword.text, minLength: MINLENGTH_5, maxLength: MAXLENGTH_200)){
            Utility.showErrorAlert( message: "Password required minimum \(MINLENGTH_5) and maximum \(MAXLENGTH_200) characters . ")
            txtFldPassword.setRequiredFildErrorStyle()
            return false;
        }
        return true;
    }
    
    
   
    
    // MARK: - UITextField Delegate Method
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.isEqual(txtFldPartnerType) {
            if !(txtFldPartnerType.text?.isEmpty)! {
                pickerView.selectRow(pickOption.index(of: txtFldPartnerType.text!)!, inComponent: 0, animated: true)
            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField .isEqual(txtFldUserName) {
            txtFldPassword.becomeFirstResponder()
        }
        else if textField.isEqual(txtFldPassword){
            self.view.endEditing(true)
        }
        else{
            self.view.endEditing(true)
        }
        return false
    }
    // MARK: - UIPickerView Delegate Method
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickOption[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
    @objc func cancelPicker(){
        txtFldPartnerType.resignFirstResponder()
    }
    @objc func donePicker(){
        txtFldPartnerType.text = pickOption[pickerView.selectedRow(inComponent: 0)]
        txtFldPartnerType.resignFirstResponder()
    }
    
    
    // MARK: - Call Webservice
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
