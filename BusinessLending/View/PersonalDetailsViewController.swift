//
//  PersonalDetailsViewController.swift
//  Business Lending
//
//  Created by Amitkumar Patel on 6/27/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit
import SDWebImage
import Photos
class PersonalDetailsViewController: UITableViewController, ENSideMenuDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate,KeyboardToolbarDelegate {
    
    @IBOutlet weak var txtFldFirstName: UITextField!
    @IBOutlet weak var txtFldLastName: UITextField!
    @IBOutlet weak var txtFldDOB: UITextField!
    @IBOutlet weak var txtFldStreet: UITextField!
    @IBOutlet weak var txtFldCity: UITextField!
    @IBOutlet weak var txtFldPostcode: UITextField!
    @IBOutlet weak var txtFldMobileNumber: UITextField!
    @IBOutlet weak var txtFldCompanyName: UITextField!
    @IBOutlet weak var imgViewProfilePic: UIImageView!
    @IBOutlet weak var profileBackgroundImage: UIImageView!
    @IBOutlet weak var visualEffectView: UIVisualEffectView!
    let datePickerView = UIDatePicker()
    let imagePicker = UIImagePickerController()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var responseString:String = ""
    var objKeyboardToolbar: KeyboarToolbar = KeyboarToolbar.sharedInstance
     var activeTextField = UITextField()
    override func viewDidLoad() {
        
   //CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: self.tableView.frame.size.width+200)
        self.sideMenuController()?.sideMenu?.delegate = self
        setUI()
         objKeyboardToolbar.objKeyboardToolbarDelegate = self
        getData()
        self.imagePicker.delegate = self
        let tapGestureImage = UITapGestureRecognizer(target: self, action: #selector(PersonalDetailsViewController.openImagePicker))
        tapGestureImage.cancelsTouchesInView = true
        visualEffectView.addGestureRecognizer(tapGestureImage)
              visualEffectView.isUserInteractionEnabled = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        tapGesture.cancelsTouchesInView = true
        tableView.addGestureRecognizer(tapGesture)
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = BLACK_TINTCOLOR
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(LogInViewController.donePicker))
        doneButton.tintColor = UIColor.black
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(LogInViewController.cancelPicker))
        cancelButton.tintColor = UIColor.black
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.maximumDate = Date()
        txtFldDOB.inputView = datePickerView
        txtFldDOB.inputAccessoryView = toolBar
        
        self.imgViewProfilePic.image = UIImage(named: "user_ avatar")
        self.profileBackgroundImage.image = UIImage(named: "user_ avatar")
        
        
        let imgUrl:String = UserData.sharedInstance.image
        let url = URL(string: imgUrl)
        if Utility.validateUrl(urlString: imgUrl){
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async {
                    self.imgViewProfilePic.image = UIImage(data: data!)
                    self.profileBackgroundImage.image = UIImage(data: data!)
                }
            }
        }
        
       
        
        
        super.viewDidLoad()

        
    }

    override func viewWillLayoutSubviews() {
        
    }
    func getData() {
        if (Reachability.isConnectedToNetwork()){
            // Create and add the view to the screen.
            self.view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            // All done!
            //            let strBody = "partnerId=" + UserData.sharedInstance.partnerId
            
            let paramsDict: [String : Any] = ["partnerId" : UserData.sharedInstance.partnerId]
            Utility.requestWithPostURLWithData(strUrl: BASE_URL + PERSONAL_DETAILS_PATH, paramDict:paramsDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
                
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    if error == nil{
                        let httpResponse = urlResponse as! HTTPURLResponse
                        self.responseString = String(data: dataResponse!, encoding: .utf8)!
                        let dict = Utility.convertToDictionary(text: self.responseString)
                        
                        if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK && dict != nil){
                            self.txtFldFirstName.text = dict?["firstName"] as? String
                            self.txtFldLastName.text = dict?["lastName"] as? String
                            self.txtFldDOB.text = dict?["dateOfBirth"] as? String
                            self.txtFldMobileNumber.text = (dict?["mobileNumber"] as? String)?.formatePhoneNumber()
                            self.txtFldCompanyName.text = dict?["companyName"] as? String
                            self.txtFldStreet.text = dict?["street"] as? String
                            self.txtFldCity.text = dict?["city"] as? String
                            self.txtFldPostcode.text = dict?["postCode"] as? String
                           
                        }
                        else{
                            if((dict?["message"]) != nil){
                                Utility.showErrorAlert( message: (dict?["message"] as? String)!)
                            }
                            else if((dict?["details"]) != nil){
                                let dictError = (dict?["details"] as![Dictionary<String,Any>])[0]
                                if((dictError["message"]) != nil){
                                    Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                }
                            }
                        }
                    }
                    else{
                        Utility.showErrorAlert( message: (error?.localizedDescription)!)
                        
                    }
                }
            })
        }
        else{
            self.activityIndicator.stopAnimating()
            Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
        }
        
    }
    
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }
    func cancelPicker(){
        txtFldDOB.resignFirstResponder()
    }
    func donePicker(){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DATE_FORMAT
        let dateString = dateFormatter.string(from: datePickerView.date)
        txtFldDOB.text = dateString
        txtFldDOB.resignFirstResponder()
        txtFldMobileNumber.becomeFirstResponder()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.sideMenuController()?.sideMenu?.delegate = self
    }
    // MARK: - ENSideMenu Delegate
    @IBAction func toggleSideMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    func sideMenuWillOpen() {
        self.view.endEditing(true)
    }
    
    func sideMenuWillClose() {
        
    }
    
    func sideMenuDidClose() {
        self.view.isUserInteractionEnabled = true
    }
    
    func sideMenuDidOpen() {
        self.view.isUserInteractionEnabled = false
    }
    
    func sideMenuShouldOpenSideMenu() -> Bool {
        
        return true
    }
    
    func setUI() {
        
        self.navigationItem.title = "Personal Details"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : NAVIGATIONBAR_TINTCOLOR];
        
        self.navigationController?.navigationBar.tintColor = NAVIGATIONBAR_TINTCOLOR
        let button = UIButton.init(type: .custom)
       button.setMenuButtonStyle()
        button.addTarget(self, action:#selector(SettingViewController.toggleSideMenu(sender:)), for: UIControlEvents.touchUpInside)

        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        self.view.backgroundColor = Utility.hexStringToUIColor(hex: VIEW_BACKGROUND_COLOR)
        
        
       
        
        
        imgViewProfilePic.layer.cornerRadius = 0.5 * imgViewProfilePic.bounds.size.height
        imgViewProfilePic.clipsToBounds = true
        imgViewProfilePic.layer.borderColor = UIColor.black.cgColor
        imgViewProfilePic.layer.borderWidth = 1.0

        imgViewProfilePic.contentMode = .scaleAspectFit
        profileBackgroundImage.contentMode = .scaleToFill
        activityIndicator.setDefaultThemeStyle()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - UITextField Delegate Method
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.isEqual(txtFldDOB) {
            if !(txtFldDOB.text?.isEmpty)! {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = DATE_FORMAT
                let selectedDate = dateFormatter.date(from: txtFldDOB.text!)
                datePickerView.setDate(selectedDate!, animated: true)
            }
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtFldMobileNumber{
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            if  (text.characters.count == 0 && newLength == 1){
                textField.text = "(" + textField.text!
            }
            
            if  (text.characters.count == 2 && newLength == 1){
                textField.text = textField.text?.substring(to: (textField.text?.index(before: (textField.text?.endIndex)!))!)
            }
            
            if  (text.characters.count == 4 && newLength == 5){
                textField.text =  textField.text! + ") "
            }
            
            if  (text.characters.count == 7 && newLength == 6){
                textField.text = textField.text?.substring(to: (textField.text?.index(before: (textField.text?.endIndex)!))!)
                textField.text = textField.text?.substring(to: (textField.text?.index(before: (textField.text?.endIndex)!))!)
            }
            
            if  (text.characters.count == 9 && newLength == 10){
                textField.text =  textField.text! + "-"
            }
            
            if  (text.characters.count == 11 && newLength == 10){
                textField.text = textField.text?.substring(to: (textField.text?.index(before: (textField.text?.endIndex)!))!)
            }
            return newLength <= PHONE_NUMBER_LENGTH
        }
        if textField == txtFldPostcode{
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= ZIP_CODE_LENGTH
        }
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if (self.sideMenuController()?.sideMenu?.isMenuOpen)!{
            return false;
        }
        if textField.inputAccessoryView == nil {
            textField.inputAccessoryView = objKeyboardToolbar
        }
        activeTextField = textField
         self.tableView.contentSize =  CGSize(width: self.tableView.frame.size.width, height: self.tableView.contentSize.height+300)
//        let cell: UITableViewCell = textField.superview!.superview as! UITableViewCell
//        let textFieldIndexPath = tableView.indexPath(for: cell)
//        self.tableView.scrollToRow(at: textFieldIndexPath!, at: .bottom, animated: true)
//        self.tableViewScrollToBottom()
        return true
    }
    

    func textFieldDidEndEditing(_ textField: UITextField) {
         self.tableView.contentSize =  CGSize(width: self.tableView.frame.size.width, height: self.tableView.contentSize.height-300)
    }
    func keyboardToolbarNextButtonClicked() {
        switch activeTextField {
        case txtFldFirstName:
            txtFldLastName.becomeFirstResponder()
            break
        case txtFldLastName:
            txtFldDOB.becomeFirstResponder()
            break
        case txtFldDOB:
            txtFldMobileNumber.becomeFirstResponder()
            break
        case txtFldMobileNumber:
            txtFldCompanyName.becomeFirstResponder()
            break
        case txtFldCompanyName:
            txtFldStreet.becomeFirstResponder()
            break
        case txtFldStreet:
            txtFldCity.becomeFirstResponder()
            break
        case txtFldCity:
            txtFldPostcode.becomeFirstResponder()
            break
        case txtFldPostcode:
            txtFldPostcode.resignFirstResponder()
            break
        default:
            self.view.endEditing(true)
            break
        }
            
        
        
    }
    func keyboardToolbarPreviousButtonClicked() {
        switch activeTextField {
        case txtFldPostcode:
            txtFldCity.becomeFirstResponder()
            break
        case txtFldCity:
            txtFldStreet.becomeFirstResponder()
            break
        case txtFldStreet:
            txtFldCompanyName.becomeFirstResponder()
            break
        case txtFldCompanyName:
            txtFldMobileNumber.becomeFirstResponder()
            break
        case txtFldMobileNumber:
            txtFldDOB.becomeFirstResponder()
            break
        case txtFldDOB:
            txtFldLastName.becomeFirstResponder()
            break
        case txtFldLastName:
            txtFldFirstName.becomeFirstResponder()
            break
        default:
            self.view.endEditing(true)
            break
        }
    }
    func keyboardToolbarHideKeyboard() {
        hideKeyboard()
    }
    
//    
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//       
//        return false
//    }
//    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if (section == tableView.numberOfSections-1) {
            return 100.0
        }
        return 0.0
    }
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if (section == tableView.numberOfSections-1) {
            let btnUpdate = UIButton()
            let footerView = UIView()
            footerView.frame = CGRect(x: 0.0, y: 0.0, width: self.view.bounds.size.width, height: 100.0)
            
            
            btnUpdate.setTitle("Update", for: .normal)
            btnUpdate.frame = CGRect(x: 8.0, y: 30.0, width: self.view.bounds.size.width-16.0, height: 40.0)
            btnUpdate.addTarget(self, action: #selector(PersonalDetailsViewController.btnUpdateClicked(_:)), for: .touchUpInside)
            btnUpdate.setDefaultThemeStyle()
            footerView.addSubview(btnUpdate)
            return footerView
        }
        let footerView = UIView()
        return footerView
    }

    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
            guard let header = view as? UITableViewHeaderFooterView else { return }
            header.textLabel?.font = UIFont.boldSystemFont(ofSize: CGFloat(TABLEVIEW_HEADERTITLE_FONT_SIZE))
            header.textLabel?.frame = header.frame
            header.textLabel?.textAlignment = .left
            header.textLabel?.textColor = UIColor.black
        
        if section == 0 {
            header.textLabel?.font = UIFont.fontAwesome(ofSize: (header.textLabel?.font.pointSize)!)
            header.textLabel?.text = (String.fontAwesomeIcon(code: FAICON_PERSONAL_DETAILS)! + "\t" + (header.textLabel?.text)!)
        }
       
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 && indexPath.section == 0 {
           
        }
    }
    
    
    
    
    @IBAction func btnUpdateClicked(_ sender: Any) {
        if (self.sideMenuController()?.sideMenu?.isMenuOpen)!{
            return;
        }

        if validateForm() {
            if (Reachability.isConnectedToNetwork()){
                // Create and add the view to the screen.
                self.view.addSubview(activityIndicator)
                activityIndicator.startAnimating()
                // All done!
                //            let strBody = "partnerId=" + UserData.sharedInstance.partnerId
                
//                {"partnerId":"PER-0000000007"}
                
                
                let paramsDict: [String : Any] = ["firstName" : txtFldFirstName.text!,
                                                  "lastName" : txtFldLastName.text!,
                                                  "dateOfBirth" : txtFldDOB.text!,
                                                  "mobileNumber" : txtFldMobileNumber.text!.removeExtraCharFromPhoneNumber(),
                                                  "companyName" : txtFldCompanyName.text!,
                                                  "street" : txtFldStreet.text!,
                                                  "city" : txtFldCity.text!,
                                                  "postCode" : txtFldPostcode.text!,
                                                  "partnerId" : UserData.sharedInstance.partnerId
                                                ]
                Utility.requestWithPostURLWithData(strUrl: BASE_URL + SET_PERSONAL_DETAILS_PATH, paramDict:paramsDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
                    
                    DispatchQueue.main.async {
                        self.activityIndicator.stopAnimating()
                        if error == nil{
                            let httpResponse = urlResponse as! HTTPURLResponse
                            self.responseString = String(data: dataResponse!, encoding: .utf8)!
                            let dict = Utility.convertToDictionary(text: self.responseString)
                            
                            if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK && dict != nil){
                                
                                // ×
                                Utility.showSuccessAlert(message: "Personal details updated successfully")
                                
                                self.txtFldFirstName.text = dict?["firstName"] as? String
                                self.txtFldLastName.text = dict?["lastName"] as? String
                                self.txtFldDOB.text = dict?["dateOfBirth"] as? String
                                self.txtFldMobileNumber.text = (dict?["mobileNumber"] as? String)?.formatePhoneNumber()
                                self.txtFldCompanyName.text = dict?["companyName"] as? String
                                self.txtFldStreet.text = dict?["street"] as? String
                                self.txtFldCity.text = dict?["city"] as? String
                                self.txtFldPostcode.text = dict?["postCode"] as? String
                                Utility.getWhoAmIData()
                            }
                            else{
                                if((dict?["message"]) != nil){
                                    Utility.showErrorAlert( message: (dict?["message"] as? String)!)
                                }
                                else if((dict?["details"]) != nil){
                                    let dictError = (dict?["details"] as![Dictionary<String,Any>])[0]
                                    if((dictError["message"]) != nil){
                                        Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                    }
                                }
                            }
                        }
                        else{
                            Utility.showErrorAlert( message: (error?.localizedDescription)!)
                            
                        }
                    }
                })
            }
            else{
                self.activityIndicator.stopAnimating()
                Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
            }

        }
    }
    func validateForm() -> Bool {
        
        /// First Name Validation
        if (txtFldFirstName.text?.isEmpty)!{
            Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.FirstName.rawValue) )
            txtFldFirstName.setRequiredFildErrorStyle()
            return false;
        }
        else if( !Utility.checkLengthValidation(strInput: txtFldFirstName.text, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100)){
            Utility.showErrorAlert( message: Message.getFieldLengthErrorMessage(strFiledName: Fields.FirstName.rawValue, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100))
            txtFldFirstName.setRequiredFildErrorStyle()
            return false;
        }
        
        /// Last Name Validation
        if (txtFldLastName.text?.isEmpty)!{
            Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.LastName.rawValue))
            txtFldLastName.setRequiredFildErrorStyle()
            return false;
        }
        else if( !Utility.checkLengthValidation(strInput: txtFldLastName.text, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100)){
            
            Utility.showErrorAlert( message: Message.getFieldLengthErrorMessage(strFiledName: Fields.LastName.rawValue, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100))
            txtFldLastName.setRequiredFildErrorStyle()
            return false;
        }
        
        /// DOB Validation
        if (txtFldDOB.text?.isEmpty)!{
            Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.DOB.rawValue))
            txtFldDOB.setRequiredFildErrorStyle()
            return false;
        }
       
        
        /// Mobile Phone Validation
        if (txtFldMobileNumber.text?.isEmpty)!{
            Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.MobilePhoneNumber.rawValue))
            txtFldMobileNumber.setRequiredFildErrorStyle()
            return false;
        }
        else if(!Utility.validatePhone(phoneString:txtFldMobileNumber.text!)){
            Utility.showErrorAlert( message: Message.getFieldInvalidInputErrorMessage(strFiledName: Fields.MobilePhoneNumber.rawValue))
            return false;
        }

        /// Company Name Validation
        if (txtFldCompanyName.text?.isEmpty)!{
            Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.CompanyName.rawValue))
            txtFldCompanyName.setRequiredFildErrorStyle()
            return false;
        }
        else if( !Utility.checkLengthValidation(strInput: txtFldCompanyName.text, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100)){
            Utility.showErrorAlert( message: Message.getFieldLengthErrorMessage(strFiledName: Fields.CompanyName.rawValue, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100))
            return false;
        }
        
        
        

        /// Street Validation
        if (txtFldStreet.text?.isEmpty)!{
            Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.Street.rawValue))
            txtFldStreet.setRequiredFildErrorStyle()
            return false;
        }
        else if( !Utility.checkLengthValidation(strInput: txtFldStreet.text, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100)){
            Utility.showErrorAlert( message: Message.getFieldLengthErrorMessage(strFiledName: Fields.Street.rawValue, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100))
            return false;
        }

        /// City Validation
        if (txtFldCity.text?.isEmpty)!{
            Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.City.rawValue))
            txtFldCity.setRequiredFildErrorStyle()
            return false;
        }
        else if( !Utility.checkLengthValidation(strInput: txtFldCity.text, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100)){
            Utility.showErrorAlert( message: Message.getFieldLengthErrorMessage(strFiledName: Fields.City.rawValue, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100))
            return false;
        }

        
        /// Poscode Validation
        if (txtFldPostcode.text?.isEmpty)!{
            Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.Postcode.rawValue))
            txtFldPostcode.setRequiredFildErrorStyle()
            return false;
        }
        else if( !Utility.checkLengthValidation(strInput: txtFldPostcode.text, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100)){
            Utility.showErrorAlert( message: Message.getFieldLengthErrorMessage(strFiledName: Fields.Postcode.rawValue, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100))
            return false;
        }
        
        return true;
    }
    
    
    
    @objc func openImagePicker(){
        if (self.sideMenuController()?.sideMenu?.isMenuOpen)!{
            return;
        }
        let alert = UIAlertController(title: nil, message: "Choose photo", preferredStyle: UIAlertControllerStyle.actionSheet)
        if imgViewProfilePic.image != UIImage(named: "user_ avatar") {
            alert.addAction(UIAlertAction(title: "Delete Photo", style: UIAlertActionStyle.destructive) { (result : UIAlertAction) -> Void in
                self.imgViewProfilePic.image = UIImage(named: "user_ avatar")
                self.profileBackgroundImage.image = self.imgViewProfilePic.image
                //Code for Camera
                //cameraf
            })
        }
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            alert.addAction(UIAlertAction(title: "Take Photo", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                self.imagePicker.allowsEditing = true
                self.imagePicker.sourceType = .camera
                self.imagePicker.showsCameraControls = true
                self.present(self.imagePicker, animated: true, completion: nil)
            })
        }
        alert.addAction(UIAlertAction(title: "Choose Photo", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            self.imagePicker.allowsEditing = true
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) { (result : UIAlertAction) -> Void in
            alert.dismiss(animated: true, completion: {
                
            })
        })
        if (device_type == .pad){
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = imgViewProfilePic
                popoverController.permittedArrowDirections = .any
                alert.popoverPresentationController?.sourceRect = imgViewProfilePic.bounds;
                
            }
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerEditedImage] as? UIImage
        imgViewProfilePic.image = image
        profileBackgroundImage.image = imgViewProfilePic.image
        self.imagePicker.dismiss(animated: true, completion: nil)
        
        
        var fileName = ""
        if let imageURL = info[UIImagePickerControllerReferenceURL] as? URL {
            let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
            let asset = result.firstObject
            //            print(asset?.value(forKey: "filename") ?? )
            fileName = asset?.value(forKey: "filename") as! String
            
        }
        if fileName.isEmpty{
            fileName = "profileimage_\(Date().millisecondsSince1970).pdf"

        }
        
        uploadProfileImage(image: image!,fileName: fileName)
       
        
    }
    
    func uploadProfileImage(image: UIImage ,fileName: String){
        if (Reachability.isConnectedToNetwork()){
            // Create and add the view to the screen.
            
            
            let imageData: NSData = UIImageJPEGRepresentation(image, 0.5)! as NSData
            let imageStr = imageData.base64EncodedString(options: .lineLength64Characters)
            
            self.view.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            // All done!
            //            let strBody = "partnerId=" + UserData.sharedInstance.partnerId
            
            //                {"partnerId":"PER-0000000007"}
            
            let paramsDict: [String : Any] = ["userName" : UserData.sharedInstance.userName,
                                              "fileList" : [["file" : fileName,
//                                                             "type": "image/png",
                                                             "dataUrl" : "data:image/png;base64, \(imageStr)",
//                                                            "size" : imageData.length
                                                             ]]
                                            ]
            Utility.requestWithPostURLWithData(strUrl: BASE_URL + SET_PROFILE_IMAGE, paramDict:paramsDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
                
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    if error == nil{
                        let httpResponse = urlResponse as! HTTPURLResponse
                        self.responseString = String(data: dataResponse!, encoding: .utf8)!
                        let dict = Utility.convertToDictionary(text: self.responseString)
                        
                        if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK && dict != nil){
                            Utility.getWhoAmIData()
                        }
                        else{
                            if((dict?["message"]) != nil){
                                Utility.showErrorAlert( message: (dict?["message"] as? String)!)
                            }
                            else if((dict?["details"]) != nil){
                                let dictError = (dict?["details"] as![Dictionary<String,Any>])[0]
                                if((dictError["message"]) != nil){
                                    Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                }
                            }
                        }
                    }
                    else{
                        Utility.showErrorAlert( message: (error?.localizedDescription)!)
                        
                    }
                }
            })
        }
        else{
            self.activityIndicator.stopAnimating()
            Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
        }
    }
    
    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
