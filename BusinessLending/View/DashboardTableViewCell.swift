//
//  DashboardTableViewCell.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 8/16/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit

class DashboardTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblNoOfApplication: UILabel!
    @IBOutlet weak var lblApplicationType: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var borderView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
