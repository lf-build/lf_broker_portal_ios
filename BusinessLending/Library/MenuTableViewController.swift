//
//  MenuTableViewController.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 6/21/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit

struct item {
    var name: String!
    var viewCtrl: UIViewController!
    var icon: String!
}
struct Section {
    var name: String!
    var items: [item]!
    var collapsed: Bool!
    
    
    init(name: String, items: [item], collapsed: Bool = false) {
        self.name = name
        self.items = items
        self.collapsed = collapsed
    }
}

class MenuTableViewController: UITableViewController {
    var selectedMenuItemRow : Int = 0
    var selectedMenuItemSection : Int = 0
    var arrayMenuItems : [Dictionary<String, String>] = []
    let headerView: UIView = UIView.init()
    let imageView: UIImageView = UIImageView.init()
    let lblEmail: UILabel = UILabel.init()
    let lblUserName: UILabel = UILabel.init()
    var sections = [Section]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.frame = CGRect.init(x: 0, y: 0, width: 200, height: 100)
        imageView.frame = CGRect.init(x: 15, y:15, width: 70, height: 70)

        imageView.image = UIImage(named: "user_ avatar")

        headerView.backgroundColor = Utility.hexStringToUIColor(hex: THEME_COLOR)
        imageView.backgroundColor = .white
        
        imageView.layer.cornerRadius = 0.5 * imageView.bounds.size.height
        imageView.clipsToBounds = true
        headerView.addSubview(imageView)

        lblUserName.frame = CGRect.init(x: 85, y: (headerView.frame.size.height/2) - 20, width: headerView.frame.size.width, height: 20)
        lblUserName.numberOfLines = 0
        lblUserName.backgroundColor = .clear
        lblUserName.textColor = .white
        lblUserName.font = UIFont.boldSystemFont(ofSize: 20.0)
        lblUserName.textAlignment = .center
         headerView.addSubview(lblUserName)
        
        
        lblEmail.frame = CGRect.init(x: 85, y: headerView.frame.size.height/2, width: headerView.frame.size.width, height:20 )
        lblEmail.numberOfLines = 0
        lblEmail.backgroundColor = .clear
        lblEmail.textColor = .white
        lblEmail.font = UIFont.boldSystemFont(ofSize: 14.0)
        lblEmail.textAlignment = .center
        headerView.addSubview(lblEmail)
        
        let tap =  UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
        tap.delegate = self as? UIGestureRecognizerDelegate
        headerView.addGestureRecognizer(tap)
        
        self.tableView.tableHeaderView = headerView

        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
       
        
        sections = [
            Section(name: "", items: [item(name:"Dashboard",viewCtrl: mainStoryboard.instantiateViewController(withIdentifier: "DashboardViewController"),icon:FAICON_DASHBOARD)]),
            Section(name: "Application", items: [item(name:"All",viewCtrl:mainStoryboard.instantiateViewController(withIdentifier: "AllApplicationViewController"),icon:FAICON_APPLICATION_ALL) , item(name:"Active",viewCtrl:mainStoryboard.instantiateViewController(withIdentifier: "ActiveApplicationViewController"),icon:FAICON_APPLICATION_ACTIVE), item(name:"Closed",viewCtrl:mainStoryboard.instantiateViewController(withIdentifier: "ClosedApplicationViewController"),icon:FAICON_APPLICATION_CLOSED)]),
            Section(name: "Setting", items: [item(name:"Personal details",viewCtrl:mainStoryboard.instantiateViewController(withIdentifier: "PersonalDetailsViewController"),icon:FAICON_PERSONAL_DETAILS), item(name:"Bank Details",viewCtrl:mainStoryboard.instantiateViewController(withIdentifier: "BankDetailsViewController"),icon:FAICON_BANK_DETAILS), item(name:"Account Details",viewCtrl:mainStoryboard.instantiateViewController(withIdentifier: "AccountDetailsViewController"),icon:FAICON_ACCOUNT_DETAILS),item(name:"Logout",viewCtrl:nil,icon:FAICON_SIGNOUT)]),
        ]
        
        
        // Customize apperance of table view
        tableView.contentInset = UIEdgeInsetsMake(64.0, 0, 0, 0) //
        tableView.separatorStyle = .singleLine
        tableView.separatorColor = Utility.hexStringToUIColor(hex: THEME_COLOR)
        tableView.backgroundColor = UIColor.clear
        tableView.scrollsToTop = false
        tableView.backgroundColor = UIColor.white
        // Preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.selectRow( at: NSIndexPath(item:selectedMenuItemRow, section:selectedMenuItemSection) as IndexPath
            , animated: false, scrollPosition: .middle)
        
        // Define identifier
        let notificationName = Notification.Name("reloadUserDetails")
        
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(MenuTableViewController.reloadUserData), name: notificationName, object: nil)

        
        
    }
    
    @objc func handleTap() {
        // handling code
        selectedMenuItemRow = 0
        selectedMenuItemSection = 2
        
        var destViewController : UIViewController
        destViewController = sections[selectedMenuItemSection].items[selectedMenuItemRow].viewCtrl
        sideMenuController()?.setContentViewController(destViewController)
           self.tableView.selectRow(at: IndexPath(row: selectedMenuItemRow, section: selectedMenuItemSection), animated: true, scrollPosition: UITableViewScrollPosition.middle)
    }
    @objc func reloadUserData() {
        let imgUrl:String = UserData.sharedInstance.image
        let url = URL(string: imgUrl)
        if Utility.validateUrl(urlString: imgUrl){
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async {
                    self.imageView.image = UIImage(data: data!)
                }
            }
        }
        lblEmail.text = UserData.sharedInstance.email
        lblUserName.text = UserData.sharedInstance.name
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50.0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        if (sections[(indexPath as NSIndexPath).section].items[(indexPath as NSIndexPath).row].name == "Logout") {
            UserData.sharedInstance.isUserAlreadyLogIn = false
            appDelegate.loadLoginScreen()
        }
        else{
            selectedMenuItemRow = indexPath.row
            selectedMenuItemSection = indexPath.section
          
            var destViewController : UIViewController
            destViewController = sections[(indexPath as NSIndexPath).section].items[(indexPath as NSIndexPath).row].viewCtrl
            sideMenuController()?.setContentViewController(destViewController)
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */
}

//
// MARK: - View Controller DataSource and Delegate
//
extension MenuTableViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].items.count
    }
    
    // Cell
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as UITableViewCell? ?? UITableViewCell(style: .default, reuseIdentifier: "cell")
        cell.textLabel?.font = UIFont.fontAwesome(ofSize: (cell.textLabel?.font.pointSize)!)
        let strIconName: String = sections[(indexPath as NSIndexPath).section].items[(indexPath as NSIndexPath).row].icon
        cell.textLabel?.text = String.fontAwesomeIcon(code: strIconName)! + "\t" + sections[(indexPath as NSIndexPath).section].items[(indexPath as NSIndexPath).row].name
     
        return cell
    }
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == selectedMenuItemRow && indexPath.section == selectedMenuItemSection {
            cell.setSelected(true, animated: true)
        }
        else{
            cell.setSelected(false, animated: true)
        }
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return sections[(indexPath as NSIndexPath).section].collapsed! ? 0 : 44.0
    }
    
    // Header
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as? CollapsibleTableViewHeader ?? CollapsibleTableViewHeader(reuseIdentifier: "header")
        header.titleLabel.text = sections[section].name
        header.arrowLabel.text = ">"
        header.setCollapsed(sections[section].collapsed)
        header.section = section
        header.delegate = self
        return header
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0.0
        }
        return 44.0
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
    
}

//
// MARK: - Section Header Delegate
//
extension MenuTableViewController: CollapsibleTableViewHeaderDelegate {
    
    func toggleSection(_ header: CollapsibleTableViewHeader, section: Int) {
        let collapsed = !sections[section].collapsed
        // Toggle collapse
        sections[section].collapsed = collapsed
        header.setCollapsed(collapsed)
        // Adjust the height of the rows inside the section
//        tableView.beginUpdates()
//        for i in 0 ..< sections[section].items.count {
//            tableView.reloadRows(at: [IndexPath(row: i, section: section)], with: .automatic)
//        }
        tableView.reloadData()
    }
    
}
