//
//  KeyboarToolbar.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 8/4/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit
protocol KeyboardToolbarDelegate: class {
    func keyboardToolbarNextButtonClicked()
    func keyboardToolbarPreviousButtonClicked()
    func keyboardToolbarHideKeyboard()
}
class KeyboarToolbar: UIToolbar {
    
    weak var objKeyboardToolbarDelegate: KeyboardToolbarDelegate? = nil
    static let sharedInstance : KeyboarToolbar = {
        let instance = KeyboarToolbar()
        
        instance.barStyle = .black
        instance.isTranslucent = true
        instance.sizeToFit()
        
        var doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(KeyboarToolbar.hideKeyboard))
        doneButton.tintColor = BLACK_TINTCOLOR
        var flexibleSpaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        var fixedSpaceButton = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        
        var nextButton = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(KeyboarToolbar.keyboardNextButton))
        nextButton.tintColor = BLACK_TINTCOLOR
        var previousButton = UIBarButtonItem(title: "Previous", style: .plain, target: self, action: #selector(KeyboarToolbar.keyboardPreviousButton))
        previousButton.tintColor = BLACK_TINTCOLOR
        instance.setItems([fixedSpaceButton, previousButton, fixedSpaceButton, nextButton, flexibleSpaceButton, doneButton], animated: false)
        instance.isUserInteractionEnabled = true
        
        return instance
    }()
    
    @objc func keyboardNextButton() {
        objKeyboardToolbarDelegate?.keyboardToolbarNextButtonClicked()
    }
    @objc func keyboardPreviousButton() {
        objKeyboardToolbarDelegate?.keyboardToolbarPreviousButtonClicked()
    }
    @objc func hideKeyboard() {
        objKeyboardToolbarDelegate?.keyboardToolbarHideKeyboard()
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
