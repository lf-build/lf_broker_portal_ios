//
//  BLNavigationController.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 6/21/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit

class BLNavigationController: ENSideMenuNavigationController, ENSideMenuDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        sideMenu = ENSideMenu(sourceView: self.view, menuViewController: MenuTableViewController(), menuPosition:.left)
        //sideMenu?.delegate = self //optional
        sideMenu?.menuWidth = 300.0 // optional, default is 160
        sideMenu?.bouncingEnabled = false
        sideMenu?.animationDuration = 0.3
        
        //sideMenu?.allowPanGesture = false
        // make navigation bar showing over side menu
        view.bringSubview(toFront: navigationBar)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.sideMenuController()?.sideMenu?.delegate = self
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - ENSideMenu Delegate
    func sideMenuWillOpen() {
        
    }
    
    func sideMenuWillClose() {
        
    }
    
    func sideMenuDidClose() {
       
    }
    
    func sideMenuDidOpen() {
       
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
