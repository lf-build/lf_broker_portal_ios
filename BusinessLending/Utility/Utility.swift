//
//  Utility.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 6/21/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit

class Utility: NSObject {
    
    
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    static func checkLengthValidation(strInput: String?, minLength: Int, maxLength: Int) -> Bool {
        let strInputLength = strInput?.characters.count
        if (strInputLength! < minLength || strInputLength! > maxLength) {
            return false
        }
        return true
    }
    

    
    static func requestWithPostURLWithData(strUrl : String, paramDict : Dictionary<String,Any>, strToken: String, completion: @escaping (_ result: Data?, _ response: URLResponse?, _ error: Error?)->()) {
        var request = URLRequest(url: URL(string:strUrl )!)
        request.httpMethod = "POST"
        request.timeoutInterval = 30
        if !strToken.isEmpty {
            request.setValue("Bearer \(strToken)", forHTTPHeaderField: "Authorization")
        }
         let jsonData = try? JSONSerialization.data(withJSONObject: paramDict, options: JSONSerialization.WritingOptions())
        request.setValue("\(String(describing: jsonData?.count))" , forHTTPHeaderField: "Content-Length")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                 completion(nil,response,error)
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                
            }
            completion(data,response,error)
        }
        task.resume()
    }
    static func GET_API_CALL(strURL : String, completion: @escaping ( _ result: String?,  _ error: Error?)->())
    {
        var urlRequest = URLRequest(url: URL(string:strURL )!)
        urlRequest.httpMethod = "GET"
        urlRequest.timeoutInterval = 30
        
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = URLSession.shared.dataTask(with: urlRequest){ data, response, error in
            guard let data = data, error == nil
                else
            {                                                 // check for fundamental networking error
                completion("",error)
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200
            {
                completion(httpStatus.description,nil)
            }
            else
            {
                let jsonResponce = String(data: data, encoding: .utf8)
                completion(jsonResponce,error)
            }
            
        }
        task.resume()
    }
    static func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    static func showErrorAlert(message: String) {
//        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
//        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
//        viewCtrl.present(alert, animated: true, completion: nil)
        
        CRNotifications.showNotification(type: .error, title: "Error", message: message, dismissDelay: 3)
    }
    static func showSuccessAlert(message: String) {
        CRNotifications.showNotification(type: .success, title: "Success!", message: message, dismissDelay: 3)
    }
    
    
    // MARK: Validate Email
    static func validateEmail(emailString: String?) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: emailString)
    }
    
    // MARK: Validate SSN
    static func validateSSN(ssnString: String?) -> Bool {
        let ssnFormat = "[0-9]{3}+-[0-9]{2}+-[0-9]{4}"
        let ssnPredicate = NSPredicate(format:"SELF MATCHES %@", ssnFormat)
        return ssnPredicate.evaluate(with: ssnString)
    }
    
    // MARK: Validate SSN
    static func validateAmount(amountString: String?) -> Bool {
        let amountFormat = "[0-9]{4,5}"
        let amountPredicate = NSPredicate(format:"SELF MATCHES %@", amountFormat)
        return amountPredicate.evaluate(with: amountString)
    }
    
    // MARK: Validate URL
    static func validateUrl (urlString: String?) -> Bool {
        if let urlString = urlString {
            if let url  = NSURL(string: urlString) {
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    // MARK: Validate Phone Number
    static func validatePhone(phoneString: String) -> Bool{
      let newPhoneString = phoneString.removeExtraCharFromPhoneNumber()
        let numberCharacters = CharacterSet.decimalDigits.inverted
        if (Utility.checkLengthValidation(strInput: newPhoneString, minLength: MINLENGTH_10, maxLength: MAXLENGTH_10) ) {
            return true && newPhoneString.rangeOfCharacter(from:numberCharacters) == nil
        }
        else{
            return false
        }
    }
    
    
    static func checkAndAddURLPrefic(strURL:String) -> String{
        if (strURL.lowercased().hasPrefix("http://") || strURL.lowercased().hasPrefix("https://")) {
            return strURL
        } else {
            return "http://\(strURL)"
        }
    }
    
    
    static func getWhoAmIData() {
        if (Reachability.isConnectedToNetwork()){
            // Create and add the view to the screen.
           
            // All done!
            let paramsDict: [String : Any] = ["userId" : UserData.sharedInstance.userId,
                                              "userName" : UserData.sharedInstance.userName,
                                              "partnerId" : UserData.sharedInstance.partnerId
            ]
            Utility.requestWithPostURLWithData(strUrl: BASE_URL + WHO_AM_I_PATH, paramDict:paramsDict, strToken: ApplicationAccess.sharedInstance.token, completion: { (dataResponse, urlResponse, error) in
                
                DispatchQueue.main.async {
                  
                    if error == nil{
                        let httpResponse = urlResponse as! HTTPURLResponse
                        let responseString = String(data: dataResponse!, encoding: .utf8)
                        let dict = Utility.convertToDictionary(text: responseString!)
                        
                        if (httpResponse.statusCode == RESPONSE_STATUSCODE_OK && dict != nil){
                            UserData.sharedInstance.email = dict?["email"] as! String
                            UserData.sharedInstance.name = dict?["name"] as! String
//                            if dict?["image"] != nil && dict?["image"] != NUll {
//                                 UserData.sharedInstance.image = (dict?["image"] as? String)!
//                            }
//                            else{
//                                 UserData.sharedInstance.image = ""
//                            }
                           
                            if let imgUrl = dict?["image"] as? String {
                                //Here you received string 'category'
                                UserData.sharedInstance.image = imgUrl
                            }
                            else{
                                UserData.sharedInstance.image = ""
                            }
                            
                            UserData.sharedInstance.userId = dict?["userId"] as! String
                            UserData.sharedInstance.partnerId = dict?["partnerId"] as! String
                            // Post notification
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadUserDetails"), object: nil)

                        }
                        else{
                            if((dict?["message"]) != nil){
                                Utility.showErrorAlert( message: (dict?["message"] as? String)!)
                            }
                            else if((dict?["details"]) != nil){
                                let dictError = (dict?["details"] as![Dictionary<String,Any>])[0]
                                if((dictError["message"]) != nil){
                                    Utility.showErrorAlert( message: (dictError["message"] as? String)!)
                                }
                            }
                        }
                    }
                    else{
                        Utility.showErrorAlert( message: (error?.localizedDescription)!)
                        
                    }
                }
            })
        }
        else{
        
            Utility.showErrorAlert( message: NO_INTERNET_MESSAGE)
        }
    }
   
    static func setPhoneFormat(strPhone:String) -> String{
        
        if (strPhone.isEmpty || strPhone.characters.count != 10 ){
            return strPhone
        }
        var strNewPhone:String = strPhone
        strNewPhone.insert("(", at: strNewPhone.index(strNewPhone.startIndex, offsetBy: 0))
        strNewPhone.insert(")", at: strNewPhone.index(strNewPhone.startIndex, offsetBy: 4))
        strNewPhone.insert(" ", at: strNewPhone.index(strNewPhone.startIndex, offsetBy: 5))
        strNewPhone.insert("-", at: strNewPhone.index(strNewPhone.startIndex, offsetBy: 9))
        return strNewPhone
    }

    
//    static func drawPDFfromURL(url: URL) -> UIImage? {
//        guard let document = CGPDFDocument(url as CFURL) else { return nil }
//        guard let page = document.page(at: 1) else { return nil }
//        let pageRect = page.getBoxRect(.mediaBox)
//        let renderer = UIGraphicsImageRenderer(size: pageRect.size)
//        let img = renderer.image { ctx in
//            UIColor.white.set()
//            ctx.fill(pageRect)
//            ctx.cgContext.translateBy(x: 0.0, y: pageRect.size.height);
//            ctx.cgContext.scaleBy(x: 1.0, y: -1.0);
//            ctx.cgContext.drawPDFPage(page);
//        }
//        return img
//    }
    
   
    
}
