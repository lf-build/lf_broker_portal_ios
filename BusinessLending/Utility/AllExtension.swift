//
//  UIButton+BLButton.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 6/22/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    func setDefaultThemeStyle() {
        self.backgroundColor = Utility.hexStringToUIColor(hex: THEME_COLOR)
        self.setTitleColor(Utility.hexStringToUIColor(hex: BUTTON_TEXT_COLOR), for: UIControlState.normal)
        self.layer.cornerRadius = 0.5 * self.bounds.size.height
        self.clipsToBounds = true
    }
    func setNewApplicationStyle() {
        self.backgroundColor = Utility.hexStringToUIColor(hex: THEME_COLOR)
        self.layer.cornerRadius = 0.5 * self.bounds.size.height
        self.clipsToBounds = true
    }
    func setMenuButtonStyle(){
        self.setImage(UIImage.init(named: "white_menu"), for: UIControlState.normal)
        self.frame = CGRect.init(x: 0, y: 0, width: 25, height: 25) //CGRectMake(0, 0, 30, 30)
        self.backgroundColor = .clear
    }
}

extension UIActivityIndicatorView{
    func setDefaultThemeStyle() {
        self.center = (appDelegate.window?.center)!
        self.hidesWhenStopped = true
        self.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.whiteLarge
        self.color = Utility.hexStringToUIColor(hex: THEME_COLOR)
    }
}
extension UITextField {
    func setRequiredFildErrorStyle() {
         self.attributedPlaceholder = NSAttributedString(string: self.placeholder!, attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
    }
    
    func setFaIconAsLeftView(strFaIcon : String){
        let lblNew = UILabel()
        lblNew.frame = CGRect(x: 0 , y: 0, width: 100, height: self.frame.size.height)
        lblNew.backgroundColor = UIColor.clear
        lblNew.textColor = UIColor.black
        lblNew.translatesAutoresizingMaskIntoConstraints = false
        lblNew.font = UIFont.fontAwesome(ofSize: (self.font!.pointSize))
        lblNew.text = String.fontAwesomeIcon(code: strFaIcon)! + "\t"
        self.leftView = lblNew;
        self.leftViewMode = UITextFieldViewMode.always
    }
    
    func setleftViewString(strText : String){
        let lblNew = UILabel()
        lblNew.frame = CGRect(x: 0 , y: 0, width: 100, height: self.frame.size.height)
        lblNew.backgroundColor = UIColor.clear
        lblNew.textColor = UIColor.darkGray
        lblNew.translatesAutoresizingMaskIntoConstraints = false
        lblNew.font = self.font!
        lblNew.text = strText
        self.leftView = lblNew;
        self.leftViewMode = UITextFieldViewMode.always
    }
}
extension String {
    var asciiArray: [UInt32] {
        return unicodeScalars.filter{$0.isASCII}.map{$0.value}
    }
    func removeExtraCharFromPhoneNumber() -> String {
        var newPhoneString = self.replacingOccurrences(of: "(", with: "")
        newPhoneString = newPhoneString.replacingOccurrences(of: ") ", with: "")
        newPhoneString = newPhoneString.replacingOccurrences(of: "-", with: "")
        return newPhoneString
    }
    func formatePhoneNumber() -> String {
        var newPhoneString = self
        newPhoneString.insert("(", at: newPhoneString.index(newPhoneString.startIndex, offsetBy: 0))
        newPhoneString.insert(")", at: newPhoneString.index(newPhoneString.startIndex, offsetBy: 4))
        newPhoneString.insert(" ", at: newPhoneString.index(newPhoneString.startIndex, offsetBy: 5))
        newPhoneString.insert("-", at: newPhoneString.index(newPhoneString.startIndex, offsetBy: 9))
        return newPhoneString
        
    }
    
}

extension Date {
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
}

extension UIViewController {
    func backViewController() -> UIViewController? {
        if let stack = self.navigationController?.viewControllers {
            for i in (1..<stack.count).reversed() {
                if(stack[i] == self) {
                    return stack[i-1]
                }
            }
        }
        return nil
    }
}
extension UIImageView{
    func setThemColor(){
        self.image = self.image!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        self.tintColor = Utility.hexStringToUIColor(hex: THEME_COLOR)
    }
}
