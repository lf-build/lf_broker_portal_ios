//
//  ValidateTextField.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 6/26/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit

class ValidateTextField: NSObject {
    static func FirstName(txtFldFirstName: UITextField, viewCtlr:UIViewController, completion: @escaping (_ isValidate: Bool)->()) {
        
        //        /// First Name Validation
        if (txtFldFirstName.text?.isEmpty)!{
            Utility.showErrorAlert( message: Message.getBlankFieldErrorMessage(strFiledName: Fields.FirstName.rawValue))
            txtFldFirstName.setRequiredFildErrorStyle()
            completion(false);
        }
        else if( !Utility.checkLengthValidation(strInput: txtFldFirstName.text, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100)){
            Utility.showErrorAlert( message: Message.getFieldLengthErrorMessage(strFiledName: Fields.FirstName.rawValue, minLength: MINLENGTH_2, maxLength: MAXLENGTH_100))
            txtFldFirstName.setRequiredFildErrorStyle()
            completion(false);
        }
        completion(true);
    }

}
