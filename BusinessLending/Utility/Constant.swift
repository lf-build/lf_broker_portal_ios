//
//  Constant.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 6/22/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit




let CURRENCY_SIGN = "$"

//  MARK: - Application Access
//let APPLICATION_ACCESS_URL = "http://192.168.1.72:7100"
//let APPLICATION_ACCESS_URL = "http://34.213.92.143:7100"
//let APPLICATION_ACCESS_PARTNERTYPE = "divergent-capital"
//let APPLICATION_ACCESS_USERNAME = "broker@portal.com"
//let APPLICATION_ACCESS_PASSWORD = "Afyk22@31fDfs"
//let APPLICATION_ACCESS_REALM = "broker-portal"


let APPLICATION_ACCESS_URL = "http://52.32.244.25:7100"
let APPLICATION_ACCESS_PARTNERTYPE = "divergent-capital"
let APPLICATION_ACCESS_USERNAME = "dcbroker004@mailinator.com"
let APPLICATION_ACCESS_PASSWORD = "Sigma@1234"
let APPLICATION_ACCESS_REALM = "broker-portal"



//  MARK: - URLs
//let BASE_URL = "http://192.168.1.72:6101/1499144782999"
//let BASE_URL = "http://34.213.92.143:6101/1499144782999"
let BASE_URL = "http://52.32.244.25:6101/1499144782999"

let LOGIN_PATH = "/authorization/identity/login"
let WHO_AM_I_PATH = "/authorization/identity/who-am-i"
let SIGNUP_PATH = "/authorization/identity/signup"

let FORGOT_PASSWORD_PATH = "/authorization/identity/password-reset-init"

let DASHBOARD_COUNT_PATH = "/api/dashboard/get-dashboard-count"
let DASHBOARD_GRID_PATH = "/api/dashboard/get-dashboard-grid"
let PERSONAL_DETAILS_PATH = "/api/settings/get-personal-details"
let BANKS_DETAILS_PATH = "/api/settings/get-bank-details"

let SET_PROFILE_IMAGE = "/api/settings/upload-profile-image"
let SET_PERSONAL_DETAILS_PATH = "/api/settings/set-personal-details"
let SET_BANK_DETAILS_PATH = "/api/settings/set-bank-details"
let RESET_PASSWORD_PATH = "/authorization/identity/password-reset-init"

let SET_APPLICATION_DRAFT = "/api/application/set-application-draft"
let SET_APPLICATION = "/api/application/set-application"

let Privacy_Policy_URL = "https://divergecap.com/privacy-policy/"
let Terms_of_Service_URL = "https://divergecap.com/terms-of-use/"

let GET_APPLICATION_DETAILS_PATH = "/api/application/get-application"
let GET_APPLICANT_DETAILS_PATH = "/api/application/applicant-details"

let GET_DECLINED = "/api/declined/get-declined"
let GET_VERIFICATION = "/api/verification/get-verification"
let GET_AGREEMENT = "/api/agreement/get-agreement"
let RESEND_DOCUMENT = "/api/agreement/resend-docusign"
let DELET_DOCUMENT = "/api/agreement/discard-document"
let UPLOAD_DOCUMENT = "/api/agreement/upload-engine"
let DOWNLOAD_DOCUMENT = "/api/agreement/download-file"
let GET_PRICE_OFFER = "/api/pricing/get-offers"
let GET_INTRIM_OFFER = "/api/pricing/get-intrim-offers"
let SET_DEAL = "/api/pricing/set-deal"
let DISCARD_APPLICATION = "/api/application/discard-application"

//  MARK: - Field Name
enum Fields:String {
    case FirstName = "First name"
    case LastName = "Last name"
    case EmailAddress = "Email address"
    case PhoneNumber = "Phone number"
    case Address = "Address"
    case City = "City"
    case State = "State"
    case Zip = "Zip"
    case CompanyName = "Company name"
    case DBAName = "DBA Name"
    case Website = "Website"
    case TaxID = "Tax ID"
    case DOB = "Date of Birth"
    case MobilePhoneNumber = "Mobile Phone number"
    case Street = "Street"
    case Postcode = "Postcode"
    case BankName = "Bank Name"
    case BankAccountName = "Bank Account Name"
    case BankAccountNumber = "Bank Account Number"
    case AccountType = "Account Type"
    case ABA = "ABA"
    // MARK: - new application form
    case SSN = "SSN"
    case PrimaryPhone = "Primary Phone"
    case SecondaryPhone = "Secondary Phone"
    case HomeAddress = "Home Address"
    case BusinessLegalName = "Business Legal Name"
    case DBA = "DBA"
    case EIN = "EIN"
    case BusinessAddress = "Business Address"
    case OfficePhone = "Office Phone"
    case RequestedAmount = "Requested Amount"
}
enum FieldFaIcon:String {
    case FirstName = "fa-user-o"
    case EmailAddress = "fa-envelope"
    case PhoneNumber = "fa-phone"
    case Address = "fa-map-marker"
    case Company = "fa-building-o"
    case Website = "fa-globe"
    case TaxID = "fa-money"
    case Temp = "fa-dot-circle-o"
    case Map = "fa-map-o"
}

enum DocumentType:String {
    case Signeddocument = "signeddocument"
    case Bankstatement = "bankstatement"
    case Photoid = "photoid"
    case TaxReturn = "taxreturnstatement"
    case Confession = "confessionofjudgment"
    case Lien = "liendocument"
}



enum AlertTitle:String {
    case Error = "Error"
}




let arrayState:[Dictionary<String,String>] = [  ["stateId":"AL" , "stateDisplayName":"Alabama"],
                    ["stateId":"AK" , "stateDisplayName":"Alaska"],
                    ["stateId":"AZ" , "stateDisplayName":"Arizona"],
                    ["stateId":"AR" , "stateDisplayName":"Arkansas"],
                    ["stateId":"CA" , "stateDisplayName":"California"],
                    ["stateId":"CO" , "stateDisplayName":"Colorado"],
                    ["stateId":"CT" , "stateDisplayName":"Connecticut"],
                    ["stateId":"DE" , "stateDisplayName":"Delaware"],
                    ["stateId":"FL" , "stateDisplayName":"Florida"],
                    ["stateId":"GA" , "stateDisplayName":"Georgia"],
                    ["stateId":"HI" , "stateDisplayName":"Hawaii"],
                    ["stateId":"ID" , "stateDisplayName":"Idaho"],
                    ["stateId":"IL" , "stateDisplayName":"Illinois"],
                    ["stateId":"IN" , "stateDisplayName":"Indiana"],
                    ["stateId":"IA" , "stateDisplayName":"Iowa"],
                    ["stateId":"KS" , "stateDisplayName":"Kansas"],
                    ["stateId":"KY" , "stateDisplayName":"Kentucky"],
                    ["stateId":"LA" , "stateDisplayName":"Louisiana"],
                    ["stateId":"ME" , "stateDisplayName":"Maine"],
                    ["stateId":"MD" , "stateDisplayName":"Maryland"],
                    ["stateId":"MA" , "stateDisplayName":"Massachusetts"],
                    ["stateId":"MI" , "stateDisplayName":"Michigan"],
                    ["stateId":"MN" , "stateDisplayName":"Minnesota"],
                    ["stateId":"MS" , "stateDisplayName":"Mississippi"],
                    ["stateId":"MO" , "stateDisplayName":"Missouri"],
                    ["stateId":"MT" , "stateDisplayName":"Montana"],
                    ["stateId":"NE" , "stateDisplayName":"Nebraska"],
                    ["stateId":"NV" , "stateDisplayName":"Nevada"],
                    ["stateId":"NH" , "stateDisplayName":"New Hampshire"],
                    ["stateId":"NJ" , "stateDisplayName":"New Jersey"],
                    ["stateId":"NM" , "stateDisplayName":"New Mexico"],
                    ["stateId":"NY" , "stateDisplayName":"New York"],
                    ["stateId":"NC" , "stateDisplayName":"North Carolina"],
                    ["stateId":"ND" , "stateDisplayName":"North Dakota"],
                    ["stateId":"OH" , "stateDisplayName":"Ohio"],
                    ["stateId":"OK" , "stateDisplayName":"Oklahoma"],
                    ["stateId":"OR" , "stateDisplayName":"Oregon"],
                    ["stateId":"PA" , "stateDisplayName":"Pennsylvania"],
                    ["stateId":"RI" , "stateDisplayName":"Rhode Island"],
                    ["stateId":"SC" , "stateDisplayName":"South Carolina"],
                    ["stateId":"SD" , "stateDisplayName":"South Dakota"],
                    ["stateId":"" , "stateDisplayName":"State"],
                    ["stateId":"TN" , "stateDisplayName":"Tennessee"],
                    ["stateId":"TX" , "stateDisplayName":"Texas"],
                    ["stateId":"UT" , "stateDisplayName":"Utah"],
                    ["stateId":"VT" , "stateDisplayName":"Vermont"],
                    ["stateId":"VA" , "stateDisplayName":"Virginia"],
                    ["stateId":"WA" , "stateDisplayName":"Washington"],
                    ["stateId":"WV" , "stateDisplayName":"West Virginia"],
                    ["stateId":"WI" , "stateDisplayName":"Wisconsin"],
                    ["stateId":"WY" , "stateDisplayName":"Wyoming"]

]

let splashScreenDelay = 1

//  MARK: - Field Minimum Length
let MINLENGTH_2 = 2
let MINLENGTH_5 = 5
let MINLENGTH_10 = 10
//  MARK: - Field Maximum Length
let MAXLENGTH_5 = 5
let MAXLENGTH_10 = 10
let MAXLENGTH_100 = 100
let MAXLENGTH_200 = 200

let PHONE_NUMBER_LENGTH = 14
let ZIP_CODE_LENGTH = 5
let SSN_MAX_LEGNTH = 11

//  MARK: - Color Hex Code
let TOPBAR_BACKGROUND_COLOR = "#ffffff"
//let THEME_COLOR = "#8ac040"
let THEME_COLOR = "#3b5998"
let BUTTON_TEXT_COLOR = "#ffffff"
//let VIEW_BACKGROUND_COLOR = "#f3f3f3"
let VIEW_BACKGROUND_COLOR = "#DCDCDC"
let TABLEVIEW_CELL_BACKGROUNDCOLOR = "#f9f9f9"
let ACTIVE_APPLICATION_COLOR = "#8ac040"
let CLOSED_APPLICATION_COLOR = "#C12200"
let BORDER_RED_COLOR = "#ff0000"
let GRAY_COLOR = "#A9A9A9"
let SILVER_GRAY_COLOR = "#D3D3D3"
let BORDER_ORANGE_COLOR = "#f59786"
let BORDER_GREEN_COLOR = "#009f33"
//let NAVIGATIONBAR_TINTCOLOR = "#000000"

// MARK: - Color code
let NAVIGATIONBAR_TINTCOLOR = UIColor.white
let BLACK_TINTCOLOR = UIColor.black

//  MARK: - Response Status Code
let RESPONSE_STATUSCODE_OK = 200
let RESPONSE_STATUSCODE_BADREQUEST = 400
let RESPONSE_STATUSCODE_UNAUTHORIZED = 400


// MARK: - FA ICON name
let FAICON_DASHBOARD = "fa-home"
let FAICON_APPLICATION_ALL = "fa-file"
let FAICON_APPLICATION_ACTIVE = "fa-check-square"
let FAICON_APPLICATION_CLOSED = "fa-times-circle"
let FAICON_PERSONAL_DETAILS = "fa-user"
let FAICON_BANK_DETAILS = "fa-university"
let FAICON_ACCOUNT_DETAILS = "fa-user-circle"
let FAICON_SIGNOUT = "fa-sign-out"

// MARK: - TABLEVIEW
let TABLEVIEW_HEADERTITLE_FONT_SIZE = 15.0

let DATE_FORMAT = "MM/dd/yyyy"

//let BLANK_FIELD_VALIDATION_MESSAGE = " should not be blank"


//  MARK: - Validation Message
let NO_INTERNET_MESSAGE = "Please check internet connection"
//let BLANK_FIELD_FIRSTNAME_MESSAGE = "First name "


//  MARK: - Application Sattus
let APPLICATION_STATUS_DRAFT = "Draft"
let APPLICATION_STATUS_APPLICATION_SUBMITTED = "Application Submitted"
let APPLICATION_STATUS_FINANCIAL_VERIFICATION = "Financial Verification"
let APPLICATION_STATUS_PERSONAL_VERIFICATION = "Personal Verification"
let APPLICATION_STATUS_CREDIT_VERIFICATION = "Credit Verification"
let APPLICATION_STATUS_PRICING = "Pricing"
let APPLICATION_STATUS_AGREEMENT_DOCS_OUT = "Agreement Docs Out"
let APPLICATION_STATUS_FUNDING_REVIEW = "Funding Review"
let APPLICATION_STATUS_APPROCED_FOR_FUNDING = "Approved For Funding"
let APPLICATION_STATUS_FUNDED = "Funded"
let APPLICATION_STATUS_CANCELLED = "Cancelled"
let APPLICATION_STATUS_NOT_INTERESTED = "Not Interested"
let APPLICATION_STATUS_DECLINED = "Declined"
let APPLICATION_STATUS_EXPIRED = "Expired"
let APPLICATION_STATUS_DISCARDED = "Discarded"


// 1. request an UITraitCollection instance
let device_type = UIScreen.main.traitCollection.userInterfaceIdiom
let objStoryBoard = UIStoryboard(name: "Main", bundle: nil)

class Constant: NSObject {
    
    
    

}

class Message: NSObject {
    static func getBlankFieldErrorMessage(strFiledName: String) -> String{
        return  "\(strFiledName) is required"
    }
    static func getFieldLengthErrorMessage(strFiledName: String, minLength: Int, maxLength: Int ) -> String{
        return "\(strFiledName) required minimum \(minLength) and maximum \(maxLength) characters."
    }
    static func getFieldInvalidInputErrorMessage(strFiledName: String) -> String{
        return "Please enter valid \(strFiledName)"
    }
    
}
