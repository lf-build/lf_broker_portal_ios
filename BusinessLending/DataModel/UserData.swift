//
//  UserData.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 6/21/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit

class UserData: NSObject {
    
    static let sharedInstance: UserData = {
        let instance = UserData()
        // setup code
        return instance
    }()
    
    
    var userId: String {
        get {
            return UserDefaults.standard.object(forKey: "userId") as? String ?? ""
        }
        set(newValue) {
            UserDefaults.standard.set(newValue, forKey: "userId")
            UserDefaults.standard.synchronize()
        }
    }
    
    var partnerId: String {
        get {
            return UserDefaults.standard.object(forKey: "partnerId") as? String ?? ""
        }
        set(newValue) {
            UserDefaults.standard.set(newValue, forKey: "partnerId")
            UserDefaults.standard.synchronize()
        }
    }
    var token: String {
        get {
            return UserDefaults.standard.object(forKey: "token") as? String ?? ""
        }
        set(newValue) {
            UserDefaults.standard.set(newValue, forKey: "token")
            UserDefaults.standard.synchronize()
        }
    }
    var email: String {
        get {
            return UserDefaults.standard.object(forKey: "email") as? String ?? ""
        }
        set(newValue) {
            UserDefaults.standard.set(newValue, forKey: "email")
            UserDefaults.standard.synchronize()
        }
    }
    var userName: String {
        get {
            return UserDefaults.standard.object(forKey: "userName") as? String ?? ""
        }
        set(newValue) {
            UserDefaults.standard.set(newValue, forKey: "userName")
            UserDefaults.standard.synchronize()
        }
    }
    var name: String {
        get {
            return UserDefaults.standard.object(forKey: "name") as? String ?? ""
        }
        set(newValue) {
            UserDefaults.standard.set(newValue, forKey: "name")
            UserDefaults.standard.synchronize()
        }
    }

    
    var image: String {
        get {
            return UserDefaults.standard.object(forKey: "image") as? String ?? ""
        }
        set(newValue) {
            UserDefaults.standard.set(newValue, forKey: "image")
             UserDefaults.standard.synchronize()
        }
    }
    
    var isUserAlreadyLogIn: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "isAlreadyLogIn") 
        }
        set(newValue) {
            UserDefaults.standard.set(newValue, forKey: "isAlreadyLogIn")
            UserDefaults.standard.synchronize()
        }
    }
    
    override init() {
        super.init()
    }
}
