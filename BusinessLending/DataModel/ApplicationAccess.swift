//
//  ApplicationAccess.swift
//  BusinessLending
//
//  Created by Amitkumar Patel on 7/3/17.
//  Copyright © 2017 Amitkumar Patel. All rights reserved.
//

import UIKit

class ApplicationAccess: NSObject {
    static let sharedInstance: ApplicationAccess = {
        let instance = ApplicationAccess()
        // setup code
        return instance
    }()
    
    var userId: String {
        get {
            return UserDefaults.standard.object(forKey: "appUserId") as? String ?? ""
        }
        set(newValue) {
            UserDefaults.standard.set(newValue, forKey: "appUserId")
            UserDefaults.standard.synchronize()
        }
    }
    
    var partnerId: String {
        get {
            return UserDefaults.standard.object(forKey: "appPartnerId") as? String ?? ""
        }
        set(newValue) {
            UserDefaults.standard.set(newValue, forKey: "appPartnerId")
            UserDefaults.standard.synchronize()
        }
    }
    var token: String {
        get {
            return UserDefaults.standard.object(forKey: "appToken") as? String ?? ""
        }
        set(newValue) {
            UserDefaults.standard.set(newValue, forKey: "appToken")
            UserDefaults.standard.synchronize()
        }
    }
    
    var isUserAlreadyLogIn: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "isApplicationAccessVerified")
        }
        set(newValue) {
            UserDefaults.standard.set(newValue, forKey: "isApplicationAccessVerified")
            UserDefaults.standard.synchronize()
        }
    }
    override init() {
        super.init()
    }

}
